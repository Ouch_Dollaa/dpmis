
//$(document).ready(function () {
	var isRotation = true;
	new Propeller(document.getElementById('wrapper-propeller'), {
		inertia: 0,
		speed: 0,
		step: 0,
		onRotate : function(){
			/*console.log("rotate");*/
			isRotation = false;
		},
		onDragStop : function(){
		   /* console.log("stop");*/
			if(!isRotation){
				setTimeout(function(){
					isRotation = true;
				},500);
			} 
		},
		onDragStart : function(){
			
		}
	});
	$(".vertical .carousel .bxslider").bxSlider({
		  mode: 'vertical',
		  moveSlides:1,
		  auto:true,
		  autoHover:true,
		  adaptiveHeight:true,
		  pager:false,
		  controls:false,
		  minSlides:3,
		 
		  infiniteLoop:true
	});
	

	var degree = localStorage.getItem("key");

	$("#wrapper-propeller").css("transform", 'rotate(' + degree + ') translateZ(0px)');

	/*$("#wrapper1").css("transform", "'rotate(' + degree +') translateZ(0px) !important;'" );*/
	
	setInterval(function () {
		var transform = $("#wrapper-propeller").attr("style");
		var res = transform.split("(")[1];
		res = res.split(")")[0];
		localStorage.setItem("key", res);
	}, 1000);
		
		var isSmall = false;
		var clickRight = false;
	   $(".piece").click(function(){
			var route = $(this).attr("data-page");
			do_small_module(isRotation,isSmall,route);
	   });
	   
	   function do_small_module(isRotation,isSmall,route){
		   if(isRotation){
				if(!isSmall){
					isSmall = true;
					$(".top-left").css("margin-top","80px");

					$(".h2-schedule").velocity({
						"opacity":1
					},{
						complete:function(){
							$(this).css("display","block");
						}
					});
					if ($(window).width() > 1700) {
						$(".cssplay-menu").velocity({
							"top":"-230px",
							"left":"-82%"
						});
					}
					else if ($(window).width() <= 1700 && $(window).width() > 1400 ) {
						$(".cssplay-menu").velocity({
							"top":"-225px",
							"left":"-80%"

						});

					}
					else if ($(window).width() <= 1400 && $(window).width() > 1200 ) {
						$(".cssplay-menu").velocity({
							"top":"-225px",
							"left":"-79%"
						});

					}
					else if ($(window).width() <= 1200 && $(window).width() > 1000 ) {
						$(".cssplay-menu").velocity({
							"top":"-225px",
							"left":"-80%"
						});

					}else if ($(window).width() <= 1000 && $(window).width() > 450 ) {
						$(".cssplay-menu").velocity({
							"top":"-220px",
							"left":"0"

						});

					}else{
						$(".cssplay-menu").velocity({
							"top":"-190px",
							"left":"0"
						});

					}

					$(".cssplay-menu").css({
						"transform":"scale(0.15,0.15)",
						"position":"absolute"
					});
					var sCircle = $("#small-circle");

					$(".blg-table").velocity({
									height: "100%" ,
									bottom : 0
					}, {
						delay: 300,
						complete:function(){
							$(this).css("display","block");
						}
					});
					$(".module").css({"display":"block"});
					$(".module").velocity({opacity: 1});
				}


				// var iframe = document.getElementById("iframe");
				// iframe.src = $(this).attr("data-page");
				if(route != undefined){
					window.location.href = route;
				};
				//var h2_ = document.getElementById("h2-iframe");
				//h2_.innerHTML = $(this).attr("data-head");
			}
			$("#is_small_module").val("true");
	   }

	   $(".blg-logo-middle .logo-middle").click(function(){
		   window.location.href = '#smart-office';
		   do_large_module();
	   });
	   
	   function do_large_module(){
		    isSmall = false;
			setTimeout(function(){
				$(".top-left").css("margin-top","0");
			},400);
		  
		   
			$(".h2-schedule").velocity({
				"opacity":0
			},{						
				complete:function(){
					$(this).css("display","none");
				}
			});
			if(!clickRight){
				$(".cssplay-menu").velocity({
					 "left":"0",
					 "right":"0",
					 "top" : "20vh"
				});
			}else{
				$(".cssplay-menu").velocity({
					 "left":"15%",
					 "right":"0",
					 "top" : "20vh"
				});
			}
			if ($(window).width() > 1460) {
				$(".cssplay-menu").css({
				  "transform":"scale(1,1)",
				  "position":"absolute",
				});
				$(".cssplay-menu").velocity({
					"top":"20vh",
				});
			}else if ($(window).width() < 1460 && $(window).width() > 1300){
				$(".cssplay-menu").css({
				  "transform":"scale(1,1)",
				  "position":"absolute",
				});
				$(".cssplay-menu").velocity({
					"top":"10vh",
				});
			}else if ($(window).width() > 960 && $(window).width() < 1300){
				$(".cssplay-menu").css({
				  "transform":"scale(1,1)",
				  "position":"absolute",
				});
				$(".cssplay-menu").velocity({
					"top":"6vh",
				
				});
			}else if ($(window).width() <= 960 && $(window).width() > 430){
				$(".cssplay-menu").css({
				  "transform":"scale(0.7,0.7)",
				  "position":"relative"
				});	
			}else{
				$(".cssplay-menu").css({
				  "transform":"scale(0.5,0.5)",
				  "position":"relative"
				});	
				
			}
			$(".blg-table").velocity({
				height: "auto" ,
				bottom : "-40vh"
			}, {
			 delay: 300
			 
			});
			$(".module").css({"display":"none"});
			$(".module").velocity({opacity: 0});
			$("#is_small_module").val("false");
	   }
	    
	 	var keyRightClose = localStorage.getItem("keyRightClose");
		
		if(keyRightClose == null){
			localStorage.setItem("keyRightClose",1);
		}
		if(keyRightClose == 0){
			
		}
	   $(".clickRight").click(function(){
		   var isSmall = $("#is_small_module").val();
		   //console.log(isSmall);
		   isSmall = isSmall == "true" ? true : false;
		   
		   if(!isSmall){
			   
			   if(!clickRight){
				   isBigClose();
				 
				}else{
					isBigOpen();
				}
				
				
			}else{
				if(!clickRight){
					isSmallClose();
					
				}else{
					isSmallOpen();
					
				}
				//localStorage.setItem("keyRightClose",1);
			}
	   });
//});
function isBigClose(){
	console.log($(".slimScrollBar"));
		localStorage.setItem("keyRightClose",0);
	   clickRight = true;
	   $(".blg-right").children().css("display","none");
	  
	   $(".slimScrollBar").css({"display":"none"});
	   $(".clickRight").css("display","block");
	   $(".blg-right").velocity({
			width: "3%",   
		},{
			complete:function(){
				$(".cssplay-menu").css("left","15%");
				 $(".slimScrollBar").css("right","3%");
				  $(".slimScrollBar").css({"display":"block"});
			}	
		});
		$(".blg-main").css("width","77%");
}
function isBigOpen(){
	console.log($(".slimScrollBar"));
	localStorage.setItem("keyRightClose",1);
	clickRight = false;
   	$(".blg-right").children().css("display","block");
	$(".slimScrollBar").css({"display":"none"});
  	 
  	 $(".clickRight").css("display","block");
  	 $(".blg-right").velocity({
		width: "20%",   
		},{
		complete:function(){
			$(".cssplay-menu").css("left","0");
			$(".slimScrollBar").css({"right":"20%"});
			$(".slimScrollBar").css({"display":"block"});
		}	
		});
		$(".blg-main").css("width","60%");	
}
function isSmallClose(){
	console.log($(".slimScrollBar"));
		localStorage.setItem("keyRightClose",0);
	   clickRight = true;
	   $(".blg-right").children().css("display","none");
	   $(".slimScrollBar").css({"display":"none"});
	  
	   $(".clickRight").css("display","block");
	   $(".blg-right").velocity({
			width: "3%",   
		},{
			complete:function(){
				$(".cssplay-menu").css("left","-80%");
				$(".cssplay-menu").css("top","-230px");
				 $(".slimScrollBar").css({"right":"3%"});
				  $(".slimScrollBar").css({"display":"block"});
			}	
		});
		$(".blg-main").css("width","77%");
	
}
function isSmallOpen(){
	console.log($(".slimScrollBar"));
		localStorage.setItem("keyRightClose",1);
		clickRight = false;
	   $(".blg-right").children().css("display","block");
	    $(".slimScrollBar").css({"display":"none"});
	  
	   $(".clickRight").css("display","block");
	   $(".blg-right").velocity({
			width: "20%",   
		},{
			complete : function(){
				 $(".slimScrollBar").css({"right":"20%"});
				  $(".slimScrollBar").css({"display":"block"});
			}
		});
		
		$(".cssplay-menu").css("left","-80%");
		$(".cssplay-menu").css("top","-230px");
		$(".blg-main").css("width","60%");
}

 function showLibraryInfo() {
	if (window.libInfoShown !== true) {
		var windStuff = document.getElementsByClassName('wind');
		var libStuff = document.getElementsByClassName('propeller');
		for (var i = 0; i < windStuff.length; i++) {
			var obj = windStuff[i];
			obj.style.display = 'none';
		}
		for (var j = 0; j < libStuff.length; j++) {
			var objs = libStuff[j];
			objs.style.display = 'inline-block';
		}
		window.libInfoShown = true;
	}
}
$(function(){
	$('.blg-main').slimscroll({
	   distance : '20%'
	});
	
	var keyRightClose = localStorage.getItem("keyRightClose");
		
	if(keyRightClose == null){
		localStorage.setItem("keyRightClose",1);
	}
//	if(keyRightClose == 0){

		var url =window.location.href;
		if(url.indexOf("smart-office") !== -1){
			if(keyRightClose == 0){
				isBigClose();
			}
		}else{
			if(keyRightClose == 0){
				isSmallClose();
			}
			//
		}
		
	//}

});