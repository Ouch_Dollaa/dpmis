var jqxTheme = 'bootstrap';
var rowsheight = 35;
var gridHeight = $(window).height() - 160;
var template = 'primary';
var basePath = document.getElementById('baseUrl').value;

/* jqxLoader */
$('#jqxLoader').jqxLoader({ width: 130, height:90, isModal: true,text: 'Loading...'});

function initialButton(buttonArray,width,height){
    for(var j = 0; j < buttonArray.length;j++){
        $('#'+buttonArray[j]).jqxButton({ width: width,height: height,roundedCorners:'all',template:template});
    }
}

function createJqxWindowId(id){
    if($('#'+id).length>0){
        $('#'+id).jqxWindow('destroy');
    }
    $('body').append('<div id="'+id+'"><div><div style="top:50%;right:45%;position: absolute;"><img src="'+basePath+'jqwidgets/styles/images/loader.gif"></div></div></div>');
}

function closeJqxWindowId(id){
    if($('#'+id).length!=0){
        $('#'+id).jqxWindow('destroy');
    }
}

function newJqxItem(prefix, windowTitle,windowWidth, windowHeight, url, rowId, token) {
    var id = "jqxwindow"+prefix;
    createJqxWindowId(id);
    $('#'+id).jqxWindow({ theme: jqxTheme, width: windowWidth, height:windowHeight, resizable: true, isModal: true, modalOpacity: 0.7, animationType: 'slide',maxHeight:'850px',maxWidth:'1200px',
        initContent: function(){
            $('#'+id).jqxWindow('setTitle',windowTitle);
            $.ajax({
                type: 'post',
                url: url,
                data:{'id':rowId,'_token':token,'ajaxRequestHtml':'true'},
                success: function (data) {
                    $('#'+id).jqxWindow('setContent',data);
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }
            });
            $('#'+id).on('close',function(){
                closeJqxWindowId(id);
            });
        }
    });
}
function newJqxAjax(prefix, windowTitle,windowWidth, windowHeight, url, param) {
    var id = "jqxwindow"+prefix;
    createJqxWindowId(id);
    $('#'+id).jqxWindow({ theme: jqxTheme, width: windowWidth, height:windowHeight, resizable: true, isModal: true, modalOpacity: 0.7, animationType: 'slide',maxHeight:'800px',maxWidth:'1200px',
        initContent: function(){

            $('#'+id).jqxWindow('setTitle',windowTitle);
            $.ajax({
                type: 'post',
                url: url,
                data: param,
                success: function (data) {
                    $('#'+id).jqxWindow('setContent',data);
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }
            });
            $('#'+id).on('close',function(){
                closeJqxWindowId(id);
            });
        }
    });
}

function saveJqxItemNotClose(prefix, saveUrl, token){
    var valid = $('#jqx-form'+prefix).jqxValidator('validate');
    if(valid || typeof(valid) === 'undefined'){
        var formData = new window.FormData($('#jqx-form'+prefix)[0]);
        $.ajax({
            type: "post",
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: "json",
            url: saveUrl,
            beforeSend: function( xhr ) {
                $('#jqx-save'+prefix).jqxButton({ disabled: false });
            },
            success: function (response) {
                $("#jqx-grid"+prefix).jqxTreeGrid('updateBoundData');
                $("#jqx-grid"+prefix).jqxGrid('updatebounddata');
                $("#jqx-grid"+prefix).jqxGrid('clearselection');
                $("#jqx-notification").jqxNotification();
                $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:3000});
                if(response.code == 0){
                    $('#jqx-notification').jqxNotification({ position: 'top-right',template: "warning",autoClose: false }).html(response.message);
                    $("#jqx-notification").jqxNotification("open");
                }else{
                    $('#jqx-notification').jqxNotification({ position: 'top-right',template: "success" }).html(response.message);
                    $("#jqx-notification").jqxNotification("open");
                }
            },
            complete: function(jqXHR, textStatus) {
                var responseText = JSON.parse(jqXHR.responseText);
                if(responseText.code == 0){
                    $('#jqx-save'+prefix).jqxButton({ disabled: false });
                }
            },
            error: function (request, textStatus, errorThrown) {
                $('#jqx-save'+prefix).jqxButton({ disabled: false });
                console.log(textStatus);
            }
        });
    }
}

function saveJqxItem(prefix, saveUrl, token, callback){
    var valid = $('#jqx-form'+prefix).jqxValidator('validate');
    if(valid || typeof(valid) === 'undefined'){
        var formData = new window.FormData($('#jqx-form'+prefix)[0]);
        $.ajax({
            type: "post",
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: "json",
            url: saveUrl,
            beforeSend: function( xhr ) {
                $('#jqx-save'+prefix).jqxButton({ disabled: true });
            },
            success: function (response) {
                $("#jqx-grid"+prefix).jqxTreeGrid('updateBoundData');
                $("#jqx-grid"+prefix).jqxGrid('updatebounddata');
                $("#jqx-grid"+prefix).jqxGrid('clearselection');
                $("#jqx-notification").jqxNotification();
                $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:3000});
                if(response.code == 0){
                    $('#jqx-notification').jqxNotification({ position: 'top-right',template: "warning",autoClose: false }).html(response.message);
                    $("#jqx-notification").jqxNotification("open");
                }else{
                    $('#jqx-notification').jqxNotification({ position: 'top-right',template: "success" }).html(response.message);
                    $("#jqx-notification").jqxNotification("open");
                    if(callback){
                        callback(response);
                    }else{
                        closeJqxWindowId('jqxwindow'+prefix);
                    }
                }
            },
            complete: function(jqXHR, textStatus) {
                var responseText = JSON.parse(jqXHR.responseText);
                if(responseText.code == 1){
                    closeJqxWindowId('jqxwindow'+prefix);
                }else{
                    $('#jqx-save'+prefix).jqxButton({ disabled: false });
                }
            },
            error: function (request, textStatus, errorThrown) {
                $('#jqx-save'+prefix).jqxButton({ disabled: false });
                console.log(textStatus);
            }
        });
    }
}

//Dropdown Tree Selection
function jqxTreeDropDownList(label,jqxTheme, width, height, jqxdropDownListId, jqxTreeId, jsonList, hiddenId, hasRoot, hasMainRoot, isSelectFirstItem){
    var dropDownList = jqxdropDownListId;
    if(jsonList==null || jsonList.length <= 0){
        $(dropDownList).jqxDropDownButton({ theme: jqxTheme, width: width, height: 25});
        return false;
    }

    var jqxTree = jqxTreeId;
    var defaultHiddenVal = (hiddenId != null && hiddenId!="" ? $(hiddenId).val() : '');
    $(dropDownList).jqxDropDownButton({ enableBrowserBoundsDetection:true,theme: jqxTheme, width: width, height: 32,animationType: 'fade'});

    $(jqxTree).bind('select', function (event) {
        var args = event.args;
        var item = $(jqxTree).jqxTree('getItem', args.element);
        (hiddenId!=null && hiddenId!=""?$(hiddenId).val(item.id):'');
        var dropDownContent = '<div style="position: relative;margin-top:8px;">'+item.label+'</div>';
        $(dropDownList).jqxDropDownButton('setContent', dropDownContent);
        $(dropDownList).jqxDropDownButton('close');
    });

    var source = jsonList;//console.log(source);
    var dataAdapter = new $.jqx.dataAdapter(source);
    dataAdapter.dataBind();
    var records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label'}]);
    if(hasRoot===true){
        if(hasMainRoot!=null && hasMainRoot===true){
            var records = [{ label: label, id:0, parentid:0, icon: basePath+"icon/folder.png", expanded: true, items: records}];
        }else{
            var records = [{ label: " ", id:0, parentid:0, icon: basePath+"icon/folder.png", expanded: true, items: records}];
        }
    }

    $(jqxTree).jqxTree({ theme: jqxTheme, source: records, width: width-2, height: height,allowDrag:false});
    //$(jqxTree).jqxTree().jqxTree('expandAll');
    if(isSelectFirstItem===true){
        $(jqxTree).jqxTree('selectItem', $(jqxTree).find('li:first')[0]);
    }
    if(defaultHiddenVal != "" && defaultHiddenVal != "0"){
        var element = $(jqxTree).find('#'+defaultHiddenVal)[0];
        $(jqxTree).jqxTree('selectItem', element);
    }
}

//Dropdown List
function initDropDownList(jqxTheme, width, height, jqxdropDownListId, source, displayField, valueField, hasDefaultItem, defaultItemText, defaultItemValue, hiddenFeildId,filterPlaceHolder,dropDownHeight){

    if(source==null || source.length <= 0){
        $(jqxdropDownListId).jqxDropDownList({selectedIndex: 0, source:[], width: width,height:height, dropDownHeight: 1, theme: jqxTheme});
        return false;
    }
    var dataAdapter = new $.jqx.dataAdapter(source,
        {
            beforeLoadComplete: function (records) {
                if(hasDefaultItem===true){
                    var defaultItem = '{"'+valueField+'":"'+defaultItemValue+'","'+displayField+'":"'+defaultItemText+'"}';
                    records.unshift(JSON.parse(defaultItem));
                }
                return records;
            }
        }
    );
    dataAdapter.dataBind();
    //initial dropdown property
    $(jqxdropDownListId).jqxDropDownList({
        selectedIndex:0,
        source: dataAdapter,
        displayMember: displayField,
        valueMember: valueField,
        width: width,
        height:height,
        itemHeight:30,
        filterHeight:35,
        dropDownHeight: dropDownHeight,
        theme: jqxTheme,
        filterable: true,
        searchMode: 'contains',
        filterPlaceHolder: filterPlaceHolder,
        enableBrowserBoundsDetection: true,
        animationType: 'fade'
    });

    //On dropdown select event
    $(jqxdropDownListId).bind('select', function (event) {
        var args = event.args;
        var item = $(jqxdropDownListId).jqxDropDownList('getItem', args.item);
        if(item == undefined){
            $(hiddenFeildId).val('');
        }else{
            ((hiddenFeildId!="" && item != null) ? $(hiddenFeildId).val(item.value) : '');
        }
    });

    if(hiddenFeildId!="" && $(hiddenFeildId).val()!= ""){
        $(jqxdropDownListId).jqxDropDownList('selectItem', $(hiddenFeildId).val());
    }


}

//Dropdown Tree Checkbox
function initJqxTreeCheckbox(localData,treeCheckBoxDiv,hiddenFields,width,height){
    var source = {
        type: "post",
        datatype: "json",
        datafields: [
            { name: 'id' },
            { name: 'parentid' },
            { name: 'text' },
            { name: 'value' }
        ],
        id: 'id',
        localdata: localData
    };
    var dataAdapter = new $.jqx.dataAdapter(source);
    dataAdapter.dataBind();
    var records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label'}]);
    $('#'+treeCheckBoxDiv).jqxTree({ source: records, width: width, height:height,hasThreeStates: false ,checkboxes: true, theme: jqxTheme});
    $('#'+treeCheckBoxDiv).on('change', function (event) {
       getCheckedItems('#'+hiddenFields, 'getCheckedItems',treeCheckBoxDiv);
    });
}

function getCheckedItems(txtId, checkStatus,treeCheckBoxDiv){
    var arrId = [];
    var items = $('#'+treeCheckBoxDiv).jqxTree(checkStatus);
    if(items.length > 0){
        $.each(items, function (index,value) {
            if(value != ""){
                arrId.push(value.id);
                $('#'+treeCheckBoxDiv).jqxTree('checkItem', value.id, true);
            }
        });
        $(txtId).val(arrId.join(','));
    }else{
        $(txtId).val('0');
    }
}



/*
 * Validate File Upload by Exetensions
 * allowFiles = ['.jpg','.png','.gif']
 * Usage:
 * isValidFileUpload(allowFiles)
 * */
function isValidFileUpload(allowFiles){
    var inputMethod = document.getElementsByTagName('input');
    $("#jqx-notification").jqxNotification();
    for(var i = 0; i < inputMethod.length; i++){
        var oInput = inputMethod[i];
        var isValid = false;
        if(oInput.type == 'file'){
            var fileName = oInput.value;
            if(fileName.length > 0){
                for(var j = 0; j < allowFiles.length; j++){
                    var curExt = allowFiles[j];
                    if(fileName.substr(fileName.length - curExt.length,curExt.length).toLowerCase() == curExt.toLowerCase()){
                        isValid = true;
                        break;
                    }
                }
                if(!isValid){
                    var message = "Sorry, " + fileName + " is invalid, allow files: " + allowFiles.join(", ");
                    $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning",autoClose: false }).html(message);
                    $("#jqx-notification").jqxNotification("open");
                    return false;
                }
            }
        }
    }
    return true;
}

function isNumeric(input){
    var value = isNaN($("#"+input).val());
    if(value){
        $("#"+input).val('');
    }
}
function getJqxCalendar(divJqxCalendar,divHiddenValue,width,height,placeHolder,value){
    if(value == null || value == ''){
        $('#'+divJqxCalendar).jqxDateTimeInput({
            enableBrowserBoundsDetection:true,
            width: width,
            height: height,
            formatString: 'dd/MM/yyyy',
            readonly: false,
            showFooter: true,
            placeHolder: placeHolder,
            value:null
        });
    }else{
        $('#'+divJqxCalendar).jqxDateTimeInput({
            enableBrowserBoundsDetection:true,
            width: width,
            height: height,
            formatString: 'dd/MM/yyyy',
            showFooter: true,
            placeHolder: placeHolder,
            readonly: false,
            value:value
        });
    }
    $('#'+divJqxCalendar).on('change', function () {
        var dateVal = $(this).val().split('/');
        if(dateVal != undefined){
            $('#'+divHiddenValue).val(dateVal[2]+'-'+dateVal[1]+'-'+dateVal[0]);
        }
    });
}
function getJqxTime(divTime,timeHidden){
    $('#'+divTime).jqxDateTimeInput({
        width: 86,
        height: 30,
        formatString: 't',
        animationType:'fade',
        showTimeButton: true,
        showCalendarButton: false
    });
    $('#'+timeHidden).val($("#"+divTime).val());
    $('#'+divTime).on('change',function(){
        $('#'+timeHidden).val($("#"+divTime).val());
    });
}

function confirmDelete(title,content,callback) {
    $.confirm({
        title: title,
        content: content,
        buttons: {
            confirm: function () {
                callback();
            },
            cancel: function () {
                console.log('canceled');
            }
        }
    });
}
