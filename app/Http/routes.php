<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$secretRoute = Config::get('constant')['secretRoute'];
Route::get('/', 'Auth\AuthController@getIndex');
Route::get($secretRoute.'/auth/logout', 'Auth\AuthController@getLogout');
Route::get($secretRoute.'/change-password', 'BackEnd\User\UserController@getChangePassword');

Route::controllers([
    'auth'                           		=>'Auth\AuthController',
    /* Dasboard */
	$secretRoute.'/dashboard'        		=>'BackEnd\Dashboard\DashboardController',
	/* User and Role */
	$secretRoute.'/role'       	     		=>'BackEnd\User\RoleController',
	$secretRoute.'/user'       	     		=>'BackEnd\User\UserController',
	$secretRoute.'/resource'         		=>'BackEnd\User\ResourceController',
	/* Setting */
	$secretRoute.'/document'         		=>'BackEnd\document\DocumentController',
	$secretRoute.'/ministry-code'    		=>'BackEnd\Setting\MinistryCode\MinistryCodeController',
	$secretRoute.'/kindof-expend'    		=>'BackEnd\Setting\KindofExpend\KindofExpendController',
	$secretRoute.'/processing'    	 		=>'BackEnd\Setting\Processing\ProcessingController',
	$secretRoute.'/permission'    	 		=>'BackEnd\Setting\Permission\PermissionController',
	$secretRoute.'/processing-place' 		=>'BackEnd\Setting\ProcessingPlace\ProcessingPlaceController',
	$secretRoute.'/document-reporting'      =>'BackEnd\Setting\report\ReportController'
]);