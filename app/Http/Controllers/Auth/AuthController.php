<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Validation\ValidationUser;
use Config;
use Illuminate\Support\Facades\Session;
use App\Models\BackEnd\AuthModel;
use App\Validation\ValidationAuth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
//    protected $auth;
//
//    /**
//     * The registrar implementation.
//     *
//     * @var Registrar
//     */
//    protected $registrar;

    public function __construct()
    {
        $this->data = array();
		$this->Models = new AuthModel;
        $this->data["html_title"] = "Meeting Scheduler System";
        $this->data['messages'] = Config::get('messages');
        $this->constant = Config::get('constant');
        $this->data['constant'] = $this->constant;
		$this->Validation = new ValidationAuth;
        $this->viewFolder = "auth";
    }

    public function getIndex() {
        return view($this->viewFolder.'.login')->with($this->data);
    }

    public function postLogin(Request $request){
        /* Validation Insert Data */
        $validator = $this->Validation->validationLogin($request->all());
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
        /* Validation Insert Data End */
        return $this->Models->postLogin($request->all());
    }
	
    public function getLogout(){
        Session::forget('sessionUser');
        return redirect('/auth');
    }
}

