<?php 
namespace App\Http\Controllers\BackEnd\User;

use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\User\UserModel;

class UserController extends BackendController {

    public function __construct(){
        parent::__construct();
		$this->user = new UserModel();
    }

    public function getIndex(){
        $this->data['inputUrl'] = $this->user->getUserName();
        return view($this->viewFolder.'.users.user.index')->with($this->data);
    }
	public function postIndex(Request $request){
        return $this->user->getDataGrid($request->all());
    }
	public function postNew(Request $request){
		$this->data['id'] = $request['id'];
        $dataModels = $this->user->postNew($request['id']);
        $this->data['user'] = $dataModels['user'];
        $this->data['listRole'] = $dataModels['listRole'];
        return view($this->viewFolder.'.users.user.new')->with($this->data);
    }
	public function postSave(Request $request){
        //dd($request->all());
        return $this->user->postSave($request->all());
    }
	public function postDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        return $this->user->postDelete($listId);
    }

    public function postUserTaken(Request $request){
        $name = isset($request['user_name']) ? $request['user_name']:'';
        $result = $this->user->cheekUserName($name);

        return json_encode(array('success' =>$result['success']));
    }
	public function getChangePassword(){
        return view($this->viewFolder.'.users.user.change-password')->with($this->data);
    }
    public function postSaveChangePassword(Request $request){
       return $this->user->postSaveChangeUserPassword($this->userSession->id,$request->password,$request->passwordNew);
    }
}
