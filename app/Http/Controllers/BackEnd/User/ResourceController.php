<?php 
namespace App\Http\Controllers\BackEnd\User;

use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\User\ResourceModel;

class ResourceController extends BackendController {

    public function __construct(){
        parent::__construct();
		$this->resource = new ResourceModel();
    }

    public function getIndex(){
        return view($this->viewFolder.'.users.resource.index')->with($this->data);
    }
	public function postIndex(Request $request){
        return $this->resource->getDataGrid($request->all());
    }
	public function postNew(Request $request){
		$this->data['id'] = $request['id'];
		$dataResponse = $this->resource->postNew($request['id']);
        $this->data['authentication'] = $dataResponse['authentication'];
        $this->data['listAuthentication'] = $dataResponse['listAuthentication'];
        return view($this->viewFolder.'.users.resource.new')->with($this->data);
    }
	public function postSave(Request $request){
        return $this->resource->postSave($request->all());
    }
	public function postDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        return $this->resource->postDelete($listId);
    }
}
