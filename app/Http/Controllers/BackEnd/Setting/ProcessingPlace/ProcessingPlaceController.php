<?php 
namespace App\Http\Controllers\BackEnd\Setting\ProcessingPlace;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\Setting\ProcessingPlace\processingplaceModel;

class ProcessingPlaceController extends BackendController {

    public function __construct(){
        parent::__construct();
		$this->processingplace = new ProcessingPlaceModel();
    }
    public function getIndex(){
        return view($this->viewFolder.'.setting.processing-place.index')->with($this->data);
    }
	public function postIndex(Request $request){
        return $this->processingplace->getDataGrid($request->all());
    }
	public function postNew(Request $request){
        return view($this->viewFolder.'.setting.processing-place.new')->with($this->data);
    }
    public function postEdit(Request $request){
        $this->data['row'] = $this->processingplace->getDataByRowId($request['id']);
        return view($this->viewFolder.'.setting.processing-place.new')->with($this->data);
    }
	public function postSave(Request $request){
        return $this->processingplace->postSave($request->all());
    }
	public function postDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        return $this->processingplace->postDelete($listId);
    }

}
