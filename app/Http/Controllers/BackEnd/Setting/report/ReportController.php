<?php namespace App\Http\Controllers\BackEnd\Setting\report;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\Setting\Report\ReportModel;
use Excel;

class ReportController extends BackendController {
    public function __construct(){
        parent::__construct();
		$this->report = new ReportModel();
    }

    public function getIndex(){
        $ministry = $this->report->getDataByRowIdMinistryCode();
        $this->data['ministry'] = json_encode($ministry);
        $kindofexpend = $this->report->getDataByKindofExpends();
        $this->data['kindofexpend'] = json_encode($kindofexpend);
        $this->data['inputUrl'] = $this->report->getSectionName();
        return view($this->viewFolder.'.setting.report.index')->with($this->data);
    }

    public function postIndex(Request $request){
        //dd($request->all());
    	return $this->report->getDataGrid($request->all());
    }

    public function postNew(Request $request){

        return view($this->viewFolder.'.setting.report.new')->with($this->data);
    }

//    public function postExport(request $request){
//        $data = $this->report->getExport($request->all());
//        $path = public_path().'/excel/Budget.xls';
//        //dd($data);
//        Excel::load($path, function($reader) use($data){
//            $select_sheet = Excel::selectSheetsByIndex(0)->load(public_path().'/excel/Budget.xls')->get();
//            $count_sheet = count($select_sheet);
//            $count_sheet = $count_sheet + 2;
//            //dd($count_sheet);
//            $reader->setActiveSheetIndex(0);
//            foreach ($data as $key => $values) {
//                //dd(intval(str_replace(',', '', $values->money)));
//                $reader->getActiveSheet()->setCellValue('A'.($key + $count_sheet), $values->id_doc);
//                $reader->getActiveSheet()->setCellValue('B'.($key + $count_sheet), $values->date != NULL ? date('d/m/Y',strtotime($values->date)):'');
//                $reader->getActiveSheet()->setCellValue('C'.($key + $count_sheet), $values->id_mef);
//                $reader->getActiveSheet()->setCellValue('D'.($key + $count_sheet), $values->chapter);
//                $reader->getActiveSheet()->setCellValue('E'.($key + $count_sheet), intval(str_replace(',', '', $values->money)));
//                $reader->getActiveSheet()->setCellValue('G'.($key + $count_sheet), $values->kindof_expend);
//                $reader->getActiveSheet()->setCellValue('J'.($key + $count_sheet), intval(str_replace(',', '', $values->outcome)));
//                $reader->getActiveSheet()->setCellValue('S'.($key + $count_sheet), intval(str_replace(',', '', $values->result)));
//                $reader->getActiveSheet()->setCellValue('T'.($key + $count_sheet), $values->objectiv_outcome);
//            }
//        })->store('xls',public_path().'/excel/',false)->export('xls');
//    }

    public function postExport(Request $request){
        //dd($request->all());
        $data = $this->report->getExport($request->all());
        Excel::create('Budget'. '-'. date('Y-m-d'), function($excel) use ($data) {
            $excel->sheet('Budget', function($sheet) use ($data) {
                $data_cell=array();
                $row = array();
                $text = "របាយការណ៍សំណុំឯកសារ";
                $this->data['text'] = $text;
                $this->data['row'] = $row;
                foreach ($data as $key => $values) {
                    $data_cell[$key]["ល.រ"] = $key + 1;
                    $data_cell[$key]["លេខលិខិត"] = $values->id_doc;
                    $data_cell[$key]["កាលបរិច្ឆេទ"] = $values->date != NULL ? date('d/m/Y',strtotime($values->date)):'';
                    $data_cell[$key]["លេខកូដក្រសួង"] = $values->id_mef;
                    $data_cell[$key]["ជំពូក"] = $values->chapter;
                    $data_cell[$key]["ជំពូក១"] = $values->chapter1;
                    $data_cell[$key]["ជំពូក២"] = $values->chapter2;
                    $data_cell[$key]["ជំពូក៣"] = $values->chapter3;
                    $data_cell[$key]["ទឹកប្រាក់ស្នើសុំ"] = $values->money;
                    $data_cell[$key]["ប្រាក់"] = $values->typemoney;
                    $data_cell[$key]["ទឹកប្រាក់ស្នើសុំ១"] = $values->money1;
                    $data_cell[$key]["ទឹកប្រាក់ស្នើសុំ២"] = $values->money2;
                    $data_cell[$key]["ទឹកប្រាក់ស្នើសុំ៣"] = $values->money3;
                    $data_cell[$key]["ទឹកប្រាក់ផ្តល់ជូន"] = $values->outcome;
                    $data_cell[$key]["ប្រាក់"] = $values->typeoutcome;
                    $data_cell[$key]["ទឹកប្រាក់ផ្តល់ជូន១"] = $values->outcome1;
                    $data_cell[$key]["ទឹកប្រាក់ផ្តល់ជូន២"] = $values->outcome2;
                    $data_cell[$key]["ទឹកប្រាក់ផ្តល់ជូន៣"] = $values->outcome3;
                    $data_cell[$key]["សន្សំ"] = $values->result;
                    $data_cell[$key]["សន្សំ១"] = $values->result1;
                    $data_cell[$key]["សន្សំ២"] = $values->result2;
                    $data_cell[$key]["សន្សំ៣"] = $values->result3;
                    $data_cell[$key]["ប្រភេទចំណាយ"] = $values->kindof_expend;
                    $data_cell[$key]["ប្រភេទចំណាយ១"] = $values->kindof_expend1;
                    $data_cell[$key]["ប្រភេទចំណាយ២"] = $values->kindof_expend2;
                    $data_cell[$key]["ប្រភេទចំណាយ៣"] = $values->kindof_expend3;
                    $data_cell[$key]["កម្មវត្ថុចំណាយ"] = $values->objectiv_outcome;
                    $data_cell[$key]["ផ្សេងៗ"] = $values->note;
                }
                $sheet->fromArray($data_cell);
                $sheet->setFontFamily('Khmer MEF1');
                $countDataPlush1 = count($data) + 1;
                $sheet->setBorder('A1:AA1'.$countDataPlush1, 'thin');
                $sheet->row(1, function($row) {
                    $row->setBackground('#ff6600');
                });

                $sheet->prependRow(1, array(
                    $this->data['text'] . chr(10)
                ));
                $sheet->mergeCells('A1:AA1','center');
                $sheet->setHeight(1, 60);
                $sheet->cell('A1:I1', function($cell) {
                    $cell->setFontFamily('Khmer MEF2');
                    $cell->setValignment('center');
                });
                $sheet->cell('A2:A'.($countDataPlush1 +1 ), function($cell) {
                    $cell->setAlignment('center');
                });
            });
            $excel->getActiveSheet()->getStyle('A1:I'.$excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        })->export('xls');
    }

}
