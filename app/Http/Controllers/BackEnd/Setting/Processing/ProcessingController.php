<?php
namespace App\Http\Controllers\BackEnd\Setting\Processing;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\Setting\Processing;

class ProcessingController extends BackendController {

    public function __construct(){
        parent::__construct();
		$this->processing = new ProcessingModel();
    }

    public function getIndex(){
        return view($this->viewFolder.'.document.index')->with($this->data);
    }

    public function postIndex(Request $request){
        dd($request->all());
    	return $this->processing->getDataGrid($request->all());
    }
 
    public function postSave(Request $request){
        //dd($request->all());
        return $this->processing->postSave($request->all());
    }
    
    public function postEdit(Request $request){
		$this->data['row'] = $this->processing->getDataByRowId($request['id']);
        return view($this->viewFolder.'.document.processing')->with($this->data);
    }
    
    public function postDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        return $this->processing->postDelete($listId);
    }



}
