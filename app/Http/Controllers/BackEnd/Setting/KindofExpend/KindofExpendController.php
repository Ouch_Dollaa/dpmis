<?php 
namespace App\Http\Controllers\BackEnd\Setting\KindofExpend;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\Setting\KindofExpend\KindofExpendModel;

class KindofExpendController extends BackendController {

    public function __construct(){
        parent::__construct();
		$this->kindofexpend = new KindofExpendModel();
    }

    public function getIndex(){
        return view($this->viewFolder.'.setting.kind-of-expend.index')->with($this->data);
    }
	
	public function postIndex(Request $request){
        return $this->kindofexpend->getDataGrid($request->all());
    }
	public function postNew(Request $request){
        return view($this->viewFolder.'.setting.kind-of-expend.new')->with($this->data);
    }
    public function postEdit(Request $request){
        $this->data['row'] = $this->kindofexpend->getDataByRowId($request['id']);
        return view($this->viewFolder.'.setting.kind-of-expend.new')->with($this->data);
    }
	public function postSave(Request $request){
        return $this->kindofexpend->postSave($request->all());
    }
	public function postDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        return $this->kindofexpend->postDelete($listId);
    }

}
