<?php namespace App\Http\Controllers\BackEnd\Setting\Permission;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\Setting\Permission\PermissionModel;

class PermissionController extends BackendController {

    public function __construct(){
        parent::__construct();
		$this->permission = new PermissionModel();
    }

    public function getIndex(){
        $useranme = $this->permission->getDataByRowIdUser();
        $this->data['username'] = json_encode($useranme);
        return view($this->viewFolder.'.setting.permission.index')->with($this->data);
    }

    public function postIndex(Request $request){
        //dd($request->all());
    	return $this->permission->getDataGrid($request->all());
    }
 
    public function postSave(Request $request){
        //dd($request->all());
        return $this->permission->postSave($request->all());
    }

    public function postNew(Request $request){
        $useranme = $this->permission->getDataByRowIdUser();
        $this->data['username'] = json_encode($useranme);
        $ministry = $this->permission->getDataByRowIdMinistryCode();
        $this->data['ministry'] = json_encode($ministry);
		$this->data['row'] = $this->permission->getDataByRowId($request['id']);
        return view($this->viewFolder.'.setting.permission.new')->with($this->data);
    }
    
    public function postDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        return $this->permission->postDelete($listId);
    }

}
