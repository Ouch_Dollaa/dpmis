<?php 
namespace App\Http\Controllers\BackEnd\Setting\MinistryCode;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\Setting\MinistryCode\MinistryCodeModel;

class MinistryCodeController extends BackendController {

    public function __construct(){
        parent::__construct();
		$this->ministry = new MinistryCodeModel();
    }

    public function getIndex(){
        //$this->data['inputUrl'] = $this->ministry->getProvinceName();
        return view($this->viewFolder.'.setting.ministry-code.index')->with($this->data);
    }
	
	public function postIndex(Request $request){
        return $this->ministry->getDataGrid($request->all());
    }
	public function postNew(Request $request){
        return view($this->viewFolder.'.setting.ministry-code.new')->with($this->data);
    }
    public function postEdit(Request $request){
        $this->data['row'] = $this->ministry->getDataByRowId($request['id']);
        return view($this->viewFolder.'.setting.ministry-code.new')->with($this->data);
    }
	public function postSave(Request $request){
        return $this->ministry->postSave($request->all());
    }
	public function postDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        return $this->ministry->postDelete($listId);
    }

}
