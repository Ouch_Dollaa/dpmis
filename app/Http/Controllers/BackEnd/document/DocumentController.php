<?php namespace App\Http\Controllers\BackEnd\document;
use App\Http\Controllers\BackendController;
use Illuminate\Http\Request;
use App\Models\BackEnd\Document\DocumentModel;

class DocumentController extends BackendController {
    public function __construct(){
        parent::__construct();
		$this->document = new DocumentModel();
    }

    public function getIndex(){
        $this->data['inputUrl'] = $this->document->getSectionName();
        return view($this->viewFolder.'.document.index')->with($this->data);
    }

    public function postIndex(Request $request){
    	return $this->document->getDataGrid($request->all());
    }

    public function postNew(Request $request){
        $id_mef = $this->document->getDataByRowIdMef();
        $this->data['id_mef'] = json_encode($id_mef);
        $kindofexpend = $this->document->getDataByKindofExpends();
        $this->data['kindofexpend'] = json_encode($kindofexpend);
        return view($this->viewFolder.'.document.new')->with($this->data);
    }

    public function postProcess(Request $request){
        //dd($request->id);
        $query_processing = $this->document->getDataByProcessing($request->id);
        $newprocess = $this->document->getDataByNewProcess();
        $this->data['id_doc'] = $request->id;
        $this->data['query_processing'] = $query_processing;
        $this->data['new_process'] = json_encode($newprocess);
        return view($this->viewFolder.'.document.processing')->with($this->data);
    }

    public function postShow(Request $request){
        //dd($request->id_doc);
        $query_processing = $this->document->getDataByProcessing($request->id_doc);
        return $query_processing;
    }

    public function getPrint($id){
        $this->data['document'] = $this->document->getDataByRowId_Document($id);
        $this->data['processing'] = $this->document->getDataByProcessing($id);
        return view($this->viewFolder.'.document.print')->with($this->data);
    }
 
    public function postSave(Request $request){
        //dd($request->all());
        return $this->document->postSave($request->all());
    }

    public function postProcessingSave(Request $request){
        //dd($request->all());
        return $this->document->postProcssingSave($request->all());
    }

    public function postProcessingDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        //dd($listId);
        return $this->document->postProcssingDelete($listId);
    }

    public function postCopy(Request $request){
        $this->data['row'] = $this->document->getDataByRowId($request['id']);
        $this->data['row']->id = '';
        $id_mef = $this->document->getDataByRowIdMef();
        $this->data['id_mef'] = json_encode($id_mef);
        $kindofexpend = $this->document->getDataByKindofExpends();
        $this->data['kindofexpend'] = json_encode($kindofexpend);
        return view($this->viewFolder.'.document.new')->with($this->data);
    }
    
    public function postEdit(Request $request){
		$this->data['row'] = $this->document->getDataByRowId($request['id']);
        $id_mef = $this->document->getDataByRowIdMef();
        $this->data['id_mef'] = json_encode($id_mef);
        $kindofexpend = $this->document->getDataByKindofExpends();
        $this->data['kindofexpend'] = json_encode($kindofexpend);
        return view($this->viewFolder.'.document.new')->with($this->data);
    }
    
    public function postDelete(Request $request){
        $listId = isset($request['id']) ? $request['id']:'';
        return $this->document->postDelete($listId);
    }
}
