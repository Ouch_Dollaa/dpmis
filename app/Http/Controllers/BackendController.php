<?php namespace App\Http\Controllers;
use App\Models\UserAuthorizeModel;
use App\libraries\Tool;
use Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Models\BackEnd\AuthModel;
use Illuminate\Support\Facades\Request;

class BackendController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $data = array();
    function __construct() {
        $this->userSession   = session('sessionUser');
        $this->viewFolder = "back-end";

        //User has not been logged into system
        //dd($this->userSession);
        if($this->userSession == null){
            Redirect::to('/auth')->send();
        }

		$this->Tool = new Tool();
        $this->data['messages'] = Config::get('messages');
        $this->data['constant'] = Config::get('constant');

        $this->data['segment'] = array(
            'two'       => Request::segment(2),
            'three'     => Request::segment(3),
            'four'      => Request::segment(4),
        );
		//Get tree menu left & tree menuId
        $this->permissionObj = new UserAuthorizeModel();
		$ajaxRequestJson = Request::input('ajaxRequestJson');
        $ajaxRequestHtml = Request::input('ajaxRequestHtml');
        if($ajaxRequestJson == null && $ajaxRequestHtml == null ){
            $this->data['treeMenu'] = $this->permissionObj->getTreeMenuLeft();
			$this->data['treeMenuId'] = $this->permissionObj->getTreeMenuIdByUrl();
            
        }

		//Check permission
        $statusPermission = $this->permissionObj->userAuthorize();
		if($statusPermission['code'] == 0){
            if($statusPermission['message'] == 'no-url'){
                if($ajaxRequestJson != null){
                    echo json_encode(array("code" => 0, "message" => "Page not found", "data" => "data"));
                    exit();
                }else{
                    if($ajaxRequestHtml != null){
                        exit(view('errors.404')->with($this->data));
                    }else{
                        exit(view('errors.404')->with($this->data));
                    }
                }
            }else{
                if($ajaxRequestJson != null){
                    $this->data['noPermission'] = $this->data['messages']['noPermission'];
                    echo json_encode(array("code" => 0, "message" => $this->data['messages']['noPermission'], "data" => "data"));
                    exit();
                }else{
                    if($ajaxRequestHtml != null){
                        $this->data['noPermission'] = $this->data['messages']['noPermission'];
                        exit(view('errors.no-permission-ajax-request')->with($this->data));
                    }else{
                        $this->data['noPermission'] = $this->data['messages']['noPermission'];
                        exit(view('errors.no-permission-html-request')->with($this->data));
                    }
                }
            }
        }
    }

}
