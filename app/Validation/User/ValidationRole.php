<?php

namespace App\Validation\User;

use Validator;

class ValidationRole {

    public function __construct() {

    }

    public function validationSaveEdit(array $data){
        if($data['id'] == 0){
            return Validator::make($data, [
                'role' 		=> 'required|min:2|max:255|unique:tbl_master_role'
            ]);
        }else{
            return Validator::make($data, [
                'role' 		=> 'required|min:2|max:255'
            ]);
        }
    }
}

?>