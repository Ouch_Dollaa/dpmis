<?php
namespace App\Validation\User;
use Validator;

class ValidationUser {

    public function __construct() {

    }

    public function validationSaveEdit(array $data){
        if($data['id'] == 0){
			$rules = array(
				'role' 				=> 'required',
				'password' 			=> 'required:min:3',
				'user_name'			=> 'required|min:2|max:255|unique:tbl_master_user',
				'avatar'			=> 'mimes:jpeg,bmp,png'
			);
            return Validator::make($data, $rules);
        }else{
            return Validator::make($data, [
                'role' 				=> 'required',
				'user_name'			=> 'required|min:2|max:255',
				'avatar'			=> 'mimes:jpeg,bmp,png'
            ]);
        }
    }
}

?>