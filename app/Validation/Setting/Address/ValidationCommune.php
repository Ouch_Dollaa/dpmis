<?php

namespace App\Validation\Setting\Address;
use Validator;

class ValidationCommune {

    public function __construct() {

    }

    public function validationSaveEdit(array $data){
        if($data['id'] == 0){
            return Validator::make($data, [
                'commune' 	=> 'required|min:2|max:255',
                'province' 	=> 'required',
                'district' 	=> 'required'
            ]);
        }else{
            return Validator::make($data, [
                'commune' 	=> 'required|min:2|max:255',
                'province' 	=> 'required',
                'district' 	=> 'required'
            ]);
        }
    }
}

?>