<?php

namespace App\Validation\Setting\Address;
use Validator;

class ValidationDistrict {

    public function __construct() {

    }

    public function validationSaveEdit(array $data){
        if($data['id'] == 0){
            return Validator::make($data, [
                'name' 		=> 'required|min:2|max:255|unique:mef_district',
                'province' 	=> 'required'
            ]);
        }else{
            return Validator::make($data, [
                'name' 		=> 'required|min:2|max:255',
                'province' 	=> 'required'
            ]);
        }
    }
}

?>