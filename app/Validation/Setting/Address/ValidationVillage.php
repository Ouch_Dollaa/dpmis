<?php

namespace App\Validation\Setting\Address;
use Validator;

class ValidationVillage {

    public function __construct() {

    }

    public function validationSaveEdit(array $data){
        if($data['id'] == 0){
            return Validator::make($data, [
                'village' 	=> 'required|min:2|max:255',
                'commune' 	=> 'required',
                'district' 	=> 'required',
                'province' 	=> 'required'
            ]);
        }else{
            return Validator::make($data, [
                'village' 	=> 'required|min:2|max:255',
                'commune' 	=> 'required',
                'district' 	=> 'required',
                'province' 	=> 'required'
            ]);
        }
    }
}

?>