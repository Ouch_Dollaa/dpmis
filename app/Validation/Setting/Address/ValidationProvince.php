<?php

namespace App\Validation\Setting\Address;
use Validator;

class ValidationProvince {

    public function __construct() {

    }

    public function validationSaveEdit(array $data){
        if($data['id'] == 0){
            return Validator::make($data, [
                'name' 		=> 'required|min:2|max:255|unique:mef_province'
            ]);
        }else{
            return Validator::make($data, [
                'name' 		=> 'required|min:2|max:255'
            ]);
        }
    }
}

?>