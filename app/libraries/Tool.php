<?php

namespace App\libraries;
use Config;

class Tool {

    public function __construct() {
        $this->constant = Config::get('constant');
    }

    // Random String
    public	function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
        for ($s = '', $cl = strlen($c) - 1, $i = 0; $i < $l; $s .= $c[ mt_rand(0, $cl) ], ++$i) ;
        return $s;
    }
	public function PPMISFormatId($format,$yourString){
		$string = sprintf("%05d",$yourString);
		return $format.$string;
	}
    public function dayFormat($now){
		$len=strlen($now);
		$newString="";
		for($i=1;$i<=$len;$i++){
			$number=substr($now,0,1);
			$now=substr($now,1);
			if($number=='1'){
				$newString = $newString."១";
			}
			else if($number=='2'){
				$newString = $newString."២";
			}
			else if($number=='3'){
				$newString = $newString."៣";
			}
			else if($number=='4'){
				$newString = $newString."៤";
			}
			else if($number=='5'){
				$newString = $newString."៥";
			}
			else if($number=='6'){
				$newString = $newString."៦";
			}
			else if($number=='7'){
				$newString = $newString."៧";
			}
			else if($number=='8'){
				$newString = $newString."៨";
			}
			else if($number=='9'){$newString = $newString."៩";
			}
			else if($number=='0'){
				$newString = $newString."០";
			}
			else{
				$newString = $newString.$number;
			}
		}
		return $newString;
	}

	public function monthFormat($number){
			$newString="";
			if($number=='1' || $number=='01'){
				$newString = "មករា"; 
			}
			else if($number=='2' || $number=='02'){
				$newString = "កុម្ភះ";
			}
			else if($number=='3' || $number=='03'){
				$newString = "មិនា";
			}
			else if($number=='4' || $number=='04'){
				$newString = "មេសា";
			}
			else if($number=='5' || $number=='05'){
				$newString = "ឧសភា";
			}
			else if($number=='6' || $number=='06'){
				$newString = "មិថុនា";
			}
			else if($number=='7' || $number=='07'){
				$newString = "កក្កដា";
			}
			else if($number=='8' || $number=='08'){
				$newString = "សីហា";
			}
			else if($number=='9' || $number=='09'){
				$newString = "កញ្ញា";
			}
			else if($number=='10'){
				$newString = "តុលា";
			}
			else if($number=='11'){
				$newString = "វិច្ឆិកា";
			}
			else if($number=='12'){
				$newString = "ធ្នូ";
			}
			else{
				$newString = $newString;
			}
	
		return $newString;
	}
}

?>