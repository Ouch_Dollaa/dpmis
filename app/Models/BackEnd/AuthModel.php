<?php

namespace App\Models\BackEnd;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Config;
use Illuminate\Support\Facades\Redirect;
use App\Validation\ValidationAuth;
use Illuminate\Support\Facades\Request;

class AuthModel
{

    public function __construct()
    {
        $this->messages = Config::get('messages');
		$this->constant = Config::get('constant');
        $this->Validation = new ValidationAuth();
    }

    public function postLogin($data){
        $user = DB::table('tbl_master_user')->where('user_name', strtolower($data['user_name']))->first();
//        Session::put('sessionUser', $user);
//        flash()->success($this->messages['loginSuccess']);
//        return redirect($this->constant['secretRoute'].'/dashboard');
        if($user == null){
            return Redirect::back()
                ->withInput()
                ->withErrors([
                    'credentials' => $this->messages['loginError']
                ]);
        }else{
            if($user->active != 1){
                return Redirect::back()
                    ->withInput()
                    ->withErrors([
						'credentials' => $this->messages['userDisable']
                    ]);
            }
        }
        if (Hash::check($data['password'], $user->password)){
            Session::put('sessionUser', $user);
            flash()->success($this->messages['loginSuccess']);

			//Update user datetime logged in
			DB::table('tbl_master_user')
                ->where('id', $user->id)
                ->update(['last_login_date'   => date('Y-m-d H:i:s')]);

            return redirect($this->constant['secretRoute'].'/dashboard');
        }else{
            return Redirect::back()
                ->withInput()
                ->withErrors([
                    'credentials' => $this->messages['loginError']
                ]);
        }
    }

}
?>