<?php

namespace App\Models\BackEnd\Setting\Permission;
use Illuminate\Support\Facades\DB;
use Config;

class PermissionModel
{
    public function __construct()
    {
		$this->messages = Config::get('messages');
    }

    public function getDataGrid($dataRequest){
        $page = $dataRequest['pagenum'] ? intval($dataRequest['pagenum']) : 0;
        $limit = $dataRequest['pagesize'] ? intval($dataRequest['pagesize']) : $this->constant['pageSize'];
        $sort = isset($dataRequest['sortdatafield']) ? strval($dataRequest['sortdatafield']) : "id";
        $order = isset($dataRequest['sortorder']) ? strval($dataRequest['sortorder']) : "ASC";
        $offset = $page*$limit;
        $filtersCount = isset($dataRequest['filterscount']) ? intval($dataRequest['filterscount']) : 0;
        DB::select(DB::raw("call get_permision()"));
        $listDb = DB::table('tem');
        $total = count($listDb->get());
        if ($filtersCount > 0) {
            for ($i = 0; $i < $filtersCount; $i++) {
                $arrFilterName = isset($dataRequest['filterdatafield' . $i]) ? $dataRequest['filterdatafield' . $i] : '';
                $arrFilterValue = isset($dataRequest['filtervalue' . $i]) ? strval($dataRequest['filtervalue' . $i]) : '';
                switch ($arrFilterName) {
                    case 'id':
                        $active = $arrFilterValue == $this->constant['id'] ? 1 : 0;
                        $listDb = $listDb->where('id', $active);
                        break;
                    case 'username':
                        $listDb = $listDb->where('username', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'ministry':
                        $listDb = $listDb->where('ministry', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'print':
                        $listDb = $listDb->where('print', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'download':
                        $listDb = $listDb->where('download', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    default:
                        #Code...
                        break;
                }
            }
            $total = count($listDb->get());
        }
        $listDb = $listDb
            ->orderBy($sort, $order)
            ->take($limit)
            ->skip($offset);
        $listDb = $listDb->get();
        $list = array();
        foreach($listDb as $row){
            $list[] = array(
                "id"           	=> $row->id,
                "username"      => $row->username,
                "ministry"   	=> $row->ministry,
                "print"  	    => $row->print,
                "download"  	=> $row->download
            );
        }
        DB::statement("drop table tem");
        return json_encode(array('total'=>$total,'items'=>$list));
    }

    
    public function postSave($data){
        //dd($data);
        $insertarray = array();
        foreach (explode(',',$data['listBox']) as $id){
            DB::table('tbl_permision')->where('username',$data['username'])->where('ministry',$id)->delete();
            $insertarray[] = array(
                'username'=>$data['username'],
                'ministry'=>$id,
                'print'=>$data['print'],
                'download'=>$data['download']
            );
        }
        //dd($insertarray);
        if (count($insertarray) and $data['username'] <> ''){
            DB::table('tbl_permision')->insert($insertarray);
            return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
        }else{
            return array("code" => 0, "message" => $this->messages['itemsDeleted']);
        }
    }

    public function postDelete($listId){
        $countDeleted = 0;
        foreach ($listId as $id){
            DB::table('tbl_permision')->where('id',$id)->delete();
        }
        if($countDeleted >= 1 && count($listId) > 1){
            return array("code" => 0, "message" => $this->messages['itemsDeleted']);
        }else if($countDeleted == 1 && count($listId) == 1){
            return array("code" => 1, "message" => $this->messages['userInUsed']);
        }else{
            return array("code" => 2,"message" => $this->messages['success']);
        }
    }

    public function getDataByRowId($id){
        return DB::table('tbl_permision')->where('id', $id)->first();
    }

    

    public function getDataByRowIdUser(){
        $sql_query = DB::table('tbl_master_user')->where('active',1)->get();
        $arrListRole = array(array('text' => '', "value" => 0));
        foreach ($sql_query as $row) {
            $arrListRole[] = array(
                'text' => $row->user_name,
                "value" => $row->id
            );
        }
        return $arrListRole;
    }

    public function getDataByRowIdMinistryCode(){
        $sql_query = DB::table('tbl_ministry')->where('active',1)->get();
        $arrListRole = array();
        foreach ($sql_query as $row) {
            $arrListRole[] = array(
                'text' => $row->ministry,
                "value" => $row->ministrycode
            );
        }
        return $arrListRole;
    }
}
?>