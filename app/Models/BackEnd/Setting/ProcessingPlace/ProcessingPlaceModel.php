<?php

namespace App\Models\BackEnd\Setting\ProcessingPlace;
use Illuminate\Support\Facades\DB;
use Config;
use App\libraries\Tool;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class processingplaceModel
{
    public function __construct()
    {
		$this->messages = Config::get('messages');
    }

    public function getDataGrid($dataRequest){
        $page = $dataRequest['pagenum'] ? intval($dataRequest['pagenum']) : 0;
        $limit = $dataRequest['pagesize'] ? intval($dataRequest['pagesize']) : $this->constant['pageSize'];
        $sort = isset($dataRequest['sortdatafield']) ? strval($dataRequest['sortdatafield']) : "id";
        $order = isset($dataRequest['sortorder']) ? strval($dataRequest['sortorder']) : "ASC";
        $offset = $page*$limit;
        $filtersCount = isset($dataRequest['filterscount']) ? intval($dataRequest['filterscount']) : 0;
        $listDb = DB::table('tbl_process');
        $total = count($listDb->get());
        if ($filtersCount > 0) {
            for ($i = 0; $i < $filtersCount; $i++) {
                $arrFilterName = isset($dataRequest['filterdatafield' . $i]) ? $dataRequest['filterdatafield' . $i] : '';
                $arrFilterValue = isset($dataRequest['filtervalue' . $i]) ? strval($dataRequest['filtervalue' . $i]) : '';
                switch ($arrFilterName) {
                    case 'active':
                        $active = $arrFilterValue == $this->constant['active'] ? 1 : 0;
                        $listDb = $listDb->where('active', $active);
                        break;
                    case 'new_process':
                        $listDb = $listDb->where('new_process', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    
                    case 'orderNumber':
                        $listDb = $listDb->where('orderNumber', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    default:
                        #Code...
                        break;
                }
            }
            $total = count($listDb->get());
        }
        $listDb = $listDb
            ->orderBy($sort, $order)
            ->take($limit)
            ->skip($offset);
        $listDb = $listDb->get();
        $list = array();
        foreach($listDb as $row){
            $list[] = array(
                "id"           	=> $row->id,
                "new_process"   => $row->new_process,
                "orderNumber"  	=> $row->orderNumber,
                "active"  	    => $row->active
            );
        }
        return json_encode(array('total'=>$total,'items'=>$list));
    }

    
    public function postSave($data){
        //dd($data);
        $inputData = array(
            'new_process'          =>$data['new_process'],
            'orderNumber'           =>$data['orderNumber'],
            'active'                =>$data['active']
        );
        if($data['id'] == 0){
            DB::table('tbl_process')->insert($inputData);
        }else{
            DB::table('tbl_process')->where('id', $data['id'])->update($inputData);
        }
        return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
    }
    public function postDelete($listId){
        $countDeleted = 0;
        foreach ($listId as $id){
            DB::table('tbl_process')->where('id',$id)->delete();
        }
        if($countDeleted >= 1 && count($listId) > 1){
            return array("code" => 0, "message" => $this->messages['itemsDeleted']);
        }else if($countDeleted == 1 && count($listId) == 1){
            return array("code" => 1, "message" => $this->messages['userInUsed']);
        }else{
            return array("code" => 2,"message" => $this->messages['success']);
        }
    }

    public function getDataByRowId($id){
        return DB::table('tbl_process')->where('id', $id)->first();
    }
    
}
?>