<?php

namespace App\Models\BackEnd\Setting\Report;
use Illuminate\Support\Facades\DB;
use Config;
use App\libraries\Tool;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Excel;

class ReportModel
{
	public function __construct()
    {
        $this->userSession = session('sessionUser');
		$this->messages = Config::get('messages');
        $this->Tool = new Tool();
    }
	
	public function getDataGrid($dataRequest){
        $page = $dataRequest['pagenum'] ? intval($dataRequest['pagenum']) : 0;
        $limit = $dataRequest['pagesize'] ? intval($dataRequest['pagesize']) : $this->constant['pageSize'];
        $sort = isset($dataRequest['sortdatafield']) ? strval($dataRequest['sortdatafield']) : "id_doc";
        $order = isset($dataRequest['sortorder']) ? strval($dataRequest['sortorder']) : "DESC";
        $offset = $page*$limit;
        $filtersCount = isset($dataRequest['filterscount']) ? intval($dataRequest['filterscount']) : 0;
        DB::select(DB::raw("call get_report_document()"));
        $listDb = DB::table('tem');
		$total = $listDb->count();
        if ($filtersCount > 0) {
            for ($i = 0; $i < $filtersCount; $i++) {
                $arrFilterName = isset($dataRequest['filterdatafield' . $i]) ? $dataRequest['filterdatafield' . $i] : '';
                $arrFilterValue = isset($dataRequest['filtervalue' . $i]) ? strval($dataRequest['filtervalue' . $i]) : '';
                switch ($arrFilterName) {
                    case 'date':
                        $listDb = $listDb->whereBetween('date' ,explode(",",$arrFilterValue));
                        break;
                    case 'id_mef':
                        if ($arrFilterValue != ''){
                            $listDb = $listDb->whereIn('id_mef',explode(",",$arrFilterValue));
                        }
                        break;
                    case 'kindof_expend':
                        $arrFilterValue = $arrFilterValue == '0' ? '' : $arrFilterValue;
                        $listDb = $listDb->where('id_kindof_expend', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    default:
                        #Code...
                        break;
                }
            }
            $total = count($listDb->get());
        }else{
            $listDb = $listDb->where('id_doc','<','0');
        }
//        $listDb = $listDb
//            ->orderBy($sort, $order)
//            ->take($limit)
//            ->skip($offset);
        $listDb = $listDb->get();
        $list = array();
        foreach($listDb as $row){
            $list[] = array(
                "id_doc"            => $row->id_doc,
                "date"              => $row->date,
                "id_mef"            => $row->id_mef,
                "chapter"           => $row->chapter,
                "chapter1"          => $row->chapter1,
                "chapter2"          => $row->chapter2,
                "chapter3"          => $row->chapter3,
                "money"             => $row->money,
                "typemoney"         => $row->typemoney,
                "money1"            => $row->money1,
                "money2"            => $row->money2,
                "money3"            => $row->money3,
                "outcome"           => $row->outcome,
                "typeoutcome"       => $row->typeoutcome,
                "outcome1"          => $row->outcome1,
                "outcome2"          => $row->outcome2,
                "outcome3"          => $row->outcome3,
                "result"            => $row->result,
                "result1"           => $row->result1,
                "result2"           => $row->result2,
                "result3"           => $row->result3,
                "kindof_expend"     => $row->kindof_expend,
                "kindof_expend1"    => $row->kindof_expend1,
                "kindof_expend2"    => $row->kindof_expend2,
                "kindof_expend3"    => $row->kindof_expend3,
                "objectiv_outcome"  => $row->objectiv_outcome,
                "note"              => $row->note
            );
        }
        DB::statement("drop table tem");
        return json_encode(array('total'=>$total,'items'=>$list));
    }

    public function postNew(){
        $list_section=DB::table('tbl_document')->OrderBy('id', 'DESC')->get();
        $arr = array(array("text"=>"", "value" => ""));
        foreach($list_section as $row){
            $arr[] = array(
                'text'  => $row->name, 
                "value" => $row->id
                );
        }
        return $arr;
    }

    public function getSectionName(){
        $obj =  DB::table('tbl_document')->select('id')->get();
        $arr = array();
        foreach($obj as $row){
            $arr[] = $row->id;
        }
        return json_encode($arr);
    }

    public function getExport($data){
        //dd($data);
        DB::select(DB::raw("call get_report_document()"));
        $listDb = DB::table('tem');
        if ($data['start_datetime'] != '' && $data['end_datetime'] != ''){
            $listDb = $listDb->whereBetween('date' ,array($data['start_datetime'],$data['end_datetime']));
        }
        if ($data['listBox'] != ''){
            $listDb = $listDb->whereIn('id_mef',explode(",",$data['listBox']));
        }
        if ($data['kindofexpend'] != ''){
            $arrFilterValue = $data['kindofexpend'] == '0' ? '' : $data['kindofexpend'];
            $listDb = $listDb->where('id_kindof_expend', 'LIKE', '%' . $arrFilterValue . '%');
        }
        $listDb = $listDb->get();
        DB::statement("drop table tem");
        return $listDb;
    }

    public function getDataByRowIdMinistryCode(){
        $sql_query = DB::table('tbl_ministry')->where('active',1)->get();
        $arrListRole = array();
        foreach ($sql_query as $row) {
            $arrListRole[] = array(
                'text' => $row->ministry,
                "value" => $row->ministrycode
            );
        }
        return $arrListRole;
    }

    public function getDataByKindofExpends(){
        $query = DB::table('tbl_kindof_expend')->where('active',1)->get();
        $arrList = array(array('text' => '', "value" => 0));
        foreach ($query as $row) {
            $arrList[] = array(
                'text' => $row->kindof_expend,
                "value" => $row->id
            );
        }
        return $arrList;
    }
}
?>