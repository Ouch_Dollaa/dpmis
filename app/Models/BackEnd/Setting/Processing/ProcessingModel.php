<?php

namespace App\Models\BackEnd\Setting\Processing;
use Illuminate\Support\Facades\DB;
use Config;
use App\libraries\Tool;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class ProcessingModel
{
	public function __construct()
    {
        $this->userSession   = session('sessionUser');
		$this->messages = Config::get('messages');
        $this->Tool = new Tool();
    }
	
	public function getDataGrid($dataRequest){
        $page = $dataRequest['pagenum'] ? intval($dataRequest['pagenum']) : 0;
        $limit = $dataRequest['pagesize'] ? intval($dataRequest['pagesize']) : $this->constant['pageSize'];
        $sort = isset($dataRequest['sortdatafield']) ? strval($dataRequest['sortdatafield']) : "id";
        $order = isset($dataRequest['sortorder']) ? strval($dataRequest['sortorder']) : "DESC";
        $offset = $page*$limit;
        $filtersCount = isset($dataRequest['filterscount']) ? intval($dataRequest['filterscount']) : 0;
        $listDb = DB::table('tbl_processing')->where('id_doc', 'LIKE', '8827');
        //dd($listDb);
		$total = $listDb->count();
        if ($filtersCount > 0) {
            for ($i = 0; $i < $filtersCount; $i++) {
                $arrFilterName = isset($dataRequest['filterdatafield' . $i]) ? $dataRequest['filterdatafield' . $i] : '';
                $arrFilterValue = isset($dataRequest['filtervalue' . $i]) ? strval($dataRequest['filtervalue' . $i]) : '';
                switch ($arrFilterName) {
                    case 'id':
                        $listDb = $listDb->where('id', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'id_doc':
                        $listDb = $listDb->where('id_doc', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'processing':
                        $listDb = $listDb->where('processing', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'date_process':
                        $listDb = $listDb->where('date_process', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'other':
                        $listDb = $listDb->where('other', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'isfrish':
                        $listDb = $listDb->where('isfrish', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    default:
                        #Code...
                        break;
                }
            }
            $total = count($listDb->get());
        }
        $listDb = $listDb
            ->orderBy($sort, $order)
            ->take($limit)
            ->skip($offset);
        $listDb = $listDb->get();
        $list = array();
        foreach($listDb as $row){
            $list[] = array(
                "id"                    => $row->id,
                "id_doc"                => $row->id_doc,
                "processing"            => $row->processing,
                "date_process"          => $row->date_process,
                "other"                 => $row->other,
                "isfrish"               => $row->isfrish
            );
        }
        return json_encode(array('total'=>$total,'items'=>$list));
    }

    public function postNew(){
        $list_section=DB::table('tbl_processing')->OrderBy('id', 'DESC')->get();
        $arr = array(array("text"=>"", "value" => ""));
        foreach($list_section as $row){
            $arr[] = array(
                'text'  => $row->processing,
                "value" => $row->id
                );
        }
        return $arr;
    }

    public function postSave($data){
        unset($data['_token']);
        unset($data['ajaxRequestJson']);
        if($data['id'] == 0){
            unset($data['id']);
            DB::table('tbl_processing')->insert($data);
        }else{
            DB::table('tbl_processing')->where('id', $data['id'])->update($data);
        }
        return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
    }

    public function postDelete($listId){
        $countDeleted = 0;
        foreach ($listId as $id){
            DB::table('tbl_processing')->where('id',$id)->delete();
        }
        if($countDeleted >= 1 && count($listId) > 1){
            return array("code" => 0, "message" => $this->messages['itemsDeleted']);
        }else if($countDeleted == 1 && count($listId) == 1){
            return array("code" => 1, "message" => $this->messages['userInUsed']);
        }else{
            return array("code" => 2,"message" => $this->messages['success']);
        }
    }

    public function getSectionName(){
        $obj =  DB::table('tbl_processing')->select('id')->get();
        $arr = array();
        foreach($obj as $row){
            $arr[] = $row->id;
        }
        return json_encode($arr);
    }

    public function getDataByRowId($id){
        $getData = DB::table('tbl_processing')->where('id',$id)->first();
        return $getData;
    }

    public function getDataByNewProcess(){
        $query = DB::table('tbl_process')->where('active',1)->get();
        $arrList = array(array('text' => '', "value" => 0));
        foreach ($query as $row) {
            $arrList[] = array(
                'text' => $row->new_process,
                "value" => $row->id
            );
        }
        return $arrList;
    }

}
?>