<?php

namespace App\Models\BackEnd\Document;
use Illuminate\Support\Facades\DB;
use Config;
use App\libraries\Tool;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class DocumentModel
{
	public function __construct()
    {
        $this->userSession = session('sessionUser');
		$this->messages = Config::get('messages');
        $this->Tool = new Tool();
        //dd($this->userSession);
    }
	
	public function getDataGrid($dataRequest){
        $page = $dataRequest['pagenum'] ? intval($dataRequest['pagenum']) : 0;
        $limit = $dataRequest['pagesize'] ? intval($dataRequest['pagesize']) : $this->constant['pageSize'];
        $sort = isset($dataRequest['sortdatafield']) ? strval($dataRequest['sortdatafield']) : "id";
        $order = isset($dataRequest['sortorder']) ? strval($dataRequest['sortorder']) : "DESC";
        $offset = $page*$limit;
        $filtersCount = isset($dataRequest['filterscount']) ? intval($dataRequest['filterscount']) : 0;
        DB::select(DB::raw("call get_document()"));
        if($this->userSession->role_id == 1){
            $listDb = DB::table('tem');
        }elseif($this->userSession->role_id == 2){
            $listDb = DB::table('tem')->where('username', $this->userSession->id);
        }else{
            $listDb = DB::table('tem')->where(1,0);
        }
        // dd($listDb);
		$total = $listDb->count();
        if ($filtersCount > 0) {
            for ($i = 0; $i < $filtersCount; $i++) {
                $arrFilterName = isset($dataRequest['filterdatafield' . $i]) ? $dataRequest['filterdatafield' . $i] : '';
                $arrFilterValue = isset($dataRequest['filtervalue' . $i]) ? strval($dataRequest['filtervalue' . $i]) : '';
                switch ($arrFilterName) {
                    case 'id':
                        $listDb = $listDb->where('id', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'no':
                        $listDb = $listDb->where('no', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'id_doc':
                        $listDb = $listDb->where('id_doc', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'id_mef':
                        $listDb = $listDb->where('id_mef', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'chapter':
                        $listDb = $listDb->where('chapter', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'date':
                        $listDb = $listDb->where('date', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'objectiv_outcome':
                        $listDb = $listDb->where('objectiv_outcome', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'kindof_expend':
                        $listDb = $listDb->where('kindof_expend', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'no_mandate':
                        $listDb = $listDb->where('no_mandate', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'date_mandate':
                        $listDb = $listDb->where('date_mandate', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'currency':
                        $listDb = $listDb->where('currency', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    case 'outcome':
                        $listDb = $listDb->where('outcome', 'LIKE', '%' . $arrFilterValue . '%');
                        break;
                    default:
                        #Code...
                        break;
                }
            }
            $total = count($listDb->get());
        }
        $listDb = $listDb
            ->orderBy($sort, $order)
            ->take($limit)
            ->skip($offset);
        $listDb = $listDb->get();
        $list = array();
        foreach($listDb as $row){
            $avatar = $row->avatar =='' ? '':'Download';
            $list[] = array(
                "id"                    => $row->id,
                "no"                    => $row->no,
                "id_doc"                => $row->id_doc,
                "chapter"               => $row->chapter,
                "id_mef"                => $row->id_mef,
                "date"                  => $row->date,
                "objectiv_outcome"      => $row->objectiv_outcome,
                "kindof_expend"         => $row->kindof_expend,
                "no_mandate"            => $row->no_mandate,
                "date_mandate"          => $row->date_mandate,
                "currency"              => $row->currency,
                "outcome"               => $row->outcome,
                "username"              => $row->username,
                "avatar"                => $row->avatar
            );
        }
        DB::statement("drop table tem");
        return json_encode(array('total'=>$total,'items'=>$list));
    }

    public function postNew(){
        $list_section=DB::table('tbl_document')->OrderBy('id', 'DESC')->get();
        $arr = array(array("text"=>"", "value" => ""));
        foreach($list_section as $row){
            $arr[] = array(
                'text'  => $row->name, 
                "value" => $row->id
                );
        }
        return $arr;
    }

    public function postSave($data){
        unset($data['_token']);
        unset($data['ajaxRequestJson']);
        $no = $data['no'];
        $id_doc = $data['no_docx'];
        $date = $data['currentDate'];
        $id_mef = $data['id_mef'];
        $objectiv_outcome = $data['objective'];
        $money = str_replace(',','',$data['moneyRequest']);
        $money1 = str_replace(',','',$data['moneyRequest_1']);
        $money2 = str_replace(',','',$data['moneyRequest_2']);
        $money3 = str_replace(',','',$data['moneyRequest_3']);
        $typemoney = '៛';
        $typemoney1 = '៛';
        $typemoney2 = '៛';
        $typemoney3 = '៛';
        $outcome = str_replace(',','',$data['moneyPay']);
        $outcome1 = str_replace(',','',$data['moneyPay_1']);
        $outcome2 = str_replace(',','',$data['moneyPay_2']);
        $outcome3 = str_replace(',','',$data['moneyPay_3']);
        $typeoutcome = '៛';
        $typeoutcome1 = '៛';
        $typeoutcome2 = '៛';
        $typeoutcome3 = '៛';
        $kindof_expend = $data['kindofexpend'];
        $kindof_expend1 = $data['kindofexpend_1'];
        $kindof_expend2 = $data['kindofexpend_2'];
        $kindof_expend3 = $data['kindofexpend_3'];
        $chapter = $data['chapter'];
        $chapter1 = $data['chapter_1'];
        $chapter2 = $data['chapter_2'];
        $chapter3 = $data['chapter_3'];
        $note = $data['other'];
        $note1 = $data['other_1'];
        $note2 = $data['other_2'];
        $note3 = $data['other_3'];
        $username = $this->userSession->id;
        $created = date('Y-m-d');
        $no_mandate = $data['no_mandate'];
        $date_mandate = null;
        if($data['date_mandate'] != ''){
            $date_mandate = $data['date_mandate'];
        }
        $array= array(
                         'no'               =>$no
                        ,'id_doc'           =>$id_doc
                        ,'date'             =>$date
                        ,'id_mef'           =>$id_mef
                        ,'objectiv_outcome' => $objectiv_outcome
                        ,'money'            =>$money
                        ,'money1'           => $money1
                        ,'money2'           =>$money2
                        ,'money3'           => $money3
                        ,'typemoney'        => $typemoney
                        ,'typemoney1'       => $typemoney1
                        ,'typemoney2'       =>$typemoney2
                        ,'typemoney3'       =>$typemoney3
                        ,'outcome'          =>$outcome
                        ,'outcome1'         =>$outcome1
                        ,'outcome2'         =>$outcome2
                        ,'outcome3'         =>$outcome3
                        ,'typeoutcome'      => $typeoutcome
                        ,'typeoutcome1'     => $typeoutcome1
                        ,'typeoutcome2'     =>$typeoutcome2
                        ,'typeoutcome3'     =>$typeoutcome3
                        ,'kindof_expend'    => $kindof_expend
                        ,'kindof_expend1'   =>$kindof_expend1
                        ,'kindof_expend2'   =>$kindof_expend2
                        ,'kindof_expend3'   =>$kindof_expend3
                        ,'chapter'          =>$chapter
                        ,'chapter1'         => $chapter1
                        ,'chapter2'         => $chapter2
                        ,'chapter3'         =>$chapter3
                        ,'note'             =>$note
                        ,'note1'            => $note1
                        ,'note2'            =>$note2
                        ,'note3'            =>$note3
                        ,'username'         =>$username
                        ,'created'          =>$created
                        ,'no_mandate'       =>$no_mandate
                        ,'date_mandate'     =>$date_mandate
                    );
        $paths = 'files/document/';
        $userObj = $this->getDataByRowId($data['id']);
        if (Input::hasFile('avatar')) {
            $files = Input::file('avatar');
            $size = $files->getSize();
            $extension = ".".strtolower($files->getClientOriginalExtension());
            $random_name = $this->Tool->mt_rand_str(5, '0123456789');
            $convertName = time() . "_" . $random_name . $extension;
            //Move image to folder
            $upload = $files->move($paths, $convertName);
            if($userObj != null){
                if($userObj->avatar != ""){
                    if (Storage::disk('public')->exists($userObj->avatar)){
                        Storage::disk('public')->delete($userObj->avatar);
                    }
                }
            }
            $imageUrl = $paths . $convertName;
            $array['avatar'] = $imageUrl;
        }else{
            $array['avatar'] = $userObj != null ? $userObj->avatar:'';
        }
        /* Click remove sign */
        if($data['statusRemovePicture'] == 1){
            if (Storage::disk('public')->exists($userObj->avatar)){
                $array['avatar'] = '';
                Storage::disk('public')->delete($userObj->avatar);
            }
        }
        if($data['id'] == 0){
            unset($data['id']);
            DB::table('tbl_document')->insert($array);
        }else{
            DB::table('tbl_document')->where('id', $data['id'])->update($array);
        }
        return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
    }

    public function postDelete($listId){
        $countDeleted = 0;
        foreach ($listId as $id){
            DB::table('tbl_document')->where('id',$id)->delete();
        }
        if($countDeleted >= 1 && count($listId) > 1){
            return array("code" => 0, "message" => $this->messages['itemsDeleted']);
        }else if($countDeleted == 1 && count($listId) == 1){
            return array("code" => 1, "message" => $this->messages['userInUsed']);
        }else{
            return array("code" => 2,"message" => $this->messages['success']);
        }
    }

    public function getSectionName(){
        $obj =  DB::table('tbl_document')->select('id')->get();
        $arr = array();
        foreach($obj as $row){
            $arr[] = $row->id;
        }
        return json_encode($arr);
    }

    public function getDataByRowIdFile($id){
        return DB::table('tbl_path_document')->where('id_doc', $id)->first();
    }

    public function getDataByRowId($id){
        $getData = DB::table('tbl_document')->where('id',$id)->first();
        return $getData;
    }

    public function getDataByRowId_Document($id){
        DB::select(DB::raw("call get_document_detail($id)"));
        $query = DB::table('tem')->first();
        DB::statement("drop table tem");
        return $query;
    }

    public function getDataByRowId_DocumentFile($id){
        $getData = DB::table('tbl_path_document')->where('id_doc',$id)->first();
        return $getData;
    }

    public function getDataByRowIdMef(){
        if($this->userSession->role_id == 1){
            $sql_query = DB::select(DB::raw("Select * From tbl_ministry"));
        }elseif($this->userSession->role_id == 2){
            $sql_query = DB::select(DB::raw("Select * From tbl_ministry Where ministrycode In (" . "Select ministry From tbl_permision Where username like " . $this->userSession->id . ")"));
        }
        $arrListRole = array(array('text' => '', "value" => 0));
        foreach ($sql_query as $row) {
            $arrListRole[] = array(
                'text' => $row->ministrycode.'->'.$row->ministry,
                "value" => floatval($row->ministrycode)
            );
        }
        return $arrListRole;
    }

    public function getDataByKindofExpends(){
        $query = DB::table('tbl_kindof_expend')->where('active',1)->get();
        $arrList = array(array('text' => '', "value" => 0));
        foreach ($query as $row) {
            $arrList[] = array(
                'text' => $row->kindof_expend,
                "value" => $row->id
            );
        }
        return $arrList;
    }

    public function getDataByNewProcess(){
        $query = DB::table('tbl_process')->where('active',1)->get();
        $arrList = array(array('text' => '', "value" => 0));
        foreach ($query as $row) {
            $arrList[] = array(
                'text' => $row->new_process,
                "value" => $row->id
            );
        }
        return $arrList;
    }

    public function getDataByProcessing($id_doc){
        DB::select(DB::raw("call get_processing($id_doc)"));
        $query = DB::table('tem')->get();
        DB::statement("drop table tem");
        return $query;
    }

    public function postProcssingSave($data){
        unset($data['_token']);
        unset($data['ajaxRequestJson']);
        $data['date_process'] = $data['date_process'];
        //dd($data);
        DB::table('tbl_processing')->insert($data);
        return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
    }

    public function postProcssingDelete($id){
        //dd($id);
        DB::table('tbl_processing')->Where('id',$id)->delete($id);
        return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
    }

}
?>