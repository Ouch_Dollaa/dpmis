<?php
namespace App\Models\BackEnd\Register;
use Illuminate\Support\Facades\DB;
use Config;

class RegisterModel{

	protected $table='mef_register';
	protected $fillable=[
			'username',
			'email',
			'password',
			'fullname',
			'sex',
			'birthday',
			'rollid',
			'classname',
			'section',
			'current_address',
			'phone',
			'tel',
			'status',
		];

}

?>