<?php

namespace App\Models\BackEnd\User;
use Illuminate\Support\Facades\DB;
use App\Validation\User\ValidationResource;
use Config;
use App\libraries\Tool;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;


class ResourceModel
{

    public function __construct()
    {
		$this->messages = Config::get('messages');
		$this->Validation = new ValidationResource();
		$this->Tool = new Tool();
    }
	public function getDataGrid($dataRequest){
        $listDb = DB::table('tbl_master_authenticate')->OrderBy('order','ASC')->get();
		$total = count($listDb);
        $list = array();
        foreach($listDb as $row){
            $list[] = array(
                "id"           	=> $row->id,
                "name"          => $row->name,
                "url"           => $row->url,
                "description"   => $row->description,
                "parent_id"     => $row->parent_id,
                "order"         => $row->order,
				"icon"         => $row->icon,
                "active"        => $row->active,
            );
        }
        return json_encode(array('total'=>$total,'items'=>$list));
    }
    
	
	public function postNew($id){
        $authentication = DB::table('tbl_master_authenticate')->where('id',$id)->first();
        $listAuthenticationDb = DB::table('tbl_master_authenticate')->orderBy('order','asc')->get();
        $listAuthentication = array();
        foreach($listAuthenticationDb as $row){
            $listAuthentication[] = array(
                "id"        => $row->id,
                "value"     => $row->id,
                "parentid"  => $row->parent_id,
                "text"		=> $row->name,
            );
        }
        return array("authentication" => $authentication, "listAuthentication" => $listAuthentication);
    }
	public function postSave($data){
		$paths = "files/icons/";
        /* Validation Insert Data */
        $validator = $this->Validation->validationSaveEdit($data);
        if ($validator->fails()) {
            $error = json_encode($validator->messages());
            $arrayError = json_decode($error);
            $message = "";
            foreach($data as $key => $val){
                if (isset($arrayError->{$key})) {
                    $message = $message . $arrayError->{$key}[0].  "<br>";
                }
            }
            return json_encode(array("code" => 0, "message" => $message, "data" => ""));
        }
        /* Validation Insert Data End */
		
		$rowResource = $this->getDataByRowId($data['id']);
		if (Input::hasFile('icon')) {
            $files = Input::file('icon');
            $size = $files->getSize();
            $extension = ".".strtolower($files->getClientOriginalExtension());
            $random_name = $this->Tool->mt_rand_str(5, '0123456789');
            $convertName = time() . "_" . $random_name . $extension;
			
			//Move image to folder
            $upload = $files->move($paths, $convertName);
			if($rowResource != null){
				if($rowResource->icon != ""){
					if (Storage::disk('public')->exists($rowResource->icon)){
						Storage::disk('public')->delete($rowResource->icon);
					}
				}
            }
            $imageUrl = $paths . $convertName;
            $data['avatarPic'] = $imageUrl;
        }else{
			$data['avatarPic'] = $rowResource != null ? $rowResource->icon:'';
		}
		
        if($data['id'] == 0){
            /* Save data */
			DB::table('tbl_master_authenticate')->insert([
				'name' 			=>$data['name'],
				'url' 			=>$data['url'], 
				'parent_id' 	=>$data['parent_id'], 
				'order' 		=>$data['order'],
				'icon' 			=>$data['avatarPic'],
				'description' 	=>$data['description'],
				'active'		=>$data['active']
			]);
            /* End Save data */
        }else{
            DB::table('tbl_master_authenticate')
				->where('id', $data['id'])
				->update([
					'name' 			=>$data['name'],
					'url' 			=>$data['url'], 
					'parent_id' 	=>$data['parent_id'], 
					'order' 		=>$data['order'],
					'icon' 			=>$data['avatarPic'],					
					'description' 	=>$data['description'],
					'active'		=>$data['active']
				]);
        }
		
        return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
    }

    public function postDelete($id){
	   $hasParent = DB::table('tbl_master_authenticate')->where('parent_id',$id)->count();
        if($hasParent > 0){
            return json_encode(array("code" => 0, "message" => $this->messages['cantDeleteParent'], "data" => "data"));
        }else{
			DB::table('tbl_master_authenticate')->where('id',$id)->delete();
			return array("code" => 1,"message" => $this->messages['success']);	
		}
    }

    public function getDataByRowId($id){
        return DB::table('tbl_master_authenticate')->where('id', $id)->first();
    }
	
	

}
?>