<?php

namespace App\Models\BackEnd\User;
use Illuminate\Support\Facades\DB;
use App\Validation\User\ValidationRole;
use Config;

class RoleModel
{

    public function __construct()
    {
        $this->Validation = new ValidationRole();
        $this->User = new UserModel();
		$this->messages = Config::get('messages');
		$this->table = Config::get('table');
    }

    public function getDataGrid($dataRequest){
        $page = $dataRequest['pagenum'] ? intval($dataRequest['pagenum']) : 0;
        $limit = $dataRequest['pagesize'] ? intval($dataRequest['pagesize']) : $this->constant['pageSize'];
        $sort = isset($dataRequest['sortdatafield']) ? strval($dataRequest['sortdatafield']) : "id";
        $order = isset($dataRequest['sortorder']) ? strval($dataRequest['sortorder']) : "DESC";
        $offset = $page*$limit;
        $filtersCount = isset($dataRequest['filterscount']) ? intval($dataRequest['filterscount']) : 0;
        $listDb = DB::table('tbl_master_role');
        $total = count($listDb->get());
        $listDb = $listDb
				->OrderBy($sort, $order)
				->take($limit)
				->skip($offset);
        if($filtersCount>0){
            $listDb = $listDb->where('role','LIKE','%'.$dataRequest['filtervalue0'].'%');
            $total = count($listDb->get());
        }        
        $listDb = $listDb->get();
        $list = array();
        foreach($listDb as $row){
            $list[] = array(
                "id"           => $row->id,
                "role"         => $row->role,
                "description"  => $row->description,
                "active"       => $row->active,
            );
        }
        return json_encode(array('total'=>$total,'items'=>$list));
    }

    public function postNew($id){
        $role = $this->getDataByRowId($id);
        $listAuthentication =DB::table('tbl_master_authenticate')->OrderBy("order", "asc")->get();
        $selectedAuthentication = DB::table('tbl_master_autenticate_to_role')->where('role_id',$id)->get();
		
        $listString = '';
        foreach($selectedAuthentication as $key=>$val){
            $listString.= $val->authenticate_id.',';
        }
        $listString = substr($listString,0,-1);
        $arr = array();
        foreach($listAuthentication as $authentication){
            $arr[] = array(
                    "id"        => $authentication->id,
                    "parentid"  => $authentication->parent_id,
                    "text"      => $authentication->name,
                    "value"     => $authentication->id
                    );
        }
        $listAuthentication = $arr;
        return array(
			"role" 						=> $role, 
			"allAuthentication" 		=> $listAuthentication,
			"selectedAuthentication" 	=> $listString
		);
    }
	
    public function postSave($data){
        /* Validation Insert Data */
        $validator = $this->Validation->validationSaveEdit($data);
        if ($validator->fails()) {
            $error = json_encode($validator->messages());
            $arrayError = json_decode($error);
            $message = "";
            foreach($data as $key => $val){
                if (isset($arrayError->{$key})) {
                    $message = $message . $arrayError->{$key}[0].  "<br>";
                }
            }
            return json_encode(array("code" => 0, "message" => $message, "data" => ""));
        }
        /* Validation Insert Data End */
		
        if($data['id'] == 0){
            /* Save data */
			$insertGetId = DB::table('tbl_master_role')->insertGetId([
				'role' 			=>$data['role'], 
				'description' 	=>$data['description'],
				'active'		=>$data['active']
			]);
            /* End Save data */
        }else{
            DB::table('tbl_master_role')
				->where('id', $data['id'])
				->update([
					'role' 			=>$data['role'], 
					'description' 	=>$data['description'],
					'active'		=>$data['active']
				]);
        }
		//Save authentication to role
		$roleId = $data['id'] == 0 ? $insertGetId:$data['id'];
		$this->postAuthenticateRole($roleId,$data['authentication_id']);
        return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
    }

	private function postAuthenticateRole($roleId,$listAuthentication){
		DB::table('tbl_master_autenticate_to_role')->where('role_id',$roleId)->delete();
        $authentication = array();
        if($listAuthentication != ""){
            $authentication = explode(",", $listAuthentication);
        }
        $dataAuthenticationRole = array();
        foreach($authentication as $key=>$val){
            $dataAuthenticationRole[] = array(
                "role_id"          => $roleId,
                "authenticate_id"  => $val,
            );
        }
        if(!empty($dataAuthenticationRole)){
			DB::table('tbl_master_autenticate_to_role')->insert($dataAuthenticationRole);
        }
	}
	
    public function getDataByRowId($id){
        return DB::table('tbl_master_role')->where('id', $id)->first();
    }
	public function postDelete($listId){
        $countDeleted = 0;
        foreach ($listId as $id){
            $boolean = $this->User->isRoleHasUser($id);
            if($boolean == 1){
                $countDeleted++;
            }else{
                DB::table('tbl_master_role')->where('id',$id)->delete();
				DB::table('tbl_master_autenticate_to_role')->where('role_id',$id)->delete();
            }
        }
        if($countDeleted >= 1 && count($listId) > 1){
            return array("code" => 0, "message" => $this->messages['itemsDeleted']);
        }else if($countDeleted == 1 && count($listId) == 1){
            return array("code" => 1, "message" => $this->messages['userInUsed']);
        }else{
            return array("code" => 2,"message" => $this->messages['success']);
        }
    }
	public function getRoleName(){
        $obj =  DB::table('tbl_master_role')->select('role')->get();
        $arr = array();
        foreach($obj as $row){
            $arr[] = $row->role;
        }
        return json_encode($arr);
    }
}
?>