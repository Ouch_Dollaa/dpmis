<?php

namespace App\Models\BackEnd\User;
use Illuminate\Support\Facades\DB;
use App\Validation\User\ValidationUser;
use Config;
use App\libraries\Tool;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class UserModel
{

    public function __construct()
    {
		$this->messages = Config::get('messages');
        $this->constant = Config::get('constant');
		$this->Validation = new ValidationUser();
		$this->Tool = new Tool();
    }
	public function getDataGrid($dataRequest){
        $page = $dataRequest['pagenum'] ? intval($dataRequest['pagenum']) : 0;
        $limit = $dataRequest['pagesize'] ? intval($dataRequest['pagesize']) : $this->constant['pageSize'];
        $sort = isset($dataRequest['sortdatafield']) ? strval($dataRequest['sortdatafield']) : "id";
        $order = isset($dataRequest['sortorder']) ? strval($dataRequest['sortorder']) : "DESC";
        $offset = $page*$limit;
        $filtersCount = isset($dataRequest['filterscount']) ? intval($dataRequest['filterscount']) : 0;
        $listDb = DB::table('tbl_master_user');
        $total = count($listDb->get());
        $listDb = $listDb
				->OrderBy($sort, $order)
				->take($limit)
				->skip($offset);
		
        if($filtersCount>0){
            $listDb = $listDb->where('user_name','LIKE','%'.$dataRequest['filtervalue0'].'%');
            $total = count($listDb->get());
        }
        $this->Role = new RoleModel();
        $listDb = $listDb->get();
        $list = array();
        foreach($listDb as $row){
			$roleObj = $this->Role->getDataByRowId($row->role_id);
            $list[] = array(
                "id"           		=> $row->id,
				"user_name"  		=> $row->user_name,
				"avatar"  			=> $row->avatar,
                "role_id"		    => $roleObj->role,
				"order_number"  	=> $row->order_number,
                "active"       		=> $row->active,
            );
        }
        return json_encode(array('total'=>$total,'items'=>$list));
    }
    public function isRoleHasUser($id){
        if(DB::table('tbl_master_user')->where('role_id',$id)->count() > 0){
            return 1;
        }else{
            return 0;
        }
    }
	
	public function postNew($id){
        $listRole = DB::table('tbl_master_role')->where('active',1)->OrderBy('role', 'asc')->get();
		$user = $this->getDataByRowId($id);
        $arrListRole = array(array("text"=>"", "value" => ""));
		$arrListDepartment = array(array("text" => "","value" => ""));
        foreach($listRole as $row){
            $arrListRole[] = array(
				'text' 	=> $row->role, 
				"value" => $row->id
				);
        }
        return array("user" => $user, "listRole" => $arrListRole);
    }
	
	public function postSave($data){
        //dd($data);
        $paths = 'files/users/';
        /* Validation Insert Data */
        //dd($data);
        $validator = $this->Validation->validationSaveEdit($data);
        if ($validator->fails()) {
            $error = json_encode($validator->messages());
            $arrayError = json_decode($error);
            $message = "";
            foreach($data as $key => $val){
                if (isset($arrayError->{$key})) {
                    $message.= $arrayError->{$key}[0].  "<br>";
                }
            }
            return json_encode(array("code" => 0, "message" => $message, "data" => ""));
        }
		
		$userObj = $this->getDataByRowId($data['id']);
		
		if (Input::hasFile('avatar')) {
            $files = Input::file('avatar');
            $size = $files->getSize();
            $extension = ".".strtolower($files->getClientOriginalExtension());
            $random_name = $this->Tool->mt_rand_str(5, '0123456789');
            $convertName = time() . "_" . $random_name . $extension;
			
			//Move image to folder
            $upload = $files->move($paths, $convertName);
			if($userObj != null){
				if($userObj->avatar != ""){
					if (Storage::disk('public')->exists($userObj->avatar)){
						Storage::disk('public')->delete($userObj->avatar);
					}
				}
            }
            $imageUrl = $paths . $convertName;
            $data['avatarPic'] = $imageUrl;
        }else{
			$data['avatarPic'] = $userObj != null ? $userObj->avatar:'';
		}
		/* Click remove sign */
		if($data['statusRemovePicture'] == 1){
			if (Storage::disk('public')->exists($userObj->avatar)){
				$data['avatarPic'] = '';
				Storage::disk('public')->delete($userObj->avatar);
			}
		}
		
        if($data['id'] == 0){
            /* Save data */
			DB::table('tbl_master_user')->insert([
				'user_name' 		=>strtolower($data['user_name']),
				'avatar' 			=>$data['avatarPic'],
				'password'			=>bcrypt($data['password']),
				'salt' 				=>$data['password'],
				'role_id' 		    =>$data['role'],
				'active'			=>$data['active'],
			]);
            /* End Save data */
        }else{
            DB::table('tbl_master_user')
				->where('id', $data['id'])
				->update([
					'user_name' 		=>strtolower($data['user_name']),
					'avatar' 			=>$data['avatarPic'],
					'role_id' 		    =>$data['role'],
					'active'			=>$data['active']
				]);
				
        }
       
        return json_encode(array("code" => 1, "message" => $this->messages['success'], "data" => ""));
    }
	public function postDelete($listId){
        $countDeleted = 0;
        foreach ($listId as $id){
            $boolean = $this->isCurrentUser($id);
			$userRec = $this->getDataByRowId($id);
            if($boolean == 1){
                $countDeleted++;
            }else{
                if($userRec->avatar != ''){
                    if(Storage::disk('public')->exists($userRec->avatar)){
                        Storage::disk('public')->delete($userRec->avatar);
                    }
                }
                DB::table('tbl_master_user')->where('id',$id)->delete();
            }
        }
        if($countDeleted >= 1 && count($listId) > 1){
            return array("code" => 0, "message" => $this->messages['itemsDeleted']);
        }else if($countDeleted == 1 && count($listId) == 1){
            return array("code" => 1, "message" => 'This user currently in used, please try another one');
        }else{
            return array("code" => 2,"message" => $this->messages['success']);
        }
    }
	public function isCurrentUser($id){
        $sessionUserId = Session::get('sessionUser')->id;
        return $sessionUserId == $id ? 1:0;
    }
	public function getDataByRowId($id){
        return DB::table('tbl_master_user')->where('id', $id)->first();
    }
    public function getUserName(){
        $obj =  DB::table('tbl_master_user')->select('user_name')->get();
        $arr = array();
        foreach($obj as $row){
            $arr[] = $row->user_name;
        }
        return json_encode($arr);
    }
    public function cheekUserName($name){
        $cheek = DB:: table('tbl_master_user')
            ->where('user_name',$name)->first();
        if(count($cheek)){
             $msg['success'] = true;
        }
        else{
            
            $msg['success'] = false;
        }
        return $msg;
    }
	public function postSaveChangeUserPassword($user_id,$oldPassword,$newPassword){
        $sql = DB:: table('tbl_master_user')
            ->where('id',$user_id)
            ->where('salt',$oldPassword)
            ->first();

        if($sql == null){
            flash()->success($this->constant['incorrectOldPassword']);
        }else{
            /* Update User Password */
            flash()->success($this->constant['passwordChangeSuccess']);
            DB::table('tbl_master_user')
                ->where('id', $user_id)
                ->update([
                    'password'  =>bcrypt($newPassword),
                    'salt'      =>$newPassword
                ]);
        }
        return Redirect::back();
    }
}
?>