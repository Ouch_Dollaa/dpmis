<?php
namespace App\Models;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
class UserAuthorizeModel
{
    public function userAuthorize(){
        $segmentTwo = Request::segment(2);
        $segmentThree = Request::segment(3);
        if($segmentThree == null){
            $segmentThree = 'index';
        }
		$url = $segmentTwo . '/'. $segmentThree;
        $action = array("index", "new", "save", "delete","edit","detail");
        if (in_array($segmentThree, $action)) {
            $userSession = session('sessionUser');
            $roleId = $userSession->role_id;
            $authentication = DB::table('tbl_master_authenticate')->where('url',$url)->first();//dd($authentication);
            if(count($authentication)){
                $authenticationRole = DB::table('tbl_master_autenticate_to_role')
										->where('role_id',$roleId)
										->where('authenticate_id',$authentication->id)->get();
                if(count($authenticationRole)){
                    return array("code"=>1,"message" => "Grand permission" ,"data" => "");
                }else{
                    return array("code"=>0,"message" => "Don't grand permission" ,"data" => "");
                }
            }else{
                return array("code"=>0,"message" => "no-url" ,"data" => "");
            }
        }else{
            return array("code"=>1,"message" => "Grand permission" ,"data" => "");
        }
    }

	public function getTreeMenuLeft(){
        $listDb = DB::table('tbl_master_authenticate')->orderBy('order', 'asc')->where('active',1)->get();
        $arr = array();
        foreach($listDb as $row){
			$icon = $row->icon != '' ? $row->icon:"icon/notepad.png";
            $arr[] = array(
					"id"  		=>$row->id,
                    "parentid"  =>$row->parent_id,
                    "text"      =>$row->name,
                    "value"     =>$row->url,
					"icon"		=>asset('/') . $icon
            );
        }
        return json_encode($arr);
    }
	public function getTreeMenuIdByUrl(){
            $segmentTwo = Request::segment(2);
            $authentication = DB::table('tbl_master_authenticate')->where('url',$segmentTwo)->first();
            if($authentication == null){
                return 0;
            }
            return $authentication->id;
        }
}
?>