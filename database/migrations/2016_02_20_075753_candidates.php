<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Candidates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('sex');
            $table->float('height');
            $table->float('weight');
            $table->date('dob');
            $table->string('phone');
            $table->string('email');
            $table->string('address');
            $table->boolean('maritalStatus');
            $table->integer('approved_user_id');
            $table->integer('rejected_user_id');
            $table->date('approvedDate');
            $table->date('rejectedDate');
            $table->string('rejectedReason');
            $table->string('hashkey')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidates');
    }
}
