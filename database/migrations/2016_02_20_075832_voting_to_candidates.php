<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VotingToCandidates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_to_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id')->unsigned();
                $table->foreign('candidate_id')->references('id')->on('candidates');
            $table->integer('voting_user_id')->unsigned();
                $table->foreign('voting_user_id')->references('id')->on('voting_users');
            $table->string('hashkey')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('voting_to_candidates');
    }
}
