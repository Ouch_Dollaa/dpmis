<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VotingUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('avatar');
            $table->string('sex');
            $table->string('social_id');
            $table->timestamp('last_login_date');
            $table->string('hashkey')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('voting_users');
    }
}
