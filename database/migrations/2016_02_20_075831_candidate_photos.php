<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CandidatePhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id')->unsigned();
                $table->foreign('candidate_id')->references('id')->on('candidates');
            $table->string('path');
            $table->string('hashkey')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidate_photos');
    }
}
