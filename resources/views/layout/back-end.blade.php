<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" charset=utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('icon/uef.ico')}}" />
    
	
    <title>Welcome to DPMIS Homepage</title>
    <link rel="stylesheet" href="{{asset('css/global.css')}}" type="text/css" />
    <!-- Font Awesome -->

  <link rel="stylesheet" href="{{asset('asset/font-awesome/css/font-awesome.min.css')}}">
  
  <link rel="stylesheet" href="{{asset('css/metro-bootstrap.min.css')}}">
  

   <!--full calenda-->
  <link rel="stylesheet" href="{{asset('asset/plugins/fullcalendar/fullcalendar.min.css')}}">
  <link rel="stylesheet" href="{{asset('asset/plugins/fullcalendar/fullcalendar.print.css')}}" media="print">

  <!-- Theme style -->
      <link rel="stylesheet" href="{{asset('asset/css/AdminLTE.min.css')}}">
  <!-- Ionicons -->
  

  
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('asset/css/skins/_all-skins.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('asset/plugins/iCheck/flat/blue.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('asset/plugins/morris/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('asset/plugins/datepicker/datepicker3.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('asset/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    {{--confirm delete--}}
    <link rel="stylesheet" href="{{asset('css/jquery-confirm.min.css')}}">
  
  <script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>

</head>

<body>

<input type="hidden" id="baseUrl" value="{{asset('')}}" />
<div id="jqx-notification"></div>
<div id="jqxLoader"></div>
<div class="wrapper">
    <?php
        $sessionUser = session('sessionUser');
        $avatar = $sessionUser->avatar !='' ? $sessionUser->avatar:'images/image-profile.jpg';
        $avatar = asset('/') . $avatar;
        $assetLogo = asset('images/logo.png');
    ?>
    <div class="container-fluid" id="header-container">
        <div class="form-group khmer-font-header">
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <div class="pull-left"><img src="{{$assetLogo}}" height="60" alt="" class="img-circle"></div>
                <div class="pull-left">
                    <div class="title-kh">{{trans('trans.institude_name_kh')}}</div>
                    <h1 class="title-en">{{trans('trans.institude_name_en')}}</h1>
                </div>
            </div>
           


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">{{trans('trans.project_name')}}</div>
           
            

            <div class="col-lg-3 col-md-2 col-sm-2 col-xs-6 text-right ptext-1">
                
            <img src="{{$avatar}}" class="img-circle" width="35" height="35" alt=""> 
                    {{ucfirst($sessionUser->user_name)}}
            </div>
            <div class="col-lg-1 col-md-2 col-sm-1 col-xs-6 tr-1">
                <a href="{{asset('auth/logout')}}" title="{{trans('trans.logout')}}" data-placement="right" data-toggle="tooltip"><i class="glyphicon glyphicon-log-out"></i></a>
            </div>
            
        </div>
    </div>

    <div id="splitter">
        <div>
            <div id="jqxTree"></div>
        </div>
        <div id="ContentPanel">
            @yield('content')
        </div>
    </div>
</div>
<div class="form-group text-center footer-developed">
    <p>{{trans('trans.sytem_copy_right')}}</p>
</div>

<style>
   .content-panel{
     overflow-y: auto;
       overflow-x: hidden !important;
   }
   .panel-heading {   
    padding-top: 5px;
  }
  .box.box-solid.box-info>.box-header {
    background: linear-gradient(#000080, #0044cc);
    background-color: #000080;
  }
  .box.box-solid.box-info {
    border: 1px solid #0044cc;
  }
  .jqx-scrollbar-thumb-state-pressed-bootstrap, .jqx-splitter-splitbar-vertical-bootstrap, .jqx-splitter-splitbar-horizontal-bootstrap, .jqx-scrollbar-thumb-state-pressed-horizontal-bootstrap {
    background: linear-gradient(#0b654a, #0b654a);
    border-color: #0044cc;
  }
  .jqx-splitter-collapse-button-vertical-bootstrap, .jqx-progressbar-value-vertical-bootstrap {
    background-color: #fff;
    padding: 5px;
  }

.jqx-listitem-state-selected-bootstrap, .jqx-menu-item-selected-bootstrap, .jqx-tree-item-selected-bootstrap, .jqx-calendar-cell-selected-bootstrap, .jqx-grid-cell-selected-bootstrap, .jqx-menu-vertical-bootstrap .jqx-menu-item-top-selected-bootstrap, .jqx-grid-selectionarea-bootstrap {
    color: #ffffff !important;
    background-color: #0b654a !important;
    border-color: #0b654a !important;
    
}

.jqx-notification-mail, .jqx-primary {
    color: #ffffff !important;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25) !important;
    background-color: #00a65a !important;
    background-repeat: repeat-x !important;
    background-image: linear-gradient(to bottom, #00a65a, #00a65a) !important;
    border-left-color: #00a65a !important;
    border-right-color: #00a65a !important;
    border-top-color: #00a65a !important;
    border-bottom-color: #00a65a !important;
}

.jqx-primary:hover,
.jqx-primary:focus,
.jqx-primary:active,
.jqx-primary.active,
.jqx-primary.disabled,
.jqx-primary[disabled] {
    color: #ffffff  !important;
    background-color: #0b654a  !important;
    *background-color: #0b654a  !important;
}

.jqx-fill-state-pressed.jqx-primary,
.jqx-primary:active,
.jqx-primary.active {
    background-color: #00a65a !important;
    background-image: linear-gradient(to bottom, #00a65a, #00a65a)  !important;
}
</style>

<script type="text/javascript" src="{{asset('jqwidgets/jqx-all.js')}}"></script>
<script type="text/javascript" src="{{asset('jqwidgets/jqxcore.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/core-function.js')}}"></script>
<script type="text/javascript" src="{{asset('js/tableToExcel.js')}}"></script>

<script type="text/javascript" src="{{asset('js/code39.js')}}"></script>
<script type="text/javascript" src="{{asset('js/detector.js')}}"></script>
<script type="text/javascript" src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('ckeditor/adapters/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.number.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.number.min.js')}}"></script>

<script type="text/javascript" src="{{asset('js/jquery-confirm.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        var fullHeight = $(window).height();
        $("#splitter").jqxSplitter({width: '100%', height: (fullHeight)-110, panels: [{ size: 220 }],theme:jqxTheme,splitBarSize:5 });
        var currentPath = '<?php echo $segment['two'];?>';
		if(currentPath == 'report'){
			$('#splitter').jqxSplitter('collapse');
		}
        var jqxTreeData = <?php echo $treeMenu; ?>;
        // prepare the data
        var treeDataSource = {
            datatype: "json",
            datafields: [
                { name: 'id' },
                { name: 'parentid' },
                { name: 'text' },
                { name: 'value' },
                { name: 'icon' }
            ],
            id: 'id',
            localdata: jqxTreeData
        };
        // create data adapter.
        var dataAdapter = new $.jqx.dataAdapter(treeDataSource);

        // perform Data Binding.
        dataAdapter.dataBind();
        var records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label'}]);
        $('#jqxTree').jqxTree({ source: records, height: '100%', width: '100%',theme:jqxTheme,allowDrag: false});
		//$('#jqxTree').jqxTree('expandAll');
		
        $('#jqxTree').on('select', function (event) {
            var args = event.args;
            var item = $('#jqxTree').jqxTree('getItem', args.element);
            if(item.level != 0 && item.hasItems == false){
                $(location).attr('href',basePath + '<?php echo $constant['secretRoute']; ?>/' + item.value);
            }else if(item.level == 0 && item.hasItems == false){
                $(location).attr('href',basePath + '<?php echo $constant['secretRoute']; ?>/' + item.value);
            }
        });
		$("#jqxTree").jqxTree('expandItem', $("#{{$treeMenuId}}")[0]);
        $('#jqxTree li#<?php echo $treeMenuId;?> div').addClass('jqx-rc-all jqx-rc-all-bootstrap jqx-tree-item jqx-tree-item-bootstrap jqx-item jqx-item-bootstrap jqx-tree-item-selected jqx-fill-state-pressed jqx-fill-state-pressed-bootstrap jqx-tree-item-selected-bootstrap');
		
		/* Scroll-Y */
		var currentUrl = window.location.pathname;
		var lastSegment = currentUrl.split('/').pop();
		if(lastSegment == 'dashboard'){
			$('#ContentPanel').addClass('content-panel');
		} else {
			$('#ContentPanel').removeClass('content-panel');
		}
    });
	
</script>


<!-- jQuery 2.2.3 -->

<!-- jQuery UI 1.11.4 -->

<script src="{{asset('asset/js/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Morris.js charts -->
<script src="{{asset('asset/js/raphael-min.js')}}"></script>


<!-- Sparkline -->
<script src="{{asset('asset/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('asset/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('asset/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('asset/plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('asset/js/moment.min.js')}}"></script>

<!-- datepicker -->
<script src="{{asset('asset/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- <script src="{{asset('asset/plugins/datepicker/bootstrap-datepicker.js')}}"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('asset/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('asset/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('asset/js/app.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('asset/js/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('asset/plugins/fullcalendar/fullcalendar.min.js')}}"></script>

<script src="{{asset('js/animate.js')}}"></script>




<script>
  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 1070,
          revert: true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        });

      });
    }

    ini_events($('#external-events div.external-event'));

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week: 'week',
        day: 'day'
      },
      //Random default events
      events: [
        {
          title: 'All Day Event',
          start: new Date(y, m, 1),
          backgroundColor: "#f56954", //red
          borderColor: "#f56954" //red
        },
        {
          title: 'Long Event',
          start: new Date(y, m, d - 5),
          end: new Date(y, m, d - 2),
          backgroundColor: "#f39c12", //yellow
          borderColor: "#f39c12" //yellow
        },
        {
          title: 'Meeting',
          start: new Date(y, m, d, 10, 30),
          allDay: false,
          backgroundColor: "#0073b7", //Blue
          borderColor: "#0073b7" //Blue
        },
        {
          title: 'Lunch',
          start: new Date(y, m, d, 12, 0),
          end: new Date(y, m, d, 14, 0),
          allDay: false,
          backgroundColor: "#00c0ef", //Info (aqua)
          borderColor: "#00c0ef" //Info (aqua)
        },
        {
          title: 'Birthday Party',
          start: new Date(y, m, d + 1, 19, 0),
          end: new Date(y, m, d + 1, 22, 30),
          allDay: false,
          backgroundColor: "#00a65a", //Success (green)
          borderColor: "#00a65a" //Success (green)
        },
        {
          title: 'Click for Google',
          start: new Date(y, m, 28),
          end: new Date(y, m, 29),
          url: 'http://google.com/',
          backgroundColor: "#3c8dbc", //Primary (light-blue)
          borderColor: "#3c8dbc" //Primary (light-blue)
        }
      ],
      editable: true,
      droppable: true, // this allows things to be dropped onto the calendar !!!
      drop: function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);

        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        copiedEventObject.backgroundColor = $(this).css("background-color");
        copiedEventObject.borderColor = $(this).css("border-color");

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }

      }
    });

    /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function (e) {
      e.preventDefault();
      //Save color
      currColor = $(this).css("color");
      //Add color effect to button
      $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
    });
    $("#add-new-event").click(function (e) {
      e.preventDefault();
      //Get value and make sure it is not null
      var val = $("#new-event").val();
      if (val.length == 0) {
        return;
      }

      //Create events
      var event = $("<div />");
      event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
      event.html(val);
      $('#external-events').prepend(event);

      //Add draggable funtionality
      ini_events(event);

      //Remove event from text input
      $("#new-event").val("");
    });
  });
</script>




</body>
</html>
