<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="{{asset('icon/mef.ico')}}" />
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{asset('css/global.css')}}" type="text/css" />
     <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" type="text/css" />
    <!-- Fonts -->
   <link rel="stylesheet" href="{{asset('asset/font-awesome/css/font-awesome.min.css')}}">
   <link rel="stylesheet" href="{{asset('asset/css/style.css')}}">

 
    
<script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

    <style>
        
        body
        {
            background-image:url('{{asset("images/bg1.jpg")}}');
            
            background-attachment: fixed;
        }
        
       

    </style>
</head>
<body>
<input type="hidden" name="base-url" id="baseUrl" value="{{asset('')}}" />
@yield('content')

</body>
</html>
