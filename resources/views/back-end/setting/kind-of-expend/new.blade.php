<?php
$jqxPrefix = '_kind_of_expend';
$saveUrl = asset($constant['secretRoute'].'/kindof-expend/save');

?>
<div class="container-fluid">
   <div class="box box-success box-solid">
        <div class="box-header with-border ui-sortable-handle" style="cursor: move;">
          <h3 class="box-title" style="font-family: KHMERMEF1; font-size: 18px;"><i class="fa fa-file-pdf-o"></i>  Form Kinde of Expends</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
           <form class="form-horizontal" role="form" method="post" name="jqx-form<?php echo $jqxPrefix;?>" id="jqx-form<?php echo $jqxPrefix;?>" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="ajaxRequestJson" value="true" />
                <input type="hidden" id="id" name="id" value="{{isset($row->id) ? $row->id:''}}">
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px; text-align: right;"><span class="red-star">*</span>ប្រភេទចំណាយ</div>
                     <div class="col-sm-9">
                         <input type="text" name="kindof_expend" id="kindof_expend" value="{{isset($row->kindof_expend) ? $row->kindof_expend:''}}" placeholder="ប្រភេទចំណាយ" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px; text-align: right;"><span class="red-star">*</span>លេខរៀងលំដាប់</div>
                    <div class="col-sm-9">
                        <input type="text" name="orderNumber" id="orderNumber" value="{{isset($row->orderNumber) ? $row->orderNumber:''}}" placeholder="លេខរៀងលំដាប់" class="form-control">
                    </div>
                </div><!--end-->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <input type="hidden" id="active" name="active" value="{{isset($row->active) ? $row->active:1}}">
                        <div id="active-checkbox"> {{$constant['active']}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-2">
                        <button id="jqx-save<?php echo $jqxPrefix;?>" type="button" class="button-color"><span class="glyphicon glyphicon-check"></span> {{trans('trans.buttonSave')}}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
</div>
<script>
    $(document).ready(function(){
        var buttons = ['jqx-save<?php echo $jqxPrefix;?>'];
        initialButton(buttons,90,35);
        //validate
        $('#jqx-form<?php echo $jqxPrefix;?>').jqxValidator({
            hintType: 'label',
            rules: [{
                input: '#kindof_expend',
                message: ' ',
                action: 'blur',
                rule: 'required'

            }]
        });
        //Save action button
        $("#jqx-save<?php echo $jqxPrefix;?>").click(function(){
            saveJqxItem('{{$jqxPrefix}}', '{{$saveUrl}}', '{{ csrf_token() }}');
        });
        //Active
        var isActive = $('#active').val() == 1 ? true:false;
        $("#active-checkbox").jqxCheckBox({ theme: jqxTheme,width: 120, height: 25, checked: isActive});
        $('#active-checkbox').on('change', function (event) {
            event.args.checked == true ? $('#active').val(1):$('#active').val(0);
        });
    });
</script>


