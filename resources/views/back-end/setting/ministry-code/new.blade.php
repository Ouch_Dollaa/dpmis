<?php
$jqxPrefix = '_ministry_code';
$saveUrl = asset($constant['secretRoute'].'/ministry-code/save');

?>
<div class="container-fluid">
   <div class="box box-success box-solid">
        <div class="box-header with-border ui-sortable-handle" style="cursor: move;">
          <h3 class="box-title" style="font-family: KHMERMEF1; font-size: 18px;"><i class="fa fa-file-pdf-o"></i>  Form Ministries Number</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
           <form class="form-horizontal" role="form" method="post" name="jqx-form<?php echo $jqxPrefix;?>" id="jqx-form<?php echo $jqxPrefix;?>" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="ajaxRequestJson" value="true" />
                <input type="hidden" id="id" name="id" value="{{isset($row->id) ? $row->id:''}}">
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px; text-align: right;"><span class="red-star">*</span>លេខកូដក្រសួង</div>
                     <div class="col-sm-9">
                         <input type="text" name="ministrycode" id="ministrycode" value="{{isset($row->ministrycode) ? $row->ministrycode:''}}" placeholder="លេខកូដក្រសួង" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px; text-align: right;"><span class="red-star">*</span>ឈ្មោះក្រសួង</div>
                    <div class="col-sm-9">
                     <input type="text" id="ministry" name="ministry" value="{{isset($row->ministry) ? $row->ministry:''}}" placeholder="លេខកូដក្រសួង" class="form-control">
                    </div>
                </div><!--end-->
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px; text-align: right;"><span class="red-star">*</span>លេខរៀងលំដាប់</div>
                    <div class="col-sm-9">
                        <input type="text" name="orderNumber" id="orderNumber" value="{{isset($row->orderNumber) ? $row->orderNumber:''}}" placeholder="លេខរៀងលំដាប់" class="form-control">
                    </div>
                </div><!--end-->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <input type="hidden" id="active" name="active" value="{{isset($row->active) ? $row->active:1}}">
                        <div id="active-checkbox"> {{$constant['active']}}</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-2">
                        <button id="jqx-save<?php echo $jqxPrefix;?>" type="button" class="button-color"><span class="glyphicon glyphicon-check"></span> {{trans('trans.buttonSave')}}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
</div>
<script>
    $(document).ready(function(){
        var buttons = ['jqx-save<?php echo $jqxPrefix;?>'];
        initialButton(buttons,90,35);

        //validate
        $('#jqx-form<?php echo $jqxPrefix;?>').jqxValidator({
            hintType: 'label',
            rules: [{
                input: '#ministry',
                message: ' ',
                action: 'blur',
                rule: 'required'

            }]
        });

        //Save action button
        $("#jqx-save<?php echo $jqxPrefix;?>").click(function(){
            saveJqxItem('{{$jqxPrefix}}', '{{$saveUrl}}', '{{ csrf_token() }}');
        });

        //Active
        var isActive = $('#active').val() == 1 ? true:false;
        $("#active-checkbox").jqxCheckBox({ theme: jqxTheme,width: 120, height: 25, checked: isActive});
        $('#active-checkbox').on('change', function (event) {
            event.args.checked == true ? $('#active').val(1):$('#active').val(0);
        });

    });
</script>


