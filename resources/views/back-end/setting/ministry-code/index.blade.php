<?php
$jqxPrefix = '_ministry_code';
$newUrl = asset($constant['secretRoute'].'/ministry-code/new');
$editUrl = asset($constant['secretRoute'].'/ministry-code/edit');
$listUrl = asset($constant['secretRoute'].'/ministry-code/index');
$deleteUrl = asset($constant['secretRoute'].'/ministry-code/delete');
$processUrl = asset($constant['secretRoute'].'/ministry-code/process');
$printUrl = asset($constant['secretRoute'].'/ministry-code/print');
?>
@extends('layout.back-end')
@section('content')
    <div id="content-container" class="content-container">
        <div class="panel">
            <div class="row panel-heading custome-panel-headering">
                <div class="form-group title-header-panel">
                    <div class="pull-left">
                        <div class="col-sm-4 col-xs-12" style="width: 32%"><i class="fa fa-file fa-lg"></i> {{$constant['new_document']}}</div>
                    </div>
                    <div class="text-center"></div>
                    <div class="pull-right">
                        <div class="col-lg-12">
                            <button id="btn-new<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-plus"></i> {{$constant['buttonNew']}}</button>
                            <button id="btn-edit<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-edit"></i> {{$constant['buttonEdit']}}</button>
                            <button id="btn-delete<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-trash"></i> {{$constant['buttonDelete']}}</button>
                        </div>
                    </div>
                </div>
                <div id="jqx-grid<?php echo $jqxPrefix;?>"></div>
            </div>
        </div>
    </div>
    
<script type="text/javascript">
      
    // prepare the data
    var source<?php echo $jqxPrefix;?> = {
        type: "post",
        dataType: "json",
        data:{"_token":'{{ csrf_token() }}'},
        dataFields: [
            { name: 'id', type: 'number' },
            { name: 'ministrycode', type: 'string' },
            { name: 'ministry', type: 'string' },
            { name: 'orderNumber', type: 'number' },
            { name: 'active', type: 'number' },
        ],
        cache: false,
        id: 'id',
        url: '<?php echo $listUrl;?>',
        
        beforeprocessing: function(data) {
            source<?php echo $jqxPrefix;?>.totalrecords = (data != null)? data.total:0;
        },
        sort: function(data) {
        // Short Data
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'sort');
        },
        filter: function() {
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'filter');
        },
        deleteRow: function (rowid, commit) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: "<?php echo $deleteUrl;?>",
                cache: false,
                data: {"id":rowid,"_token":'{{ csrf_token() }}','ajaxRequestJson':'true'},
                success: function (response, status, xhr) {
                    $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                    if(response.code == 0) {
                        //Some items delete execpt the one in used
                        $('#jqx-notification').jqxNotification({position: 'bottom-right',template: "warning",autoClose: false}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else if(response.code == 1){
                        //Item in used
                        $('#jqx-notification').jqxNotification({position: 'bottom-right',template: "warning"}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else{
                        //Items delete success
                        closeJqxWindowId('jqxwindow<?php echo $jqxPrefix;?>');
                        $('#jqx-notification').jqxNotification({ position: 'bottom-right', template: "success" }).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata');
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('clearselection');
                },
                error: function (request, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    };
    var numberRenderer<?php echo $jqxPrefix;?> = function (row, column, value) {
        return '<div style="text-align: center; margin-top: 5px;">' + (1 + value) + '</div>';
    };
    var active<?php echo $jqxPrefix;?> = function (row, datafield, value) {
        return value == 0 ? '<div style="text-align: center; margin-top: 5px;"><i class="glyphicon glyphicon-remove"></i></div>':'<div style="text-align: center; margin-top: 5px;"><i class="glyphicon glyphicon-ok"></i></div>';
    };
    $(document).ready(function () {
        //Button action
        var buttons = ['btn-new<?php echo $jqxPrefix;?>','btn-edit<?php echo $jqxPrefix;?>','btn-delete<?php echo $jqxPrefix;?>'];
        initialButton(buttons,80,36);

        var dataAdapter = new $.jqx.dataAdapter(source<?php echo $jqxPrefix;?>);
        // create Tree Grid
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid({
                theme:jqxTheme,
                width:'100%',
                height:gridHeight,
                rowsheight:rowsheight,
                source: dataAdapter,
                selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                virtualmode: true,
                filterable:true,
                showfilterrow: true,
                pagerMode: 'advanced',
                pageSize: {{$constant['pageSize']}},
                pageSizeOptions: {{$constant['pageSizeOptions']}},
                rendergridrows: function(obj) {
                    return obj.data;
                },
                columns: [
                    { text: '{{$constant['autoNumber']}}',filterable:false, columntype: 'number', width:'5%', cellsrenderer: numberRenderer<?php echo $jqxPrefix;?>, align:'center', cellsalign:'center' },
                    { text: '{{$constant['ministry_code']}}', dataField: 'ministrycode', width: '10%', cellsalign:'center', align:'center' },
                    { text: '{{$constant['ministry_name']}}', dataField: 'ministry',width: '63%', align:'center' },
                    { text: '{{$constant['order']}}', dataField: 'orderNumber', width: '10%', cellsalign:'center', align:'center' },
                    { text: '{{$constant['active']}}', dataField: 'active',filtertype:'list',filteritems:['Inactive','Active'], width: '10%',cellsrenderer: active<?php echo $jqxPrefix;?>, align:'center' }
                ]

        });

       
        $("#btn-new<?php echo $jqxPrefix;?>").on('click',function(){
            newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonNew']}}',900,500, '<?php echo $newUrl;?>', 0, '{{ csrf_token() }}');
        });


        $("#btn-edit<?php echo $jqxPrefix;?>").on('click',function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
            $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['editRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else if(row.length > 1){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['selectOneRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else{
                var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row);
                console.log(jqxdatarow);
                newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonEdit']}}', 750,500, '<?php echo $editUrl;?>', jqxdatarow.id, '{{ csrf_token() }}');
            }

        });
        $("#btn-delete<?php echo $jqxPrefix;?>").click(function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['deleteRow']}}');
                $("#jqx-notification").jqxNotification("open");
                $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                return false;
            }
            var confirmMsg = confirm("{{$constant['confirmDelete']}}?");
            var listId = [];
            if(confirmMsg){
                for(var index in row){
                    var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row[index]);
                    listId.push(jqxdatarow.id);
                }
                $('#jqx-grid<?php echo $jqxPrefix;?>').jqxGrid('deleteRow', listId);
            }
        });

    });
</script>

<style type="text/css">
    .process_doc
        {
            width: 125px !important;
        }
    
</style>
@endsection