<?php
$jqxPrefix = '_permission';
$saveUrl = asset($constant['secretRoute'].'/permission/save');

?>
<div class="container-fluid">
   <div class="box box-success box-solid">
        <div class="box-header with-border ui-sortable-handle" style="cursor: move;">
          <h3 class="box-title" style="font-family: KHMERMEF1; font-size: 18px;"><i class="fa fa-file-pdf-o"></i>  Form Ministries Number</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
           <form class="form-horizontal" role="form" method="post" name="jqx-form<?php echo $jqxPrefix;?>" id="jqx-form<?php echo $jqxPrefix;?>" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="ajaxRequestJson" value="true" />
                <input type="hidden" id="id" name="id" value="{{isset($row->id) ? $row->id:''}}">
                <div class="form-group">
                     <div class="col-sm-3" style="padding:10px; text-align: right;"><span class="red-star">*</span>អ្នកប្រើប្រាស់</div>
                     <div class="col-sm-9">
                         <input type="hidden" id="username" name="username">
                         <div id="div_username"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px; text-align: right;"><span class="red-star">*</span>ឈ្មោះក្រសួង</div>
                    <div class="col-sm-9">
                         <div id="div_listBox" name="listBox"></div>
                    </div>
                </div><!--end-->
                <div class="form-group">
                   <div class="col-sm-offset-4 col-sm-8">
                       <div id="print" name="print"> {{$constant['print']}}</div>
                   </div>
                </div>
               <div class="form-group">
                   <div class="col-sm-offset-4 col-sm-8">
                       <div id="download" name="download"> {{$constant['download']}}</div>
                   </div>
               </div>
                <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-2">
                        <button id="jqx-save<?php echo $jqxPrefix;?>" type="button" class="button-color"><span class="glyphicon glyphicon-check"></span> {{trans('trans.buttonSave')}}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
</div>
<script>
    $(document).ready(function(){
        var buttons = ['jqx-save<?php echo $jqxPrefix;?>'];
        initialButton(buttons,90,35);
        initDropDownList(jqxTheme,'100%',35, '#div_username', <?php echo $username;?>, 'text', 'value', false, '', '0', "#username","{{trans('trans.buttonSearch')}}",250);
        //Form Validation here
        $('#jqx-form<?php echo $jqxPrefix;?>').jqxValidator({
            hintType: 'label',
            rules: [
                {input: '#div_username', message: ' ', action: 'select',
                    rule: function () {
                        if($("#div_username").val() == ""){
                            return false;
                        }
                        return true;
                    }
                }
            ]
        });
        //Save action button
        $("#jqx-save<?php echo $jqxPrefix;?>").click(function(){
            saveJqxItem('{{$jqxPrefix}}', '{{$saveUrl}}', '{{ csrf_token() }}');
        });
        $("#div_listBox").jqxListBox({
             source: <?php echo $ministry;?>,
             width: '100%',
             height: '400px',
             theme:'jqxTheme',
             checkboxes:true,
             filterable:true,
             filterPlaceHolder:"ស្វែងរក",
             searchMode:"contains",
             displayMember: 'text',
             valueMember: 'value'
        });

        var isActive = $('#active').val() == 1 ? true:false;
        $("#print").jqxCheckBox({theme:jqxTheme, width: 120, height: 25, checked: isActive});
        $('#print').on('change', function (event) {
            event.args.checked == true ? $('#active').val(1):$('#active').val(0);
        });

        var isActive = $('#active').val() == 1 ? true:false;
        $("#download").jqxCheckBox({theme:jqxTheme, width: 120, height: 25, checked: isActive});
        $('#download').on('change', function (event) {
            event.args.checked == true ? $('#active').val(1):$('#active').val(0);
        });
    });
</script>


