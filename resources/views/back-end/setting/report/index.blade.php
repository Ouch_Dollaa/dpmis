<?php
    $jqxPrefix = '_document_reporting';
    $newUrl = asset($constant['secretRoute'].'/document-reporting/new');
    $exportUrl = asset($constant['secretRoute'].'/document-reporting/export');
    $listUrl = asset($constant['secretRoute'].'/document-reporting/index');
    $deleteUrl = asset($constant['secretRoute'].'/document-reporting/delete');
    $today_string = date('Y-m-d');
    ?>
    @extends('layout.back-end')
    @section('content')
        <div class="col-lg-12" style="height: 100%">
            <div class="row"​ style="margin: 0; padding: 0;height: 100%">
                <div id="content-container" class="content-container" style="height: 100% !important; padding: 0; margin: 0">
                    <div class="panel" style="height: 100% !important; padding: 0; margin: 0">
                        <div class="row panel-heading custome-panel-headering" style="height: 100%">
                            <div class="form-group title-header-panel">
                                <div class="form-group title-header-panel">
                                    <div class="col-lg-12 col-xs-12" style="border-bottom: 2px solid #0b654a; margin-bottom: 1px;"><i class="fa fa-file fa-lg"></i> {{$constant['reporting']}}</div>
                                <div class="col-lg-3" style="margin: 0; padding: 0;">
                                    <div class="col-lg-12" style="margin: 0; padding: 0; height: 100%">
                                        <div class="col-lg-12" style="padding: 0;border: 2px solid #ddd;    margin: 10px -9px;">
                                            <form class="form-horizontal" role="form" method="post" name="jqx-form<?php echo $jqxPrefix;?>" id="jqx-form<?php echo $jqxPrefix;?>" enctype="multipart/form-data" action="{{$exportUrl}}">
                                                {{ csrf_field() }}
                                                <div class="col-lg-12" style="margin-bottom: 15px">
                                                    <label>លេខកូដក្រសួង</label>
                                                    <div id="div_listBox" name="listBox"></div>
                                                </div>
                                                <div class="col-lg-12" style="margin-bottom: 15px">
                                                    <label>ប្រភេទធានាចំណាយ</label>
                                                    <div id="div_kindofexpend" name="kindofexpend"></div>
                                                </div>
                                                <div class="col-lg-12" style="margin-bottom: 15px">
                                                    <div class="row" style="margin-bottom: 5px">
                                                        <div class="col-lg-3" style=" margin: 0;">
                                                        <label>ថ្ងៃចាប់ផ្តើម</label>
                                                        </div>
                                                        <div class="col-lg-9" style=" margin: 0;">
                                                            <i class="pull-left">
                                                                <input type="hidden" name="start_datetime" id="start_datetime" value="{{$today_string}}">
                                                                <div id="div_start_datetime" class="pull-left"></div>
                                                            </i>
                                                        </div>
                                                    </div>
                                                        <div class="row">
                                                            <div class="col-lg-3" style=" margin: 0">
                                                                <label>ថ្ងៃបញ្ចប់</label>
                                                            </div>
                                                            <div class="col-lg-9" style=" margin: 0;">
                                                                <i>
                                                                    <input type="hidden" name="end_datetime" id="end_datetime" value="{{$today_string}}">
                                                                    <div id="div_end_datetime" class="pull-left"></div>
                                                                </i>
                                                            </div>
                                                        </div>
                                                        <button id="btn-export<?php echo $jqxPrefix;?>" class="process_doc button-color pull-right" style="position: relative;top: 12px"><i class="glyphicon glyphicon-file"></i> {{$constant['ButtonExport']}}</button>
                                                </div>
                                            </form>
                                            <div class="col-lg-2 pull-left">
                                                 <button id="btn-search<?php echo $jqxPrefix;?>" class="button-color" style="position: absolute; top: -38px; left: 35px"><i class="glyphicon glyphicon-search"></i> {{$constant['buttonSearch']}}</button>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-9" style="margin: 0; padding: 0; position: relative;top: 10px; bottom:0; right: 0">
                                    <div id="jqx-grid<?php echo $jqxPrefix;?>" style="height: 100% !important;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end row-->  
        </div> <!--end col-lg-12-->
        <script type="text/javascript">
                    function initialSearch(){
                        var ministry_list = $('input:hidden[name=listBox]').val();
                        var kindofexpend = $('input:hidden[name=kindofexpend]').val();
                        var start_datetime = $('#start_datetime').val();
                        var end_datetime = $('#end_datetime').val();
                        var date_start_end = [start_datetime,end_datetime];
                        if (start_datetime != '' && end_datetime !='') {
                            /* ministry_list */
                            var filterGroup = new $.jqx.filter();
                            var filtervalue = ministry_list;
                            filterGroup.operator = 'or';
                            filterCondition = 'EQUAL';
                            var nameFilter = filterGroup.createfilter('stringfilter', filtervalue, filterCondition);
                            filterGroup.addfilter(1, nameFilter);
                            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('addfilter', 'id_mef', filterGroup);

                            /* kindofexpend */
                            var filterGroup = new $.jqx.filter();
                            var filtervalue = kindofexpend;
                            filterGroup.operator = 'or';
                            filterCondition = 'EQUAL';
                            var nameFilter = filterGroup.createfilter('stringfilter', filtervalue, filterCondition);
                            filterGroup.addfilter(1, nameFilter);
                            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('addfilter', 'kindof_expend', filterGroup);

                            /* start_datetime */
                            var filterGroup = new $.jqx.filter();
                            var filtervalue = date_start_end;
                            filterGroup.operator = 'or';
                            filterCondition = 'EQUAL';
                            var nameFilter = filterGroup.createfilter('stringfilter', filtervalue, filterCondition);
                            filterGroup.addfilter(1, nameFilter);
                            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('addfilter', 'date', filterGroup);

                            //apply filter find to index
                            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('applyfilters');
                        }else {
                            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('clearfilters');
                        }
                    }
        // prepare the data
        var source<?php echo $jqxPrefix;?> = {
            type: "post",
            dataType: "json",
            data:{"_token":'{{ csrf_token() }}'},
            dataFields: [
                { name: 'id_doc', type: 'string' },
                { name: 'date', type: 'date' },
                { name: 'end_date', type: 'date' },
                { name: 'id_mef', type: 'string' },
                { name: 'chapter', type: 'string' },
                { name: 'chapter1', type: 'string' },
                { name: 'chapter2', type: 'string' },
                { name: 'chapter3', type: 'string' },
                { name: 'money', type: 'string' },
                { name: 'typemoney', type: 'string' },
                { name: 'money1', type: 'string' },
                { name: 'money2', type: 'string' },
                { name: 'money3', type: 'string' },
                { name: 'outcome', type: 'string' },
                { name: 'typeoutcome', type: 'string' },
                { name: 'outcome1', type: 'string' },
                { name: 'outcome2', type: 'string' },
                { name: 'outcome3', type: 'string' },
                { name: 'result', type: 'string' },
                { name: 'result1', type: 'string' },
                { name: 'result2', type: 'string' },
                { name: 'result3', type: 'string' },
                { name: 'kindof_expend', type: 'string' },
                { name: 'kindof_expend1', type: 'string' },
                { name: 'kindof_expend2', type: 'string' },
                { name: 'kindof_expend3', type: 'string' },
                { name: 'objectiv_outcome', type: 'string' },
                { name: 'note', type: 'string' }
        ],
        cache: false,
        id: 'id',
        url: '<?php echo $listUrl;?>',
        beforeprocessing: function(data) {
            source<?php echo $jqxPrefix;?>.totalrecords = (data != null)? data.total:0;
        },
        sort: function(data) {
        // Short Data
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'sort');
        },
        filter: function() {
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'filter');
        },
        deleteRow: function (rowid, commit) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: "<?php echo $deleteUrl;?>",
                cache: false,
                data: {"id":rowid,"_token":'{{ csrf_token() }}','ajaxRequestJson':'true'},
                success: function (response, status, xhr) {
                    $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                    if(response.code == 0) {
                        //Some items delete execpt the one in used
                        $('#jqx-notification').jqxNotification({position: 'bottom-right',template: "warning",autoClose: false}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else if(response.code == 1){
                        //Item in used
                        $('#jqx-notification').jqxNotification({position: 'bottom-right',template: "warning"}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else{
                        //Items delete success
                        closeJqxWindowId('jqxwindow<?php echo $jqxPrefix;?>');
                        $('#jqx-notification').jqxNotification({ position: 'bottom-right', template: "success" }).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata');
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('clearselection');
                },
                error: function (request, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    };

    var active<?php echo $jqxPrefix;?> = function (row, datafield, value) {
        return value == '' ? '':'<div style="text-align: center; margin-top: 5px;"><a href="<?php echo asset('/'); ?>'+value+'">Download</a> </div>';
    };
    var print<?php echo $jqxPrefix;?> = function (row, datafield, value) {

    };
    $(document).ready(function () {
        //Button action
        var buttons = ['btn-search<?php echo $jqxPrefix;?>','btn-export<?php echo $jqxPrefix;?>'];
        initialButton(buttons,80,36);
        $('#jqx-form<?php echo $jqxPrefix;?>').jqxValidator({
            hintType: 'label',
            rules: [
                {input: '#div_kindofexpend', message: ' ', action: 'select',
                    rule: function () {
                        if($('input:hidden[name=kindofexpend]').val() == ""){
                            return false;
                        }
                        return true;
                    }
                }
            ]
        });
        var sart_datetime_val = $('#start_datetime').val();
        var end_datetime_val = $('#end_datetime').val();
        getJqxCalendar('div_start_datetime','start_datetime',277,35,sart_datetime_val);
        getJqxCalendar('div_end_datetime','end_datetime',277,35,end_datetime_val);
        var dataAdapter = new $.jqx.dataAdapter(source<?php echo $jqxPrefix;?>);
        // create Tree Grid
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid({
                theme:jqxTheme,
                width:'100%',
                height:gridHeight,
                rowsheight:rowsheight,
                source: dataAdapter,
                //selectionmode: 'checkbox',
                filterable: false,
                showfilterrow: false,
                sortable: true,
                pageable: false,
                virtualmode: true,
                pagerMode: 'advanced',
                pageSize: {{$constant['pageSize']}},
                pageSizeOptions: {{$constant['pageSizeOptions']}},
                rendergridrows: function(obj) {
                    return obj.data;
                },
                columns: [
                    { text: '{{$constant['id_doc']}}', dataField: 'id_doc', width: '8%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['date']}}', dataField: 'date', width: '14%', align:'center', cellsalign:'center', filtertype:'date',cellsformat:'dd/MM/yyyy' },
                    { dataField: 'end_date', hidden:true },
                    { text: '{{$constant['id_mef']}}', dataField: 'id_mef', width: '14%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['chapter']}}', dataField: 'chapter', width: '10%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['chapter1']}}', dataField: 'chapter1', width: '10%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['chapter2']}}', dataField: 'chapter2', width: '10%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['chapter3']}}', dataField: 'chapter3', width: '10%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['money']}}', dataField: 'money', align:'center', width: '14%', cellsalign:'right'},
                    { text: '{{$constant['typemoney']}}', dataField: 'typemoney', align:'center', width: '6%' ,align:'center', cellsalign:'center' },
                    { text: '{{$constant['money1']}}', dataField: 'money1', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['money2']}}', dataField: 'money2', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['money3']}}', dataField: 'money3', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['outcome']}}', dataField: 'outcome', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['typeoutcome']}}', dataField: 'typeoutcome', align:'center', width: '6%' ,align:'center', cellsalign:'center' },
                    { text: '{{$constant['outcome1']}}', dataField: 'outcome1', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['outcome2']}}', dataField: 'outcome2', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['outcome3']}}', dataField: 'outcome3', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['result']}}', dataField: 'result', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['result1']}}', dataField: 'result1', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['result2']}}', dataField: 'result2', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['result3']}}', dataField: 'result3', align:'center', width: '14%', cellsalign:'right' },
                    { text: '{{$constant['kindof_expend']}}', dataField: 'kindof_expend', align:'center', width: '16%', cellsalign:'center' },
                    { text: '{{$constant['kindof_expend1']}}', dataField: 'kindof_expend1', align:'center', width: '16%', cellsalign:'center' },
                    { text: '{{$constant['kindof_expend2']}}', dataField: 'kindof_expend2', align:'center', width: '16%', cellsalign:'center' },
                    { text: '{{$constant['kindof_expend3']}}', dataField: 'kindof_expend3', align:'center', width: '16%', cellsalign:'center' },
                    { text: '{{$constant['objectiv_outcome']}}', dataField: 'objectiv_outcome', align:'center', width: '60%' },
                    { text: '{{$constant['note']}}', dataField: 'note', align:'center', width: '20%' }
                ]
        });

        $("#btn-search<?php echo $jqxPrefix;?>").on('click',function(){
            initialSearch();
        });

        //listbox
        $("#div_listBox").jqxListBox({
             source: <?php echo $ministry;?>,
             width: '100%',
             height: '200px',
             theme:'jqxTheme',
             checkboxes:true,
             filterable:true,
             filterPlaceHolder:"ស្វែងរក",
             searchMode:"contains",
             displayMember: 'text',
             valueMember: 'value'
        });

        //div_expand
        initDropDownList(jqxTheme,'100%',35, '#div_kindofexpend', <?php echo $kindofexpend;?>, 'text', 'value', false, '', '0', "#kindofexpend","{{trans('trans.buttonSearch')}}",250);

        $("#btn-export<?php echo $jqxPrefix;?>").on('click',function(){
            $.ajax({
                type: 'post',
                url: '{{$exportUrl}}',
                data:{'_token':'{{ csrf_token() }}'},
                success: function (response, status, xhr) {
                if(response.code == 0){
                    $('#jqx-notification').jqxNotification({ position: 'top-right', template: "warning" }).html(response.message);
                    $("#jqx-notification").jqxNotification("open");
                }else {

                }
            },
            error: function (request, textStatus, errorThrown) {
                console.log(errorThrown);
            }
            });
        });
    });
</script>

<style type="text/css">
    .process_doc
        {
            width: 150px !important;
        }
</style>
@endsection