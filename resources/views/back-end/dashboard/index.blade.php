<?php
$jqxPrefix = '_dashboard';
$newUrl = asset($constant['secretRoute'].'/dashboard/new');
$listUrl = asset($constant['secretRoute'].'/dashboard/index');
$deleteUrl = asset($constant['secretRoute'].'/dashboard/delete');
?>
@extends('layout.back-end')
@section('title','Welcome to DPMIS Homepage')
@section('content')
<div id="content-container" class="content-container">
	<div class="panel">
        <div class="row panel-heading custome-panel-headering">
			<div class="pull-left title-header-panel">
                <div class="col-lg-5 col-xs-12"><i class="fa fa-dashboard fa-lg"></i> {{$constant['dashboard']}}</div>
                <div class="col-lg-6 col-xs-7"></div> 
                <div class="col-xs-1"></div>  
            </div>
        </div>

        <!-- Dashboard content -->
        
        <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>ចំនួនអ្នកប្រើប្រាស់</p>
            </div>
            <div class="icon" style="margin-top: 10px;">
              <i class="fa fa-user-plus"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>ចំនួនលេខកូដក្រសួង</p>
            </div>
            <div class="icon" style="margin-top: 10px;">
              <i class="fa fa-sort-numeric-asc"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>

              <p>ចំនួនឯកសារដែលមិនទាន់ដំណើរការ</p>
            </div>
            <div class="icon" style="margin-top: 10px;">
              <i class="fa fa-file-pdf-o"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>ចំនួនឯកសារដែលបានដំណើរការហើយ</p>
            </div>
            <div class="icon" style="margin-top: 10px;">
              <i class="fa fa-file"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <hr style="border-color:2px solid #01554c; font-size: 2px;">
      <!--end information-->

      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
         
         <div class="col-sm-6 col-xs-6 col-md-4 zoomInUp animated" id="proccess">
            <div class="thumbnail tile tile-medium tile-carrot" style="width: 340px; height: 160px;">
                  <a href="{{url($constant['secretRoute'].'/document')}}" class="fa-links">
                    <h1 style="font-family: 'KHMERMEF2'; font-size: 20px;">ដំណើរការប្រព័ន្ធ</h1><br>
              <i class="fa fa-3x fa-home fa-lg"></i>
              </a>
              </div>
          </div>
        
          <div class="col-sm-6 col-xs-6 col-md-4 zoomInUp animated" id="create_document">
            <div class="thumbnail tile tile-medium tile-green" style="width: 340px; height: 160px;">
                  <a href="{{url($constant['secretRoute'].'/processing-place')}}" class="fa-links">
                    <h1 style="font-family: 'KHMERMEF2'; font-size: 20px;">បង្កើតទីកន្លែងដំណើរការ</h1><br>
              <i class="fa fa-3x fa-file-text-o fa-lg"></i>
              </a>
              </div>
          </div>

          <div class="col-sm-6 col-xs-6 col-md-4 zoomInUp animated" id="search">
            <div class="thumbnail tile tile-medium tile-pumpkin" style="width: 340px; height: 160px;">
                  <a href="{{url($constant['secretRoute'].'/kindof-expend')}}" class="fa-links">
                    <h1 style="font-family: 'KHMERMEF2'; font-size: 20px;">បង្កើតប្រភេទចំណាយ</h1><br>
              <i class="fa fa-3x fa-link fa-lg"></i>
              </a>
              </div>
          </div>

          <div class="col-sm-6 col-xs-6 col-md-4 zoomInUp animated" id="create_user">
            <a href="{{url($constant['secretRoute'].'/user')}}" class="fa-links">
              <div class="thumbnail tile tile-medium tile-teal" style="width: 340px; height: 160px;">
                  <h1 style="font-family: 'KHMERMEF2'; font-size: 20px;">បង្កើតអ្នកប្រើប្រាស់</h1><br>
                  <i class="fa fa-3x fa-user-plus fa-lg"></i>
              </div>
            </a>
          </div>

          <div class="col-sm-6 col-xs-6 col-md-4 zoomInUp animated" id="permission">
            <a href="{{url($constant['secretRoute'].'/permission/index')}}" class="fa-links">
              <div class="thumbnail tile tile-medium tile-wisteria" style="width: 340px; height: 160px;">
                <h1 style="font-family: 'KHMERMEF2'; font-size: 20px;">កំណត់សិទ្ធិ</h1><br>
                <i class="fa fa-3x fa-gavel fa-lg"></i>
              </div>
            </a>
          </div>


          <div class="col-sm-6 col-xs-6 col-md-4 zoomInUp animated" id="report">
            <a href="#" class="fa-links">
              <div class="thumbnail tile tile-medium tile-red" style="width: 340px; height: 160px;">
                <h1 style="font-family: 'KHMERMEF2'; font-size: 20px;">របាយការណ៍</h1><br>
                <i class="fa fa-3x fa-file-pdf-o fa-lg"></i>
              </div>
            </a>
          </div> 

          <div class="col-sm-6 col-xs-6 col-md-4 zoomInUp animated" id="report">
            <a href="{{url($constant['secretRoute'].'/ministry-code')}}" class="fa-links">
              <div class="thumbnail tile tile-medium tile-blue" style="width: 340px; height: 160px;">
                <h1 style="font-family: 'KHMERMEF2'; font-size: 20px;">បង្កើតលេខកូដក្រសួង</h1><br>
                <i class="fa fa-3x fa-sort-numeric-asc fa-lg"></i>
              </div>
            </a>
          </div>


          <div class="col-sm-6 col-xs-6 col-md-4 zoomInUp animated" id="signout">
            <a href="{{asset('auth/logout')}}" class="fa-links">
              <div class="thumbnail tile tile-medium tile-orange" style="width: 340px; height: 160px;">
                <h1 style="font-family: 'KHMERMEF2'; font-size: 20px;">ចាក់ចេញពីកម្មវិធី</h1><br>
                <i class="fa fa-3x fa-sign-out fa-lg"></i>
              </div>
            </a>
            
          </div>




         


        </section>
        <!-- /.Left col -->
        
      <!-- /.row (main row) -->

	<!-- End dashboard Content -->
    </div>
</div>
<style type="text/css">
  a.fa-user
    {
      color: #FFFFFF !important;
    }
	.p-dashboard{
		font-family:KhmerOSBattambang;
		font-size:20px;
		font-weight:800;
		padding:15px;
		height:auto;
	}
	.p-dashboard a{
		color:#FFFFFF;
	}
	.p-dashboard a:hover{
		color:#fabb24;
		text-decoration: none;
	}
	.dashboard-wrapper{
		background-color:#507335;
		line-height: 50px;
		margin:10px;
	}
	.dashboard-wrapper img{
		width:80px;
		height:80px;
		float:left;
		margin-right:5px;
		margin-top: 0px;
		padding:5px;
	}
	.title-header-panel{
		font-family: KhUniR1;
		line-height: 0px;
		padding-top: 10px;
	}

  #proccess
    {
      animation-duration: 1s;
      animation-delay: 1s;
      
    }
  #create_document
    {
      animation-duration: 1s;
      /*animation-name:fadeInRight;*/
      animation-delay: 1s;
      /*animation-iteration-count: infinite;*/
    }
  #search
    {
      animation-duration: 1s;
      animation-delay: 1s;
     
    }
  
  #create_user
    {
      animation-duration: 1s;
      animation-delay: 1s;
      
    }
  
  
  #permission
    {
      animation-duration:1s;
      animation-delay: 1s;
      
    }
  #report
    {
      animation-duration: 1s;
      animation-delay: 1s;
     
    }
  #signout
    {
      animation-duration: 1s;
      animation-delay: 1s;
     
    }

</style>
@endsection