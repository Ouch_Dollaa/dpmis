<?php
$jqxPrefix = '_document';
$saveUrl = asset($constant['secretRoute'].'/document/processing-save');
$deleteUrl = asset($constant['secretRoute'].'/document/processing-delete');
$showUrl = asset($constant['secretRoute'].'/document/show');
?>
<div class="container-fluid">
   <div class="box box-success box-solid">
        <div class="box-header with-border ui-sortable-handle" style="cursor: move;">
          <h3 class="box-title" style="font-family: KHMERMEF1; font-size: 18px;"><i class="fa fa-file-pdf-o fa-lg"></i> ដំណើរការឯកសារ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
           <form class="form-horizontal" role="form" method="post" name="jqx-form<?php echo $jqxPrefix;?>" id="jqx-form<?php echo $jqxPrefix;?>" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="ajaxRequestJson" value="true" />
                <input type="hidden" id="id_doc" name="id_doc" value="{{isset($id_doc) ? $id_doc:0}}">
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px"><span class="red-star">*</span>ទីកន្លែងដំណើរការ :</div>
                    <div class="col-sm-6">
                        <input type="hidden" name="processing" id="processing">
                        <div id="div_new_process"></div>
                    </div>
                    <div class="col-sm-3">
                        <a href="{{url($constant['secretRoute'].'/processing-place')}}" class="btn btn-success btn-block"><i class="fa fa-cogs fa-lg"></i> បង្កើតទីកន្លែងដំណើរការថ្មី</a>
                    </div>
                </div><!--end-->
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px"><span class="red-star">*</span>ទីកន្លែងដំណើរការ :</div>
                    <div class="col-sm-9">
                        <input type="hidden" name="date_process" id="date_process" class="form-control" placeholder="ទីកន្លែងដំណើរការ">
                        <div id="div_date_process"></div>
                    </div>
                </div><!--end-->
                <div class="form-group">
                    <div class="col-sm-3" style="padding:10px"><span class="red-star">*</span>បរិយាយ :</div>
                    <div class="col-sm-9">
                        <textarea class="form-control" id="other" name="other" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <input type="hidden" id="isfrish" name="isfrish">
                        <div id="active-checkbox"> {{$constant['isfrish']}}</div>
                    </div>
                </div>
                 <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-2">
                        <button id="jqx-save<?php echo $jqxPrefix;?>" type="button" style="background-color: #00a65a !important; background-image: none !important; border: none;"><span class="glyphicon glyphicon-check"></span> {{$constant['buttonSave']}}</button>
                    </div>
                </div>
                    <!-- start -->
                <div class="box box-warning box-solid">
          </div>
          <!-- start -->
              <div class="box box-success box-solid">
                <div class="box-header with-border ui-sortable-handle" style="cursor: move;">
                  <h3 class="box-title" style="font-family: KHMERMEF1; font-size: 18px;"><i class="fa fa-files-o fa-lg"></i> ដំណើរការឯកសារថ្មី</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                 <div class="box-body">
                    <table id="tbl_processing" class="table table-hover table-bordered">
                       <thead>
                          <tr>
                              <th>លុប</th>
                              <th style="display:none">id</th>
                              <th>ល.រ</th>
                              <th style="display:none">លេខរៀងឯកសារ</th>
                              <th>ដំណើរការ</th>
                              <th>ថ្ងៃខែឆ្នាំដំណើរការ</th>
                              <th>បញ្ចប់ដំណើរការ</th>
                              <th>បរិយាយ</th>
                          </tr>
                       </thead>
                       <tbody>
                       @foreach($query_processing as $key=>$val)
                           <tr class="delete_set_result" id="{{$val->id}}">
                              <th><a href="#" onclick="deleterow(this)" id="{{$val->id}}" name="{{$val->id}}">លុប</a></th>
                              <td style="display:none">{{$val->id}}</td>
                              <td>{{$key+1}}</td>
                              <td style="display:none">{{$val->id_doc}}</td>
                              <td>{{$val->processing}}</td>
                              <td>{{$val->date_process}}</td>
                              <td>{{$val->isfrish}}</td>
                              <td>{{$val->other}}</td>
                          </tr>
                       @endforeach
                       </tbody>
                    </table>
                 </div>
                <!-- /.box-body -->
              </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
</div>
<script>
    initDropDownList(jqxTheme,'100%',35, '#div_new_process', <?php echo $new_process;?>, 'text', 'value', false, '', '0', "#processing","{{trans('trans.buttonSearch')}}",250);
    $(document).ready(function(){
		var buttons = ['jqx-save<?php echo $jqxPrefix;?>'];
        initialButton(buttons,90,30);
        //Active
        var isfrish = $('#isfrish').val() == 1 ? true:false;
        $("#active-checkbox").jqxCheckBox({ theme: jqxTheme,width: 120, height: 25, checked: isfrish});
        $('#active-checkbox').on('change', function (event) {
            event.args.checked == true ? $('#isfrish').val(1):$('#isfrish').val(0);
        });
        //Form Validation here
        $('#jqx-form<?php echo $jqxPrefix;?>').jqxValidator({
            hintType: 'label',
            rules: [
                {input: '#div_new_process', message: ' ', action: 'select',
                    rule: function () {
                        if($("#processing").val() == ""){
                            return false;
                        }
                        return true;
                    }
                },
                {input: '#div_date_process', message: ' ', action: 'select',
                    rule: function () {
                        if($("#date_process").val() == ""){
                            return false;
                        }
                        return true;
                    }
                }
            ]
        });
		//Save action button
        $("#jqx-save<?php echo $jqxPrefix;?>").click(function(){
            saveJqxItemNotClose('{{$jqxPrefix}}', '{{$saveUrl}}', '{{ csrf_token() }}');
            var id_doc = $('#id_doc').val();
            $.ajax({
                type: 'post',
                url: '{{$showUrl}}',
                data:{'id_doc':id_doc,'_token':'{{ csrf_token() }}'},
                success: function (data) {
                    var tbl = document.getElementById("tbl_processing");
                    var count = data.length;                    
                    for (var x=count-1; x>0; x--) {
                        tbl.deleteRow(x);
                    }
                    for(var i=0; i<count; i++){
                        var row = tbl.insertRow(i+1);                        
                        console.log(data[i].id);
                        row.insertCell(0).innerHTML = '<a href="#" onclick="deleterow(this)" id="' +data[i].id+ '" name="'+data[i].id+'">លុប</a>'
                        row.insertCell(1).innerHTML = i+1;
                        row.insertCell(2).innerHTML = data[i].processing;
                        row.insertCell(3).innerHTML = data[i].date_process;
                        row.insertCell(4).innerHTML = data[i].other;
                        row.insertCell(5).innerHTML = data[i].isfrish;
                    }
                },
                error: function (request, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        });
    });
    var currentDate = $('#date_process').val() != null ? $('#date_process').val():null;
    getJqxCalendar('div_date_process','date_process','830px','32px','ថ្ងៃខែឆ្នាំកំណើត',currentDate);
    // select change date
    $('#div_date_process').on('change', function (event) {
        var first_dob = $('#date_process').val();
        var separate_dob = first_dob.split("/");
        var date  = separate_dob[0];
        var month = separate_dob[1];
        var year  = separate_dob[2];
        var full_date = year+'-'+month+'-'+date;
        $('#full_date').attr('value',full_date);
    });

    //delete row in database
    function deleteRecord(id){
        $.ajax({
            type: "post",
            dataType: "json",
            url: "<?php echo $deleteUrl;?>",
            cache: false,
            data: {
                "id":id,
                "_token":'{{ csrf_token() }}',
                'ajaxRequestJson':'true'
            },
            beforeSend:function(){

            },
            success: function (response, status, xhr) {
                if(response.code == 0){
                    $('#jqx-notification').jqxNotification({ position: 'top-right', template: "warning" }).html(response.message);
                    $("#jqx-notification").jqxNotification("open");
                }else {
                    $('#'+id).remove();
                }
            },
            error: function (request, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    function deleterow(row) {
        var id = $(row).attr('id');
        var title = '{{$constant['buttonDelete']}}';
        var content = 'តើអ្នកពិតជាចង់លុបទិន្នន័យនេះឬ?';
        confirmDelete(title,content,function () {
            deleteRecord(id);
            var i = row.parentNode.parentNode.rowIndex;
            document.getElementById("tbl_processing").deleteRow(i);
        });        
    }
</script>

<style type="text/css">
    table tr td input[type=text]
        {
            border:1px box-solid #00cc0c;
            border-style: none;
            font-size: 16px;
            height: 35px;
            width: 100%;
        }
    .jqx-notification-mail, .jqx-primary
        {
            background-image: none;
        }
</style>