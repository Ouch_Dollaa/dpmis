<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" charset=utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('icon/uef.ico')}}" />
    <title>Welcome to DPMIS Homepage</title>
    <link rel="stylesheet" href="{{asset('css/global.css')}}" type="text/css" />
    <!-- Font Awesome -->
  	<link rel="stylesheet" href="{{asset('asset/font-awesome/css/font-awesome.min.css')}}">
  	<link rel="stylesheet" href="{{asset('css/metro-bootstrap.min.css')}}">
    <!--full calenda-->
  	<link rel="stylesheet" href="{{asset('asset/plugins/fullcalendar/fullcalendar.min.css')}}">
  	<link rel="stylesheet" href="{{asset('asset/plugins/fullcalendar/fullcalendar.print.css')}}" media="print">
  	<!-- Theme style -->
    <link rel="stylesheet" href="{{asset('asset/css/AdminLTE.min.css')}}">
  	<!-- Ionicons -->
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="{{asset('asset/css/skins/_all-skins.min.css')}}">
  	<!-- iCheck -->
  	<link rel="stylesheet" href="{{asset('asset/plugins/iCheck/flat/blue.css')}}">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="{{asset('asset/plugins/morris/morris.css')}}">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="{{asset('asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="{{asset('asset/plugins/datepicker/datepicker3.css')}}">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="{{asset('asset/plugins/daterangepicker/daterangepicker.css')}}">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="{{asset('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    {{--confirm delete--}}
    <link rel="stylesheet" href="{{asset('css/jquery-confirm.min.css')}}">
  	<script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
<style>
	body	
		{
			font-size:12px;
			font-family: 'KHMERMEF1';
		}
    #btn-print{
        background-size: 20px;
        padding:5px 20px;
        border: 1px solid #6a6868 !important;
        border-radius: 5px 5px; 
    }
    @font-face {
    font-family: 'KHMERMEF1';
    }
    @font-face {
    font-family: 'KHMERMEF1';
    }
    @page  {
        size: auto;   
        margin:20px 30px 20px 30px;
        size: A4;
    }
    @media  print {
        
        table{page-break-inside: avoid;}
    }
    table thead tr th
    	{
    		font-size: 17px;
    		background-color: green;
    		color: white;
    		padding: 5px;
    		border-radius: 3px;
    		text-align: center;
    	}
    table tbody tr td
    	{
    		padding: 5px;
    	}
</style>
</head>
<body>
	<div class="container-fluid">
		<table style="position:absolute;right:25px;top:30px;font-size:10px;font-weight: bold;z-index:9999;">
			<tbody>
				<tr>
					<td>
						<button id="btn-print" value=" " onclick="window.print()" style="font-size: 16px; color: currentcolor; background-color: inherit; border-color: chartreuse; display: inline-block;"><i class="fa fa-print fa-lg" aria-hidden="true"></i></button>
					</td>
				</tr>
			</tbody>
		</table>
		<div style="line-break: loose;page-break-before: always">
		<table style="width:100%;margin-top:20px;">
				<tbody>
					<tr>
						<td style="width:100%;text-align:center;font-family:'khmer mef2';font-size:24px;">
							ដំណើរការឯកសារ
						</td>
					</tr>

				</tbody>
			</table>
			<table style="width: 100%; margin-top: 20px;">
				<tbody>
					<tr>
						<td style="width: 15%"></td>
						<td>លេខលិខិត</td>
						<td>:</td>
						<td>{{ isset($document->id_doc) ? $document->id_doc : '' }}</td>
					</tr>
					<tr>
						<td style="width: 15%"></td>
						<td style="width: 30%">កាលបរិច្ឆេទ</td>
						<td>:</td>
						<td>{{ isset($document->date) ? $document->date : '' }}</td>
					</tr>
					<tr>
						<td style="width: 15%"></td>
						<td style="width: 30%">កូដក្រសួង</td>
						<td>:</td>
						<td>{{ isset($document->id_mef) ? $document->id_mef : '' }}</td>
					</tr>
					<tr>
						<td style="width: 15%"></td>
						<td style="width: 30%">ជំពូក</td>
						<td>:</td>
						<td>{{ isset($document->chapter) ? $document->chapter : '' }}</td>
					</tr>
					<tr>
						<td style="width: 15%"></td>
						<td style="width: 30%">ទឹកប្រាក់</td>
						<td>:</td>
						<td>{{ isset($document->money) ? $document->money : '' }}</td>
					</tr>
					<tr>
						<td style="width: 15%"></td>
						<td style="width: 30%">ប្រភេទចំណាយ </td>
						<td>:</td>
						<td>{{ isset($document->kindof_expend) ? $document->kindof_expend : '' }}</td>
					</tr>
					<tr>
						<td style="width: 15%"></td>
						<td style="width: 30%">កម្មវត្ថុចំណាយ</td>
						<td>:</td>
						<td>{{ isset($document->objectiv_outcome) ? $document->objectiv_outcome : '' }}</td>
					</tr>
					<tr>
						<td style="width: 15%"></td>
						<td style="width: 30%">អ្នកបញ្ចូល</td>
						<td>:</td>
						<td>{{ isset($document->user_name) ? $document->user_name : '' }}</td>
					</tr>
					<tr>
						<td style="width: 15%"></td>
						<td style="width: 30%">កាលបរិច្ឆេទបញ្ចូល </td>
						<td>:</td>
						<td>{{ isset($document->created) ? $document->created : '' }}</td>
					</tr>
				</tbody>
			</table>
			<table align="center" style="width: 80%;margin-top: 20px;">
				<thead>
					<tr>
						<th>ទីកន្លែងដំណើរការ</th>
						<th>កាលបរិច្ឆេទបញ្ចូល </th>
						<th>បរិយាយ</th>
					</tr>
				</thead>
				<tbody>
				@foreach($processing as $key=>$val)
					<tr>
						<td>{{$val->processing}}</td>
						<td>{{$val->date_process}}</td>
						<td>{{$val->other}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<script>
	(function() {
		var beforePrint = function() {
			$('#btn-print').hide();
		};
		var afterPrint = function() {
			$('#btn-print').show();
		};
		if (window.matchMedia) {
			var mediaQueryList = window.matchMedia('print');
			mediaQueryList.addListener(function(mql) {
				if (mql.matches) {
					beforePrint();
				} else {
					afterPrint();
				}
			});
		}
		window.onbeforeprint = beforePrint;
		window.onafterprint = afterPrint;
	}());
	</script>
</body>
