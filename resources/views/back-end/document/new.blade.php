<?php
$jqxPrefix = '_document';
$saveUrl = asset($constant['secretRoute'].'/document/save');
$newUrl = asset($constant['secretRoute'].'/document/new');
?>
<div class="container-fluid">
   <div class="box box-success box-solid">
        <div class="box-header with-border ui-sortable-handle" style="cursor: move;">
          <h3 class="box-title" style="font-family: KHMERMEF1; font-size: 18px;"><i class="fa fa-file-pdf-o"></i> ដំណើរការឯកសារទូទៅ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
           <form class="form-horizontal" role="form" method="post" name="jqx-form<?php echo $jqxPrefix;?>" id="jqx-form<?php echo $jqxPrefix;?>" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="ajaxRequestJson" value="true" />
                <input type="hidden" id="id" name="id" value="{{isset($row->id) ? $row->id:0}}">
                <div class="form-group">
                    <div class="col-sm-2" style="padding:10px"><span class="red-star">*</span>លេខរៀង</div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="លេខរៀង" id="no" name="no" autocomplete="off" value="{{isset($row->no) ? $row->no:''}}">
                    </div>
                    <div class="col-sm-2" style="padding:10px"><span class="red-star">*</span>លេខលិខិត</div>
                    <div class="col-sm-4">
                    <input type="text" name="no_docx" id="no_docx" class="form-control" placeholder="លេខលិខិត" value="{{isset($row->id_doc) ? $row->id_doc:''}}">
                    </div>
                </div><!--end-->
                <div class="form-group">
                    <div class="col-sm-2" style="padding:10px"><span class="red-star">*</span>កាលបរិច្ឆេទ</div>
                    <div class="col-sm-4">
                        <input type="hidden" name="currentDate" id="currentDate" class="form-control" placeholder="កាលបរិច្ឆេទ" value="{{ isset($row->date) ? $row->date : '' }}">
                        <div id="div_currentDate"></div>
                    </div>
                    <div class="col-sm-2" style="padding:10px"><span class="red-star">*</span>លេខកូដក្រសួង</div>
                    <div class="col-sm-4">
                        <input type="hidden" name="id_mef" id="id_mef<?php echo $jqxPrefix;?>" value="{{ isset($row->id_mef) ? floatval($row->id_mef) : '' }}">
                        <div id="d_id_mef<?php echo $jqxPrefix; ?>"></div>
                    </div>
                </div><!--end-->
                <div class="form-group">
                    <div class="col-sm-2" style="padding:10px"><span class="red-star">*</span>កម្មវត្ថុចំណាយ</div>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="objective" name="objective" rows="3" >{{ isset($row->objectiv_outcome) ? strval($row->objectiv_outcome) : '' }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2" style="padding:10px"><span class="red-star">*</span>ឯកសារ</div>
                    <div class="col-sm-10">
                        <?php $avatar = isset($row->avatar) ? $row->avatar : asset('images/default.png'); ?>
                        <input type="file" value="" class="form-control" id="my-avatar" name="avatar" id="avatar" accept="*">
                        <div class="wrap-avatar" id="wrap-avatar">
                            <input type="hidden" name="statusRemovePicture" value="0" id="statusRemovePicture" />
                            <a href="{{$avatar == "" ? asset("images/default.png") : asset($avatar)}}">{{$constant['download']}}</a>
                            <?php $statusRemoveAvatar = isset($row->avatar) ? $row->avatar : ""; ?>
                            <span class="remove-avatar {{$statusRemoveAvatar == '' ? "display-none" : ''}}"><i class="glyphicon glyphicon-remove" style="display: none"></i></span>
                        </div>
                    </div>
                </div>
               <div class="form-group">
                   <div class="col-sm-2" style="padding:10px"><span class="red-star">*</span>លេខអាណត្តិ</div>
                   <div class="col-sm-4">
                       <input type="text" name="no_mandate" id="no_mandate" class="form-control" placeholder="លេខអាណត្តិ" value="{{isset($row->no_mandate) ? $row->no_mandate:''}}">
                   </div>
                   <div class="col-sm-2" style="padding:10px"><span class="red-star">*</span>ថ្ងៃខែឆ្នាំចុះអាណត្តិ</div>
                   <div class="col-sm-4">
                       <input type="hidden" name="date_mandate" id="date_mandate" class="form-control" placeholder="ថ្ងៃខែឆ្នាំចុះអាណត្តិ" value="{{ isset($row->date_mandate) ? $row->date_mandate : '' }}">
                       <div id="div_date_mandate"></div>
                   </div>
               </div><!--end-->
                <div class="box box-success box-solid">
            <div class="box-header with-border ui-sortable-handle" style="cursor: move;">
              <h3 class="box-title" style="font-family: KHMERMEF1; font-size: 18px;"><i class="fa fa-trophy"></i> ការស្នើរសុំចំណាយ</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-hover table-bordered">
               <thead>
                    <tr>
                        <th>ជំពូក</th>
                        <th>ទឹកប្រាក់ស្នើសុំ</th>
                        <th>ទឹកប្រាក់ផ្តល់</th>
                        <th>ប្រភេទចំណាយ</th>
                        <th>បញ្ជាក់</th>
                    </tr>
                </thead>
                <tbody id="moneyInfo">
                        <tr>
                            <td style="height: 35px !important"><input type="text" name="chapter" id="chapter" placeholder="ជំពូក" value="{{ isset($row->chapter) ? $row->chapter : '' }}"></td>
                            <td><input type="text" name="moneyRequest" id="moneyRequest" placeholder="ទឹកប្រាក់ស្នើសុំ" value="{{ isset($row->money) ? $row->money : '' }}" onkeyup="findTotal_moneyRequest();"></td>
                            <td><input type="text" name="moneyPay" id="moneyPay" placeholder="ទឹកប្រាក់ផ្តល់" value="{{ isset($row->outcome) ? $row->outcome : '' }}" onkeyup="findTotal_moneyPay();"></td>
                            <td>
                                <input type="hidden" name="kindofexpend" id="kindofexpend" value="{{ isset($row->kindof_expend) ? $row->kindof_expend : '' }}">
                                <div id="div_kindofexpend"></div>
                            </td>
                            <td>
                                <input type="text" name="other" id="other" placeholder="បញ្ជាក់" value="{{ isset($row->note) ? $row->note : '' }}">
                            </td>
                        </tr>
                        <script type="text/javascript">
                            $(function () {
                                initDropDownList(jqxTheme,'100%',35, '#div_kindofexpend', <?php echo $kindofexpend;?>, 'text', 'value', false, '', '0', "#kindofexpend","{{trans('trans.buttonSearch')}}",250);
                                
                                $('#div_kindofexpend').on('select',function (events) {
                                    var items = args.item;
                                    var label = items.label;
                                    var value = items.value;
                                    $('#kindofexpend').val(value);
                                });
                            });
                        </script>
                        <tr>
                            <td style="height: 35px !important"><input type="text" name="chapter_1" id="chapter_1" placeholder="ជំពូក" value="{{ isset($row->chapter1) ? $row->chapter1 : '' }}"></td>
                            <td><input type="text" name="moneyRequest_1" id="moneyRequest_1" placeholder="ទឹកប្រាក់ស្នើសុំ" value="{{ isset($row->money1) ? $row->money1 : '' }}" onkeyup="findTotal_moneyRequest();"></td>
                            <td><input type="text" name="moneyPay_1" id="moneyPay_1" placeholder="ទឹកប្រាក់ផ្តល់" value="{{ isset($row->outcome1) ? $row->outcome1 : '' }}" onkeyup="findTotal_moneyPay();"></td>
                            <td>
                                <input type="hidden" name="kindofexpend_1" id="kindofexpend_1" value="{{ isset($row->kindof_expend1) ? $row->kindof_expend1 : '' }}">
                                <div id="div_kindofexpend_1"></div>
                            </td>
                            <td>
                                <input type="text" name="other_1" id="other_1" placeholder="បញ្ជាក់" value="{{ isset($row->note1) ? $row->note1 : '' }}">
                            </td>
                        </tr>
                        <script type="text/javascript">
                            $(function () {
                                initDropDownList(jqxTheme,'100%',35, '#div_kindofexpend_1', <?php echo $kindofexpend;?>, 'text', 'value', false, '', '0', "#kindofexpend_1","{{trans('trans.buttonSearch')}}",250);
                                $('#div_kindofexpend_1').on('select',function (events) {
                                    var items = args.item;
                                    var label = items.label;
                                    var value = items.value;
                                    $('#kindofexpend_1').val(value);
                                });
                            });
                        </script>

                        <tr>
                            <td style="height: 35px !important"><input type="text" name="chapter_2" id="chapter_2" placeholder="ជំពូក" value="{{ isset($row->chapter2) ? $row->chapter2 : '' }}"></td>
                            <td><input type="text" name="moneyRequest_2" id="moneyRequest_2" placeholder="ទឹកប្រាក់ស្នើសុំ" value="{{ isset($row->money2) ? $row->money2 : '' }}" onkeyup="findTotal_moneyRequest();"></td>
                            <td><input type="text" name="moneyPay_2" id="moneyPay_2" placeholder="ទឹកប្រាក់ផ្តល់" value="{{ isset($row->outcome2) ? $row->outcome2 : '' }}" onkeyup="findTotal_moneyPay();"></td>
                            <td>
                                <input type="hidden" name="kindofexpend_2" id="kindofexpend_2" value="{{ isset($row->kindof_expend2) ? $row->kindof_expend2 : '' }}">
                                <div id="div_kindofexpend_2"></div>
                            </td>
                            <td>
                                <input type="text" name="other_2" id="other_2" placeholder="បញ្ជាក់" value="{{ isset($row->note2) ? $row->note2 : '' }}">
                            </td>
                        </tr>
                        <script type="text/javascript">
                            $(function () {
                                initDropDownList(jqxTheme,'100%',35, '#div_kindofexpend_2', <?php echo $kindofexpend;?>, 'text', 'value', false, '', '0', "#kindofexpend_2","{{trans('trans.buttonSearch')}}",250);
                                $('#div_kindofexpend_2').on('select',function (events) {
                                    var items = args.item;
                                    var label = items.label;
                                    var value = items.value;
                                    $('#kindofexpend_2').val(value);
                                });
                            });
                        </script>
                        <tr>
                            <td style="height: 35px !important"><input type="text" name="chapter_3" id="chapter_3" placeholder="ជំពូក" value="{{ isset($row->chapter3) ? $row->chapter3 : '' }}"></td>
                            <td><input type="text" name="moneyRequest_3" id="moneyRequest_3" placeholder="ទឹកប្រាក់ស្នើសុំ" value="{{ isset($row->money3) ? $row->money3 : '' }}" onkeyup="findTotal_moneyRequest();"></td>
                            <td><input type="text" name="moneyPay_3" id="moneyPay_3" placeholder="ទឹកប្រាក់ផ្តល់" value="{{ isset($row->outcome3) ? $row->outcome3 : '' }}" onkeyup="findTotal_moneyPay();"></td>
                            <td>
                                <input type="hidden" name="kindofexpend_3" id="kindofexpend_3" value="{{ isset($row->kindof_expend3) ? $row->kindof_expend3 : '' }}">
                                <div id="div_kindofexpend_3"></div>
                            </td>
                            <td>
                                <input type="text" name="other_3" id="other_3" placeholder="បញ្ជាក់" value="{{ isset($row->note3) ? $row->note3 : '' }}">
                            </td>
                        </tr>
                        <script type="text/javascript">
                            $(function () {
                                initDropDownList(jqxTheme,'100%',35, '#div_kindofexpend_3', <?php echo $kindofexpend;?>, 'text', 'value', false, '', '0', "#kindofexpend_3","{{trans('trans.buttonSearch')}}",250);
                                $('#div_kindofexpend_3').on('select',function (events) {
                                    var items = args.item;
                                    var label = items.label;
                                    var value = items.value;
                                    $('#kindofexpend_3').val(value);
                                });
                            });
                        </script>
                </tbody>
                <tr>
                    <td class="text-center" style="font-family: KHMERMEF1;font-size: 16px;">សរុប៖</td>
                    <td><input type="text" name="total_moneyrequest" id="total_moneyrequest" placeholder="ទឹកប្រាក់ស្នើសុំ" ></td>
                    <td><input type="text" name="total_moneypay" id="total_moneypay" placeholder="ទឹកប្រាក់ផ្តល់" ></td>
                    <td colspan="3"></td>
                </tr>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
                <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-2">
                        <button id="jqx-save<?php echo $jqxPrefix;?>" type="button" style="background-color: #00a65a !important; background-image: none !important; border: none;"><span class="glyphicon glyphicon-check"></span> {{$constant['buttonSave']}}</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
</div>

<script>
    function inrFormat(val) {
        var x = val;
        x = x.toString();
        var afterPoint = '';
        if (x.indexOf('.') > 0)
            afterPoint = x.substring(x.indexOf('.'), x.length);
        x = Math.floor(x);
        x = x.toString();
        var lastThree = x.substring(x.length - 3);
        var otherNumbers = x.substring(0, x.length - 3);
        if (otherNumbers != '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + lastThree + afterPoint;
        return res;
    }

    function findTotal_moneyRequest(){
        var moneyRequest = parseInt($('#moneyRequest').val());
        var moneyRequest_1 = parseInt($('#moneyRequest_1').val());
        var moneyRequest_2 = parseInt($('#moneyRequest_2').val());
        var moneyRequest_3 = parseInt($('#moneyRequest_3').val());
        if (isNaN(moneyRequest)){
            moneyRequest = parseInt(0);
        }
        if (isNaN(moneyRequest_1)){
            moneyRequest_1 = parseInt(0);
        }
        if (isNaN(moneyRequest_2)){
            moneyRequest_2 = parseInt(0);
        }
        if (isNaN(moneyRequest_3)){
            moneyRequest_3 = parseInt(0);
        }
        var result = parseInt(moneyRequest) + parseInt(moneyRequest_1) + parseInt(moneyRequest_2) + parseInt(moneyRequest_3);
        $('#total_moneyrequest').val(inrFormat(result));
    }

    function findTotal_moneyPay(){
        var moneyPay = parseInt($('#moneyPay').val());
        var moneyPay_1 = parseInt($('#moneyPay_1').val());
        var moneyPay_2 = parseInt($('#moneyPay_2').val());
        var moneyPay_3 = parseInt($('#moneyPay_3').val());
        if (isNaN(moneyPay)){
            moneyPay = parseInt(0);
        }
        if (isNaN(moneyPay_1)){
            moneyPay_1 = parseInt(0);
        }
        if (isNaN(moneyPay_2)){
            moneyPay_2 = parseInt(0);
        }
        if (isNaN(moneyPay_3)){
            moneyPay_3 = parseInt(0);
        }
        var result = parseInt(moneyPay) + parseInt(moneyPay_1) + parseInt(moneyPay_2) + parseInt(moneyPay_3);
        $('#total_moneypay').val(inrFormat(result));
    }

    $(document).ready(function(){
        initDropDownList(jqxTheme,'100%',35, '#d_id_mef<?php echo $jqxPrefix; ?>', <?php echo $id_mef;?>, 'text', 'value', false, '', '0', "#id_mef<?php echo $jqxPrefix;?>","{{trans('trans.buttonSearch')}}",250);
		var buttons = ['jqx-save<?php echo $jqxPrefix;?>'];
        initialButton(buttons,90,30);
        //Form Validation here
        findTotal_moneyRequest();
        findTotal_moneyPay();
        $('#jqx-form<?php echo $jqxPrefix;?>').jqxValidator({
            hintType: 'label',
            rules: [
                {input: '#d_id_mef<?php echo $jqxPrefix; ?>', message: ' ', action: 'select',
                    rule: function () {
                        if($("#id_mef<?php echo $jqxPrefix; ?>").val() == ""){
                            return false;
                        }
                        return true;
                    }
                },
                {input: '#div_currentDate', message: ' ', action: 'select',
                    rule: function () {
                        if($("#currentDate").val() == ""){
                            return false;
                        }
                        return true;
                    }
                },
                {
                    input: '#no',
                    message: ' ',
                    action: 'blur',
                    rule: 'required'
                },
                {
                    input: '#no_docx',
                    message: ' ',
                    action: 'blur',
                    rule: 'required'
                },
                {
                    input: '#objective',
                    message: ' ',
                    action: 'blur',
                    rule: 'required'
                }
            ]
        });
		//Save action button
        $("#jqx-save<?php echo $jqxPrefix;?>").click(function(){
            var id = $('#id').val();
            if(id != '0'){
                saveJqxItem('{{$jqxPrefix}}', '{{$saveUrl}}', '{{ csrf_token() }}');
            }else{
                saveJqxItemNotClose('{{$jqxPrefix}}', '{{$saveUrl}}', '{{ csrf_token() }}');
            }
        });
        $("#my-avatar").jqxFileUpload();
    });
    var currentDate = $('#currentDate').val() != null ? $('#currentDate').val():null;
    getJqxCalendar('div_currentDate','currentDate','350px','32px','ថ្ងៃខែឆ្នាំកំណើត',currentDate);
    var date_mandate = $('#date_mandate').val() != null ? $('#date_mandate').val():null;
    getJqxCalendar('div_date_mandate','date_mandate','350px','32px','ថ្ងៃខែឆ្នាំកំណើត',date_mandate);


    // select change date
    /*$('#div_currentDate').on('change', function (event) {
        var first_dob = $('#currentDate').val();
        var separate_dob = first_dob.split("/");
        var date  = separate_dob[0];
        var month = separate_dob[1];
        var year  = separate_dob[2];
        var full_date = year+'-'+month+'-'+date;
        $('#full_date').attr('value',full_date);
    });*/

    $(function(){
        // Set up the number formatting.
        $('#moneyRequest').on('change',function(){
            console.log('Change event.');
            var val = $('#moneyRequest').val();
            $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        $('#moneyRequest').change(function(){
            console.log('Second change event...');
        });
        $('#moneyRequest').number( true,0);
        //#txtmoney
        $('#moneyPay').on('change',function(){
            console.log('Change event.');
            var val = $('#moneyPay').val();
            $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        $('#moneyPay').change(function(){
            console.log('Second change event...');
        });
        $('#moneyPay').number( true,0);
        //_1
        $('#moneyRequest_1').on('change',function(){
            console.log('Change event.');
            var val = $('#moneyRequest_1').val();
            $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        $('#moneyRequest_1').change(function(){
            console.log('Second change event...');
        });
        $('#moneyRequest_1').number( true,0);
        //#txtmoney
        $('#moneyPay_1').on('change',function(){
            console.log('Change event.');
            var val = $('#moneyPay_1').val();
            $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        $('#moneyPay_1').change(function(){
            console.log('Second change event...');
        });
        $('#moneyPay_1').number( true,0);
        //_2
        $('#moneyRequest_2').on('change',function(){
            console.log('Change event.');
            var val = $('#moneyRequest_2').val();
            $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        $('#moneyRequest_2').change(function(){
            console.log('Second change event...');
        });
        $('#moneyRequest_2').number( true,0);
        //#txtmoney
        $('#moneyPay_2').on('change',function(){
            console.log('Change event.');
            var val = $('#moneyPay_2').val();
            $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        $('#moneyPay_2').change(function(){
            console.log('Second change event...');
        });
        $('#moneyPay_2').number( true,0);
        //_3
        $('#moneyRequest_3').on('change',function(){
            console.log('Change event.');
            var val = $('#moneyRequest_1').val();
            $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        $('#moneyRequest_3').change(function(){
            console.log('Second change event...');
        });
        $('#moneyRequest_3').number( true,0);
        //#txtmoney
        $('#moneyPay_3').on('change',function(){
            console.log('Change event.');
            var val = $('#moneyPay_3').val();
            $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        $('#moneyPay_3').change(function(){
            console.log('Second change event...');
        });
        $('#moneyPay_3').number( true,0);
    });

</script>

<style type="text/css">
    table tr td input[type=text]
        {
            border:1px box-solid #00cc0c;
            border-style: none;
            font-size: 16px;
            height: 35px;
            width: 100%;
        }
    .jqx-notification-mail, .jqx-primary
        {
            background-image: none;
        }
</style>

