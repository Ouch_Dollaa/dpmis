<?php
$jqxPrefix = '_document';
$newUrl = asset($constant['secretRoute'].'/document/new');
$editUrl = asset($constant['secretRoute'].'/document/edit');
$listUrl = asset($constant['secretRoute'].'/document/index');
$deleteUrl = asset($constant['secretRoute'].'/document/delete');
$processUrl = asset($constant['secretRoute'].'/document/process');
$printUrl = asset($constant['secretRoute'].'/document/print');
$copyUrl = asset($constant['secretRoute'].'/document/copy');
?>
@extends('layout.back-end')
@section('content')
    <div id="content-container" class="content-container">
        <div class="panel">
            <div class="row panel-heading custome-panel-headering">
                <div class="form-group title-header-panel">
                    <div class="pull-left">
                        <div class="col-sm-4 col-xs-12" style="width: 32%"><i class="fa fa-file fa-lg"></i> {{$constant['new_document']}}</div>
                    </div>
                    <div class="text-center"></div>
                    <div class="pull-right">
                        <div class="col-lg-12">
                            <button id="btn-copy<?php echo $jqxPrefix;?>" class="process_doc button-color"><i class="glyphicon glyphicon-file"></i> {{$constant['buttonCopy']}}</button>
                            <button id="btn-new<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-plus"></i> {{$constant['buttonNew']}}</button>
                            <button id="btn-edit<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-edit"></i> {{$constant['buttonEdit']}}</button>
                            <button id="btn-delete<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-trash"></i> {{$constant['buttonDelete']}}</button>
                            <button id="btn-process<?php echo $jqxPrefix;?>" class="process_doc button-color"><i class="glyphicon glyphicon-file"></i> {{$constant['btnProcess']}}</button>
                        </div>
                    </div>
                </div>
                <div id="jqx-grid<?php echo $jqxPrefix;?>"></div>
            </div>
        </div>
    </div>
    
<script type="text/javascript">
    // prepare the data
    var source<?php echo $jqxPrefix;?> = {
        type: "post",
        dataType: "json",
        data:{"_token":'{{ csrf_token() }}'},
        dataFields: [
            { name: 'id', type: 'number' },
            { name: 'no', type: 'string' },
            { name: 'id_doc', type: 'string' },
            { name: 'id_mef', type: 'string' },
            { name: 'chapter', type: 'string' },
            { name: 'date', type: 'string' },
            { name: 'objectiv_outcome', type: 'string' },
            { name: 'kindof_expend', type: 'string' },
            { name: 'no_mandate', type: 'string' },
            { name: 'date_mandate', type: 'string' },
            { name: 'currency', type: 'string' },
            { name: 'outcome', type: 'string' },
            { name: 'username', type: 'string' },
            { name: 'avatar', type: 'string' },
            { name: 'print', type: 'string' }
        ],
        cache: false,
        id: 'id',
        url: '<?php echo $listUrl;?>',
        beforeprocessing: function(data) {
            source<?php echo $jqxPrefix;?>.totalrecords = (data != null)? data.total:0;
        },
        sort: function(data) {
        // Short Data
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'sort');
        },
        filter: function() {
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'filter');
        },
        deleteRow: function (rowid, commit) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: "<?php echo $deleteUrl;?>",
                cache: false,
                data: {"id":rowid,"_token":'{{ csrf_token() }}','ajaxRequestJson':'true'},
                success: function (response, status, xhr) {
                    $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                    if(response.code == 0) {
                        //Some items delete execpt the one in used
                        $('#jqx-notification').jqxNotification({position: 'bottom-right',template: "warning",autoClose: false}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else if(response.code == 1){
                        //Item in used
                        $('#jqx-notification').jqxNotification({position: 'bottom-right',template: "warning"}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else{
                        //Items delete success
                        closeJqxWindowId('jqxwindow<?php echo $jqxPrefix;?>');
                        $('#jqx-notification').jqxNotification({ position: 'bottom-right', template: "success" }).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata');
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('clearselection');
                },
                error: function (request, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    };

    var active<?php echo $jqxPrefix;?> = function (row, datafield, value) {
        return value == '' ? '':'<div style="text-align: center; margin-top: 5px;"><a href="<?php echo asset('/'); ?>'+value+'">Download</a> </div>';
    };
    var print<?php echo $jqxPrefix;?> = function (row, datafield, value) {

    };
    $(document).ready(function () {
        //Button action
        var buttons = ['btn-new<?php echo $jqxPrefix;?>','btn-edit<?php echo $jqxPrefix;?>','btn-delete<?php echo $jqxPrefix;?>','btn-process<?php echo $jqxPrefix;?>','btn-copy<?php echo $jqxPrefix;?>'];
        initialButton(buttons,80,36);
        var dataAdapter = new $.jqx.dataAdapter(source<?php echo $jqxPrefix;?>);
        // create Tree Grid
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid({
                theme:jqxTheme,
                width:'100%',
                height:gridHeight,
                rowsheight:rowsheight,
                source: dataAdapter,
                selectionmode: 'checkbox',
                filterable: true,
                showfilterrow: true,
                sortable: true,
                pageable: true,
                virtualmode: true,
                pagerMode: 'advanced',
                pageSize: {{$constant['pageSize']}},
                pageSizeOptions: {{$constant['pageSizeOptions']}},
                rendergridrows: function(obj) {
                    return obj.data;
                },
                columns: [
                    { text: '{{$constant['id']}}', dataField: 'id', width:'5%', align:'center', cellsalign:'center' , hidden: true},
                    { text: '{{$constant['no']}}', dataField: 'no', width: '5%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['id_doc']}}', dataField: 'id_doc', width: '6%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['id_mef']}}', dataField: 'id_mef', width: '7%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['chapter']}}', dataField: 'chapter', width: '5%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['date']}}', dataField: 'date', width: '8%', align:'center', cellsalign:'center' },
                    { text: '{{$constant['objectiv_outcome']}}', dataField: 'objectiv_outcome', align:'center', width: '40%' },
                    { text: '{{$constant['kindof_expend']}}', dataField: 'kindof_expend', align:'center', width: '8%', cellsalign:'center' },
                    { text: '{{$constant['no_mandate']}}', dataField: 'no_mandate', align:'center', width: '8%', cellsalign:'center' },
                    { text: '{{$constant['date_mandate']}}', dataField: 'date_mandate', align:'center', width: '8%', cellsalign:'center' },
                    { text: '{{$constant['money']}}', dataField: 'currency', align:'center', width: '8%', cellsalign:'right' },
                    { text: '{{$constant['outcome']}}', dataField: 'outcome', align:'center', width: '8%', cellsalign:'right' },
                    { dataField: 'username', hidden: true },
                    { text: '{{$constant['download']}}', dataField: 'avatar', width:'6%', align:'center', cellsalign:'center',cellsrenderer:active<?php echo $jqxPrefix;?>, filterable:false },
                    { text: '{{$constant['print']}}',filterable:false,sortable: false, dataField: 'print', width: '5%',editable: false,
                        cellsrenderer: function (row, datafield, value) {
                            return "{{$constant['print']}}";
                        }, columntype: 'button',
                        buttonclick: function (row) {
                                var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row);
                                //window.location='{{$printUrl}}/' + jqxdatarow.id;
                                var url ='{{$printUrl}}/'+ jqxdatarow.id;
                                window.open(url, '_blank');
                        }
                    }
                ]
        });
        $("#btn-new<?php echo $jqxPrefix;?>").on('click',function(){
            newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonNew']}}','100%','100%', '<?php echo $newUrl;?>', 0, '{{ csrf_token() }}');
        });

        $("#btn-process<?php echo $jqxPrefix;?>").on('click',function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
            $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['editRowProcess']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else if(row.length > 1){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['selectOneRowProcess']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else{
                var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row);
                console.log(jqxdatarow);
                newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['btnProcess']}}','100%','100%', '<?php echo $processUrl;?>', jqxdatarow.id, '{{ csrf_token() }}');
            }
        });

        $("#btn-copy<?php echo $jqxPrefix;?>").on('click',function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
            $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['editRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else if(row.length > 1){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['selectOneRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else{
                var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row);
                console.log(jqxdatarow);
                newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonEdit']}}', '100%','100%', '<?php echo $copyUrl;?>', jqxdatarow.id, '{{ csrf_token() }}');
            }
        });

        $("#btn-edit<?php echo $jqxPrefix;?>").on('click',function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
            $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['editRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else if(row.length > 1){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['selectOneRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else{
                var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row);
                console.log(jqxdatarow);
                newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonEdit']}}', '100%','100%', '<?php echo $editUrl;?>', jqxdatarow.id, '{{ csrf_token() }}');
            }
        });
        $("#btn-delete<?php echo $jqxPrefix;?>").click(function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['deleteRow']}}');
                $("#jqx-notification").jqxNotification("open");
                $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                return false;
            }
            var confirmMsg = confirm("{{$constant['confirmDelete']}}?");
            var listId = [];
            if(confirmMsg){
                for(var index in row){
                    var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row[index]);
                    listId.push(jqxdatarow.id);
                }
                $('#jqx-grid<?php echo $jqxPrefix;?>').jqxGrid('deleteRow', listId);
            }
        });
    });
</script>

<style type="text/css">
    .process_doc
        {
            width: 125px !important;
        }
</style>
@endsection