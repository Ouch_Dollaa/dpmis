<?php
    $jqxPrefix = '_resource';
    $newUrl = asset($constant['secretRoute'].'/resource/new');
    $listUrl = asset($constant['secretRoute'].'/resource/index');
    $deleteUrl = asset($constant['secretRoute'].'/resource/delete');
?>
@extends('layout.back-end')
@section('content')
<div id="content-container" class="content-container">
		<div class="panel">
          <div class="row panel-heading custome-panel-headering">
                <div class="form-group title-header-panel">
                    <div class="pull-left">
                        <div class="col-lg-12">{{$constant['systemSetting']}} &raquo; {{$constant['resources']}}</div>
                    </div>
                    <div class="pull-right">
                        <div class="col-lg-4 col-xs-3">
                            <button id="btn-new<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-plus"></i> {{$constant['buttonNew']}}</button>
                        </div>
                        <div class="col-lg-4 col-xs-3">
                            <button id="btn-edit<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-edit"></i> {{$constant['buttonEdit']}}</button>
                        </div>
                        <div class="col-lg-4 col-xs-3">
                            <button id="btn-delete<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-trash"></i> {{$constant['buttonDelete']}}</button>
                        </div>
                    </div>
                </div>
                
                <div id="jqx-grid<?php echo $jqxPrefix;?>"></div>
            </div>
        </div>
	</div>
    <script type="text/javascript">
        // prepare the data
        var source<?php echo $jqxPrefix;?> =
        {
            type: "post",
            dataType: "json",
            data:{"_token":'{{ csrf_token() }}'},
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'name', type: 'string' },
                { name: 'url', type: 'string' },
				{ name: 'icon', type: 'string' },
                { name: 'description', type: 'string' },
                { name: 'parent_id', type: 'string' },
                { name: 'order', type: 'number' },
                { name: 'active', type: 'number' }
            ],
            id: 'id',
            url: '<?php echo $listUrl;?>',
            hierarchy:
            {
                keyDataField: { name: 'id' },
                parentDataField: { name: 'parent_id' }
            },
            deleteRow: function (rowid, commit) {
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: "<?php echo $deleteUrl;?>",
                    cache: false,
                    data: {"id":rowid,"_token":'{{ csrf_token() }}','ajaxRequestJson':'true'},
                    success: function (response, status, xhr) {
                        $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                        if(response.code == 0){
                            $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning",autoClose: false }).html(response.message);
                            $("#jqx-notification").jqxNotification("open");
                            $("#filterjqx-grid_authentication").css('line-height','40px');
                        }else{
                            closeJqxWindowId('jqxwindow<?php echo $jqxPrefix;?>');
                            $('#jqx-notification').jqxNotification({ position: 'bottom-right' }).html(response.message);
                            $("#jqx-notification").jqxNotification("open");
                        }

                        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxTreeGrid('updateBoundData');
                    },
                    error: function (request, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }
        };
        var numberRenderer<?php echo $jqxPrefix;?> = function (row, column, value) {
            return '<div style="text-align: center; margin-top: 5px;">' + (1 + value) + '</div>';
        };
        var active<?php echo $jqxPrefix;?> = function (row, datafield, value) {
            return value == 0 ? '<div style="text-align: center; margin-top: 5px;"><i class="glyphicon glyphicon-remove"></i></div>':'<div style="text-align: center; margin-top: 5px;"><i class="glyphicon glyphicon-ok"></i></div>';
        };
		var icon<?php echo $jqxPrefix;?> = function (row, datafield, value) {
            var asset,icon;
			if(value == ''){
				asset = '<?php echo asset('/'); ?>icon/folder.png';
				icon = '';
            }else{
				asset = '<?php echo asset('/'); ?>/' + value;
				icon = '<img width="17" height="17"  src="' + asset + '" />';
			}
            return '<div style="text-align: center; margin-top:5px;">' + icon + '</div>';
        };
        $(document).ready(function () {
            //Button action
			var buttons = ['btn-new<?php echo $jqxPrefix;?>','btn-edit<?php echo $jqxPrefix;?>','btn-delete<?php echo $jqxPrefix;?>'];
            initialButton(buttons,70,30);
			
            var dataAdapter = new $.jqx.dataAdapter(source<?php echo $jqxPrefix;?>);
            // create Tree Grid
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxTreeGrid({
                    theme:jqxTheme,
                    width:'100%',
                    height:gridHeight,
                    source: dataAdapter,
                    sortable: false,
                    pageable: false,
                    pagerMode: 'advanced',
                    filterable: false,
                    filterMode: 'simple',
                    selectionMode: 'singlerow',
                    filterHeight:35,
                    pageSize: <?php echo $constant['pageSize'];?>,
                    pageSizeOptions: <?php echo $constant['pageSizeOptions'];?>,
                    columns: [
                        { text: '{{$constant['resources']}}', dataField: 'name', width: '30%' },
                        { text: '{{$constant['url']}}', dataField: 'url', width: '21%' },
                        { text: '{{$constant['description']}}', dataField: 'description', width: '30%' },
						{ text: '{{$constant['icon']}}', dataField: 'icon', width: '7%' ,cellsalign: 'center', align:'center',cellsrenderer: icon<?php echo $jqxPrefix;?>},
                        { text: '{{$constant['order']}}', dataField: 'order', width: '7%',cellsalign: 'center', align:'center'},
                        { text: '{{$constant['active']}}', dataField: 'active', width: '5%', cellsrenderer: active<?php echo $jqxPrefix;?>, align:'center' }
                    ]
            });
            $("#btn-new<?php echo $jqxPrefix;?>").on('click',function(){
                newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonNew']}}',500,500, '<?php echo $newUrl;?>', 0, '{{ csrf_token() }}');
            });
            $("#btn-edit<?php echo $jqxPrefix;?>").on('click',function(){
                var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxTreeGrid('getSelection')[0];
                if(row == null){
                    $("#jqx-notification").jqxNotification();
                    $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['editRow']}}');
                    $("#jqx-notification").jqxNotification("open");
                    return false;
                }
                newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonEdit']}}',500,500, '<?php echo $newUrl;?>', row.id, '{{ csrf_token() }}');
            });
            $("#btn-delete<?php echo $jqxPrefix;?>").click(function(){
                var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxTreeGrid('getSelection')[0];
                if(row == null){
                    $("#jqx-notification").jqxNotification();
                    $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['deleteRow']}}');
                    $("#jqx-notification").jqxNotification("open");
                    return false;
                }
                var confirmMsg = confirm("{{$constant['confirmDelete']}}?");
                if(confirmMsg){
                    $('#jqx-grid<?php echo $jqxPrefix;?>').jqxTreeGrid('deleteRow', row.id);
                }
            });
        });
    </script>
@endsection