<?php
$jqxPrefix = '_resource';
$saveUrl = asset($constant['secretRoute'].'/resource/save');
?>
<div class="container-fluid">
    <form class="form-horizontal" role="form" method="post" name="jqx-form<?php echo $jqxPrefix;?>" id="jqx-form<?php echo $jqxPrefix;?>" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="id" name="id" value="{{$id}}">
        <input type="hidden" name="ajaxRequestJson" value="true" />
        <div class="form-group">
            <div class="col-sm-4">PARENT</div>
            <div class="col-sm-8">
				<input type="hidden" name="parent_id" id="parent_id<?php echo $jqxPrefix;?>" value="{{ isset($authentication->parent_id) ? $authentication->parent_id:0 }}">
                <div id="ddlresourses<?php echo $jqxPrefix;?>">
                    <div id="ddlTreeCategory<?php echo $jqxPrefix; ?>"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4"><span class="red-star">*</span>{{$constant['resources']}}</div>
            <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="{{$constant['resources']}}" id="name" name="name" value="{{ isset($authentication->name) ? $authentication->name:''}}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4"><span class="red-star">*</span>{{$constant['url']}}</div>
            <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="{{$constant['url']}}" id="url" name="url" value="{{ isset($authentication->url) ? $authentication->url : '' }}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4"><span class="red-star">*</span>{{$constant['order']}}</div>
            <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="{{$constant['order']}}" id="order" name="order" value="{{ isset($authentication->order) ? $authentication->order : '' }}">
            </div>
        </div>
       <div class="form-group">
            <div class="col-sm-4">{{$constant['description']}}</div>
            <div class="col-sm-8">
                <textarea class="form-control" rows="3" placeholder="{{$constant['description']}}" id="description" name="description">{{ isset($authentication->description) ? $authentication->description:''}}</textarea>
            </div>
        </div>
		<div class="form-group">
            <div class="col-sm-4">{{$constant['icon']}} </div>
            <div class="col-sm-8">
                <?php $icon = isset($authentication->icon) ? $authentication->icon : asset('icon/notepad.png'); ?>
                <input type="file" value="" class="form-control" id="my-avatar" name="icon" id="icon" accept="image/*">
                <div class="wrap-avatar" id="wrap-avatar">
                    <img class="" id="img-user" src="{{$icon == "" ? asset("icon/notepad.png") : asset($icon)}}" alt="" width="16" height="16">
					<span class="red-star">(16 x 16)px</span>
                </div>
            </div>
        </div>
		
		<div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
				<input type="hidden" id="active" name="active" value="{{isset($authentication->active) ? $authentication->active:1}}">
               <div id="active-checkbox"> {{$constant['active']}}</div>
            </div>
        </div>
		
        <div class="form-group">
            <div class="col-sm-offset-9 col-sm-3">
				<button id="jqx-save<?php echo $jqxPrefix;?>" class="button-color" type="button"><span class="glyphicon glyphicon-check"></span> {{$constant['buttonSave']}}</button>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
		var buttons = ['jqx-save<?php echo $jqxPrefix;?>'];
        initialButton(buttons,90,30);
		
		//Dropdown parent
        jqxTreeDropDownList('ជ្រើសរើស','bootstrap',295,350, '#ddlresourses<?php echo $jqxPrefix; ?>','#ddlTreeCategory<?php echo $jqxPrefix; ?>', <?php echo json_encode($listAuthentication);?>, '#parent_id<?php echo $jqxPrefix;?>', true, true);
        //validate form
        $('#jqx-form<?php echo $jqxPrefix;?>').jqxValidator({
            hintType: 'label',
            rules: [
                {
                input: '#name',
                message: ' ',
                action: 'blur',
                rule: 'required'

            },
            {
                input: '#url',
                message: ' ',
                action: 'blur',
                rule: 'required'

            },
            {
                input: '#order',
                message: ' ',
                action: 'blur',
                rule: 'required'

            }]
        });
		//Save action
        $("#jqx-save<?php echo $jqxPrefix;?>").click(function(){
            saveJqxItem('{{$jqxPrefix}}', '{{$saveUrl}}', '{{ csrf_token() }}');
        });
		
		
		var isActive = $('#active').val() == 1 ? true:false;
		$("#active-checkbox").jqxCheckBox({ theme:jqxTheme ,width: 120, height: 25, checked: isActive});
		$('#active-checkbox').on('change', function (event) {
			event.args.checked == true ? $('#active').val(1):$('#active').val(0);
		});
		
		$("#my-avatar").jqxFileUpload();
        $("#my-avatar").change(function () {
            var input = this;
            var reader = new FileReader();
            var img = new Image();
            reader.onload = function (e) {
                img.src = e.target.result;
                $('#img-user').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        });
    });
</script>