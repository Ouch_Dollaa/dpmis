<?php
$jqxPrefix = '_user';
$saveUrl = asset($constant['secretRoute'].'/user/save');
$cheekUser = asset($constant['secretRoute'].'/user/user-taken');
$statusPassword = ($id != '0' ? "display-none" : "");
?>
<div class="container-fluid">
    <form class="form-horizontal" role="form" method="post" name="jqx-form<?php echo $jqxPrefix;?>" id="jqx-form<?php echo $jqxPrefix;?>" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="ajaxRequestJson" value="true" />
        <input type="hidden" id="id" name="id" value="{{isset($user->id) ? $user->id:0}}">
        <input type="hidden" id="active" name="active" value="{{isset($user->active) ? $user->active:1}}">
        <div class="form-group">
            <div class="col-sm-4"><span class="red-star">*</span>{{$constant['userRole']}}</div>
            <div class="col-sm-8">
                <input type="hidden" name="role" id="roleId<?php echo $jqxPrefix;?>" value="{{ isset($user->role_id) ? $user->role_id : '' }}">
                <div id="role<?php echo $jqxPrefix; ?>"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4"><span class="red-star">*</span>{{$constant['userName']}}</div>
            <div class="col-sm-7">
                <input type="text" class="form-control" placeholder="{{$constant['userName']}}" id="user_name" name="user_name" autocomplete="off" value="{{isset($user->user_name) ? $user->user_name:''}}">
            </div>
        </div>
        <div class="form-group {{$statusPassword}}">
            <div class="col-sm-4"><span class="red-star">*</span>{{$constant['password']}}</div>
            <div class="col-sm-7">
                <input type="password" class="form-control" placeholder="{{$constant['password']}}" id="password" name="password" value="{{isset($user->email) ? $user->email:''}}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">{{$constant['avatar']}}</div>
            <div class="col-sm-7">
                <?php $avatar = isset($user->avatar) ? $user->avatar : asset('images/default.png'); ?>
                <input type="file" value="" class="form-control" id="my-avatar" name="avatar" accept="image/*">
                <div class="wrap-avatar" id="wrap-avatar">
                    <input type="hidden" name="statusRemovePicture" value="0" id="statusRemovePicture" />
                    <img class="img-user" id="img-user" src="{{$avatar == "" ? asset("images/default.png") : asset($avatar)}}" alt="">
                    <?php $statusRemoveAvatar = isset($user->avatar) ? $user->avatar : ""; ?>
                    <span class="remove-avatar {{$statusRemoveAvatar == '' ? "display-none" : ''}}"><i class="glyphicon glyphicon-remove"></i></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
               <div id="active-checkbox"> {{$constant['active']}}</div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-8 col-sm-4">
                <button id="jqx-save<?php echo $jqxPrefix;?>" class="button-color" type="button"><span class="glyphicon glyphicon-check"></span> {{$constant['buttonSave']}}</button>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        var buttons = ['jqx-save<?php echo $jqxPrefix;?>'];
        initialButton(buttons,90,30);
        
        var isActive = $('#active').val() == 1 ? true:false;
        $("#active-checkbox").jqxCheckBox({theme:jqxTheme, width: 120, height: 25, checked: isActive});
        $('#active-checkbox').on('change', function (event) {
            event.args.checked == true ? $('#active').val(1):$('#active').val(0);
        });

        $('#jqx-form<?php echo $jqxPrefix;?>').jqxValidator({
            hintType: 'label',
            rules: [
            {
                input: '#user_name',
                message: ' ',
                action: 'blur',
                rule: 'required'

            },
                {
                    input: '#password',
                    message: ' ',
                    action: 'blur',
                    rule: 'required'

                }
            ]
        });
        
        $("#jqx-save<?php echo $jqxPrefix;?>").click(function(){
            saveJqxItem('{{$jqxPrefix}}', '{{$saveUrl}}', '{{ csrf_token() }}');
        });
        
        $("#my-avatar").jqxFileUpload();
        $("#my-avatar").change(function () {
            var input = this;
            var reader = new FileReader();
            var img = new Image();
            reader.onload = function (e) {
                img.src = e.target.result;
                $('#img-user').attr('src', e.target.result);
                $('#statusRemovePicture').val(0);
                $('.remove-avatar').removeClass('display-none');
            };
            reader.readAsDataURL(input.files[0]);
        });

        $('.remove-avatar').click(function(){
            var defautImage = '<?php echo asset("images/default.png"); ?>';
            $('#img-user').attr('src', defautImage);
            $('#statusRemovePicture').val(1);
            $('#my-avatar').val("");
            $('.remove-avatar').addClass('display-none');
        });
        
        //Role Combobox dropdown
        initDropDownList(jqxTheme,273,30, '#role<?php echo $jqxPrefix; ?>', <?php echo json_encode($listRole);?>, 'text', 'value', false, '', '0', "#roleId<?php echo $jqxPrefix;?>","Looking for");

        $("#user_name").keyup(function() {
                var user_name = $('#user_name').val();
				setTimeout(function(){
				  $.ajax({
						type: "POST",
						url: '{{$cheekUser}}',
						data: {"user_name":user_name,"_token":'{{ csrf_token() }}'},
						success: function(response){
							 var response = eval('('+response+')');
							if(response.success==true && user_name != $('#user_name1').val()){
								 $('#disp').html('<img src="{{asset('/jqwidgets/styles/images/close_black.png')}}">');
								 $("#jqx-save<?php echo $jqxPrefix;?>").jqxButton({ disabled: true });
							} else{
								 $('#disp').html(' <img src="{{asset('/jqwidgets/styles/images/check_lightblue.png')}}">');
								  $("#jqx-save<?php echo $jqxPrefix;?>").jqxButton({ disabled: false });
							}
						}
					});
				},2000);
            });
    });
</script>