<?php
$jqxPrefix = '_user';
$newUrl = asset($constant['secretRoute'].'/user/new');
$listUrl = asset($constant['secretRoute'].'/user/index');
$editUrl = asset($constant['secretRoute'].'/user/edit');
$deleteUrl = asset($constant['secretRoute'].'/user/delete');
?>
@extends('layout.back-end')
@section('content')
    <div id="content-container" class="content-container">
		<div class="panel">
            <div class="row panel-heading custome-panel-headering">
                <div class="form-group title-header-panel">
                    <div class="pull-left">
                        <div class="col-lg-5 col-xs-12">{{$constant['userAuthorize']}} &raquo; {{$constant['user']}}</div>
                        <div class="col-lg-6 col-xs-7"><input type="text" id="search-username" class="form-control" placeholder="{{$constant['userName']}}"></div> 
                        <div class="col-xs-1">
                            <button id="btn-search{{$jqxPrefix}}" class="button-color"><i class="glyphicon glyphicon-search"></i> {{$constant['buttonSearch']}}</button>
                        </div>  
                    </div>
                    <div class="pull-right">
                        <div class="col-lg-12">
                            <button id="btn-new<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-plus"></i> {{$constant['buttonNew']}}</button>
                            <button id="btn-edit<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-edit"></i> {{$constant['buttonEdit']}}</button>
                            <button id="btn-delete<?php echo $jqxPrefix;?>" class="button-color"><i class="glyphicon glyphicon-trash"></i> {{$constant['buttonDelete']}}</button>
                        </div>
                    </div>
                </div>
                <div id="jqx-grid<?php echo $jqxPrefix;?>"></div>
            </div>
        </div>
	</div>
	
<script type="text/javascript">
    function searchSuggestion(){
        var data = JSON.parse('<?php echo $inputUrl;?>');
        $("#search-username").jqxInput({
            height: 30,
            theme: 'metro',
            source: function (query, response) {
                response(data);
            }
        });
   }     
   function initialSearch() {
        var usernameValue = $("#search-username").val();
        if (usernameValue.length > 0) {
            var nameFilterGroup = new $.jqx.filter();
            nameFilterGroup.operator = 'or';
            var nameFilter = nameFilterGroup.createfilter('stringfilter', usernameValue, 'STARTS_WITH');
            nameFilterGroup.addfilter(1, nameFilter);
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('addfilter', 'user_name', nameFilterGroup);

            //apply filter
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('applyfilters');
        }else {
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('clearfilters');
        }
    }

    // prepare the data
    var source<?php echo $jqxPrefix;?> = {
        type: "post",
        dataType: "json",
        data:{"_token":'{{ csrf_token() }}'},
        dataFields: [
            { name: 'id', type: 'number' },
			{ name: 'user_name', type: 'string' },
            { name: 'avatar', type: 'string' },
            { name: 'role_id', type: 'number' },
			{ name: 'order_number', type: 'number' },
            { name: 'active', type: 'number' }
        ],
        id: 'id',
        url: '<?php echo $listUrl;?>',
        beforeprocessing: function(data) {
            source<?php echo $jqxPrefix;?>.totalrecords = (data != null)? data.total:0;
        },
        sort: function(data) {
        // Short Data
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'sort');
        },
        filter: function() {
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'filter');
        },
        deleteRow: function (rowid, commit) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: "<?php echo $deleteUrl;?>",
                cache: false,
                data: {"id":rowid,"_token":'{{ csrf_token() }}','ajaxRequestJson':'true'},
                success: function (response, status, xhr) {
                    $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                    if(response.code == 0) {
                        //Some items delete execpt the one in used
                        $('#jqx-notification').jqxNotification({position: 'bottom-right',template: "warning",autoClose: false}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else if(response.code == 1){
                        //Item in used
                        $('#jqx-notification').jqxNotification({position: 'bottom-right',template: "warning"}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else{
                        //Items delete success
                        closeJqxWindowId('jqxwindow<?php echo $jqxPrefix;?>');
                        $('#jqx-notification').jqxNotification({ position: 'bottom-right', template: "success" }).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }

                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata');
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('clearselection');
                },
                error: function (request, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    };
    var numberRenderer<?php echo $jqxPrefix;?> = function (row, column, value) {
        return '<div style="text-align: center; margin-top: 25px;">' + (1 + value) + '</div>';
    };
    var active<?php echo $jqxPrefix;?> = function (row, datafield, value) {
        return value == 0 ? '<div style="text-align: center; margin-top: 25px;"><i class="glyphicon glyphicon-remove"></i></div>':'<div style="text-align: center; margin-top: 25px;"><i class="glyphicon glyphicon-ok"></i></div>';
    };
	var avatar<?php echo $jqxPrefix;?> = function (row, datafield, value) {
        var asset;
		if(value == ''){
			asset = '<?php echo asset('/'); ?>images/default.png';
        }else{
			asset = '<?php echo asset('/'); ?>/' + value;
		}
        return '<div style="text-align: center; margin-top:7px;"><img width="55" height="55"  src="' + asset + '" /></div>';
    };
    $(document).ready(function () {
		//Button action
		var buttons = ['btn-new<?php echo $jqxPrefix;?>','btn-edit<?php echo $jqxPrefix;?>','btn-delete<?php echo $jqxPrefix;?>','btn-search{{$jqxPrefix}}'];
        initialButton(buttons,80,30);

        //Search suggestion
        searchSuggestion();

        //Search button & enter key
        $('#btn-search{{$jqxPrefix}}').on('click',function(){
            initialSearch();
        });
        $('#search-username').on('keydown', function (event) {
            if (event.keyCode == 13) {
                $('#btn-search{{$jqxPrefix}}').trigger('click');
            }
        });
		
        var dataAdapter = new $.jqx.dataAdapter(source<?php echo $jqxPrefix;?>);
        // create Tree Grid
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid({
                theme:jqxTheme,
                width:'100%',
                height:gridHeight,
				rowsheight:70,
                source: dataAdapter,
                selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                pagerMode: 'advanced',
                pageSize: <?php echo $constant['pageSize'];?>,
                pageSizeOptions: <?php echo $constant['pageSizeOptions'];?>,
                virtualmode: true,
                rendergridrows: function(obj) {
                    return obj.data;
                },
                columns: [
                    { text: '{{$constant['autoNumber']}}', columntype: 'number', width:'8%', cellsrenderer: numberRenderer<?php echo $jqxPrefix;?>, align:'center' },
					{ text: '{{$constant['userName']}}', dataField: 'user_name', width: '40%' },
					{ text: '{{$constant['userRole']}}', dataField: 'role_id', width: '30%' },
					{ text: '{{$constant['avatar']}}', dataField: 'avatar', width: '10%',align:'center',cellsrenderer: avatar<?php echo $jqxPrefix;?>},
                    { text: '{{$constant['active']}}', dataField: 'active', width: '10%',cellsrenderer: active<?php echo $jqxPrefix;?>, align:'center' }
                ]
        });

       
        $("#btn-new<?php echo $jqxPrefix;?>").on('click',function(){
            newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonNew']}}',550,550, '<?php echo $newUrl;?>', 0, '{{ csrf_token() }}');
        });

        $("#btn-edit<?php echo $jqxPrefix;?>").on('click',function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
			$("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['editRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else if(row.length > 1){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['selectOneRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else{
                var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row);
                newJqxItem('<?php echo $jqxPrefix;?>', '{{$constant['buttonEdit']}}', 550,550, '<?php echo $newUrl;?>', jqxdatarow.id, '{{ csrf_token() }}');
            }

        });
        $("#btn-delete<?php echo $jqxPrefix;?>").click(function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'bottom-right',template: "warning" }).html('{{$constant['deleteRow']}}');
                $("#jqx-notification").jqxNotification("open");
				$("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                return false;
            }
            var confirmMsg = confirm("{{$constant['confirmDelete']}}?");
            var listId = [];
            if(confirmMsg){
                for(var index in row){
                    var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row[index]);
                    listId.push(jqxdatarow.id);
                }
                $('#jqx-grid<?php echo $jqxPrefix;?>').jqxGrid('deleteRow', listId);
            }
        });

    });
</script>
@endsection