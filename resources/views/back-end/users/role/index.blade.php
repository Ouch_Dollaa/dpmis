<?php
$jqxPrefix = '_role';
$newUrl = asset($constant['secretRoute'].'/role/new');
$editUrl = asset($constant['secretRoute'].'/role/edit');
$listUrl = asset($constant['secretRoute'].'/role/index');
$deleteUrl = asset($constant['secretRoute'].'/role/delete');

?>
@extends('layout.back-end')
@section('content')
    <div id="content-container" class="content-container">
		<div class="panel">
            <div class="row panel-heading custome-panel-headering">
                <div class="form-group title-header-panel">
                    <div class="pull-left">
                        <div class="col-lg-12 col-xs-12">{{trans('users.userAuthorize')}} &raquo; {{trans('users.userRole')}}</div>
                    </div>
                    <div class="pull-right">
                        <div class="col-lg-12 col-xs-12">
                            <button id="btn-new" class="button-color"><i class="glyphicon glyphicon-plus"></i> {{trans('trans.buttonNew')}}</button>
                            <button id="btn-edit" class="button-color"><i class="glyphicon glyphicon-edit"></i> {{trans('trans.buttonEdit')}}</button>
                            <button id="btn-delete" class="button-color"><i class="glyphicon glyphicon-trash"></i> {{trans('trans.buttonDelete')}}</button>
                        </div>
                    </div>
                </div>
                <div id="jqx-grid<?php echo $jqxPrefix;?>"></div>
            </div>
        </div>
	</div>
	
<script type="text/javascript">
    var window_width = 600;
    var widow_height = 700;
    // prepare the data
    var source<?php echo $jqxPrefix;?> = {
        type: "post",
        dataType: "json",
        data:{"_token":'{{ csrf_token() }}'},
        dataFields: [
            { name: 'id', type: 'number' },
            { name: 'role', type: 'string' },
            { name: 'description', type: 'string' },
            { name: 'active', type: 'number' }
        ],
		cache: false,
        id: 'id',
        url: '<?php echo $listUrl;?>',
		
        beforeprocessing: function(data) {
            source<?php echo $jqxPrefix;?>.totalrecords = (data != null)? data.total:0;
        },
        sort: function(data) {
			// Short Data
			$("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'sort');
        },
        filter: function() {
            $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata', 'filter');
        },
        deleteRow: function (rowid, commit) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: "<?php echo $deleteUrl;?>",
                cache: false,
                data: {"id":rowid,"_token":'{{ csrf_token() }}','ajaxRequestJson':'true'},
                success: function (response, status, xhr) {
                    $("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                    if(response.code == 0){
                        //Some items delete execpt the one in used
                        $('#jqx-notification').jqxNotification({position: 'top-right',template: "warning",autoClose: false}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else if(response.code == 1){
                        //Item in used
                        $('#jqx-notification').jqxNotification({position: 'top-right',template: "warning",autoClose: false}).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }else{
                        //Items delete success
                        closeJqxWindowId('jqxwindow<?php echo $jqxPrefix;?>');
                        $('#jqx-notification').jqxNotification({ position: 'top-right', template: "success" }).html(response.message);
                        $("#jqx-notification").jqxNotification("open");
                    }
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('updatebounddata');
                    $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('clearselection');
                },
                error: function (request, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    };

    var active<?php echo $jqxPrefix;?> = function (row, datafield, value) {
        return value == 0 ? '<div style="text-align: center; margin-top: 5px;"><i class="glyphicon glyphicon-remove"></i></div>':'<div style="text-align: center; margin-top: 5px;"><i class="glyphicon glyphicon-ok"></i></div>';
    };

 
    $(document).ready(function () {
		//Button action
		var buttons = ['btn-new','btn-edit','btn-delete'];
        initialButton(buttons,90,30);

        var dataAdapter = new $.jqx.dataAdapter(source<?php echo $jqxPrefix;?>);
        // create Tree Grid
        $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid({
                theme:jqxTheme,
                width:'100%',
                height:gridHeight,
                source: dataAdapter,
                selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
				virtualmode: true,
				pagerMode: 'advanced',
				//enabletooltips: true,
				rowsheight:rowsheight,
                filterable: true,
                showfilterrow: true,
                pageSize: <?php echo $constant['pageSize'];?>,
                pageSizeOptions: <?php echo $constant['pageSizeOptions'];?>,
                rendergridrows: function(obj) {
                    return obj.data;
                },
                columns: [
                    { text: '{{trans('users.userRole')}}', dataField: 'role', width: '30%' },
                    { text: '{{trans('trans.description')}}', dataField: 'description', width: '63%',filterable:true },
                    { text: '{{trans('trans.active')}}', dataField: 'active', width: '5%',filterable:false ,cellsrenderer: active<?php echo $jqxPrefix;?>, align:'center' }
                ]
        });

       
        $("#btn-new").on('click',function(){
            newJqxItem('<?php echo $jqxPrefix;?>', '{{trans('trans.buttonNew')}}',window_width,widow_height, '<?php echo $newUrl;?>', 0, '{{ csrf_token() }}');
        });

        $("#btn-edit").on('click',function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
			$("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'top-right',template: "warning" }).html('{{$constant['editRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else if(row.length > 1){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'top-right',template: "warning" }).html('{{$constant['selectOneRow']}}');
                $("#jqx-notification").jqxNotification("open");
                return false;
            }else{
                var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row);
                newJqxItem('<?php echo $jqxPrefix;?>', '{{trans('trans.buttonEdit')}}', window_width,widow_height, '<?php echo $editUrl;?>', jqxdatarow.id, '{{ csrf_token() }}');
            }

        });
        $("#btn-delete").click(function(){
            var row = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getselectedrowindexes');
            if(row.length == 0){
                $("#jqx-notification").jqxNotification();
                $('#jqx-notification').jqxNotification({ position: 'top-right',template: "warning" }).html('{{$constant['deleteRow']}}');
                $("#jqx-notification").jqxNotification("open");
				$("#jqx-notification").jqxNotification({animationCloseDelay:1000,autoCloseDelay:8000});
                return false;
            }
            var confirmMsg = confirm("{{$constant['confirmDelete']}}?");
            var listId = [];
            if(confirmMsg){
                for(var index in row){
                    var jqxdatarow = $("#jqx-grid<?php echo $jqxPrefix;?>").jqxGrid('getrowdata', row[index]);
                    listId.push(jqxdatarow.id);
                }
                $('#jqx-grid<?php echo $jqxPrefix;?>').jqxGrid('deleteRow', listId);
            }
        });

    });
</script>
@endsection