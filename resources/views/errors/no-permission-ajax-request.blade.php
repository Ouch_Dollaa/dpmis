<div class="col-md-12">
    <p class="error-permission">{{$noPermission}}</p>
</div>

<style>
    .error-permission{
        font-family: "KhUniR1";
        font-size: 25px;
        margin-top: 10%;
        position: relative;
        text-align: center;
        white-space: normal;
        word-break: break-all;
        color: red;
        font-weight: bold;
    }
</style>