@extends('layout.admin-login')
@section('title','Form Login')
@section('content')

 <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login" style="opacity: 0.8">
                    
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="login-form" action="{{ url('/auth/login') }}" method="post" role="form" style="display: block;">
                                    <div class="form-group">
                                        <div class="cols-sm-10 btn btn-block">
                                        <img src="{{asset('images/logo-login.png')}}" style="width: 300px;">
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if (Session::has('flash_notification.message'))
                                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            {{ Session::get('flash_notification.message') }}
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label">Username</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="user_name" id="user_name"  placeholder="Enter your Name"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="username" class="cols-sm-2 control-label">Password</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                                <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
                                            </div>
                                        </div>
                                    </div>

                                  
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                            </div>
                                        </div>
                                    </div>
                                    
                                </form><!-- end form login -->

                                




                                <!-- start form register -->

                                <div class="cols-md-10" id="register-form" style="display: none;">
                                    <div class="panel with-nav-tabs panel-primary">
                                        <div class="panel-heading">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#tab1success" data-toggle="tab"><i class="fa fa-user fa-lg"></i> Register as a student</a></li>
                                                    <li><a href="#tab2success" data-toggle="tab"><i class="fa fa-male fa-lg"></i> Register as a teacher</a></li>
                                                    
                                                        </ul>
                                                    </li>
                                                </ul>
                                        </div>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div class="tab-pane fade in active" id="tab1success">

                                                    <form action="#" method="post" role="form">

                                

                                    
                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label">Username</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Name"/>
                                            </div>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="email" class="cols-sm-2 control-label">Your Email</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="username" class="cols-sm-2 control-label">Password</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Full Name</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="fullname" id="fullname"  placeholder="Enter your Fullname"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Major</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-book fa-lg" aria-hidden="true"></i></span>
                                                <select id="country" class="form-control">
                                                    <option>Please select Major</option>
                                                    <option>IT</option>
                                                    <option>Management</option>
                                                    <option>Economy</option>
                                                    <option>Bussiness</option>
                                                    <option>Finanace</option>
                                                    <option>Bank</option>
                                                    <option>Marketing</option>
                                                </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Section</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-sun-o fa-lg" aria-hidden="true"></i></span>
                                                <select id="country" class="form-control">
                                                    <option>Please select Section</option>
                                                    <option>Morning</option>
                                                    <option>Afternoon</option>
                                                    <option>Evening</option>
                                                    <option>Holiday</option>
                                                    
                                                </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Birthday</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i></span>
                                            <input type="date" name="birthDate" id="birthDate" class="form-control">
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2 control-label">Gender</label>
                                    <div class="cols-sm-10">
                                        <div class="row">
                                            <div class="cols-sm-4" style="float: left; margin-right: 30px;">
                                                <label class="radio-inline">
                                                    <input type="radio" id="femaleRadio" value="Female" name="gender">Female
                                                </label>
                                            </div>
                                            <div class="cols-sm-4">
                                                <label class="radio-inline">
                                                    <input type="radio" id="maleRadio" value="Male"  name="gender">Male
                                                </label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Current Address</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="current_address" id="rollid"  placeholder="Enter your Current Address"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Phone No</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="phone_no" id="phone_no"  placeholder="Enter your Phone No"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">TEL</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="tel" id="tel"  placeholder="Enter your Phone"/>
                                        </div>
                                    </div>
                                </div>


                               
                                <div class="form-group">
                                    <div class="cols-sm-10 col-sm-offset-1">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox">I accept <a href="#">terms</a>
                                            </label>
                                        </div>
                                    </div>
                                </div> <!-- /.form-group -->
                                <div class="form-group">
                                    <div class="cols-sm-10">
                                        <button type="submit" class="btn btn-labeled btn-success">
                                        <span class="btn-label"><i class="fa fa-check fa-lg"></i></span>Register</button>
                                        
                                        <button type="reset" class="btn btn-danger btn-labeled">
                                        <span class="btn-label"><i class="fa fa-times fa-lg"></i></span>    
                                        Reset</button>
                                        <button class="btn btn-link pull-right" id="login-form-link"><i class="fa fa-arrow-left"></i> Login Form</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="tab-pane fade" id="tab2success">
                            <form action="#" method="post" role="form">
                                <div class="form-group">
                                    <label for="name" class="cols-sm-2 control-label">Username</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Name"/>
                                        </div>
                                    </div>
                                    </div>
                                <div class="form-group">
                                    <label for="email" class="cols-sm-2 control-label">Your Email</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="username" class="cols-sm-2 control-label">Password</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Full Name</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="fullname" id="fullname"  placeholder="Enter your Fullname"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Birthday</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i></span>
                                            <input type="date" id="birthDate" class="form-control">
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2 control-label">Gender</label>
                                    <div class="cols-sm-10">
                                        <div class="row">
                                            <div class="cols-sm-4" style="float: left; margin-right: 30px;">
                                                <label class="radio-inline">
                                                    <input type="radio" id="femaleRadio" value="Female" name="gender">Female
                                                </label>
                                            </div>
                                            <div class="cols-sm-4">
                                                <label class="radio-inline">
                                                    <input type="radio" id="maleRadio" value="Male"  name="gender">Male
                                                </label>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Current Address</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="current_address" id="rollid"  placeholder="Enter your Current Address"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">Phone No</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="phone_no" id="phone_no"  placeholder="Enter your Phone No"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm" class="cols-sm-2 control-label">TEL</label>
                                    <div class="cols-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone fa-lg" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="tel" id="tel"  placeholder="Enter your Phone"/>
                                        </div>
                                    </div>
                                </div>


                               
                                <div class="form-group">
                                    <div class="cols-sm-10 col-sm-offset-1">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox">I accept <a href="#">terms</a>
                                            </label>
                                        </div>
                                    </div>
                                </div> <!-- /.form-group -->
                                <div class="form-group">
                                    <div class="cols-sm-10">
                                        <button type="submit" class="btn btn-labeled btn-success">
                                        <span class="btn-label"><i class="fa fa-check fa-lg"></i></span>Register</button>
                                        
                                        <button type="reset" class="btn btn-danger btn-labeled">
                                        <span class="btn-label"><i class="fa fa-times fa-lg"></i></span>    
                                        Reset</button>
                                        <button class="btn btn-link pull-right" id="login-form-link"><i class="fa fa-arrow-left"></i> Login Form</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript" src="{{asset('asset/js/jquery-3.1.1.min.js')}}"></script>
<script type="text/javascript">
            
    $(function() {

    $('#login-form-link').click(function(e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

    
});

</script>



@endsection
