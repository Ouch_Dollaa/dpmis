<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    /*Button Action*/
    'buttonNew'				=> 'បង្កើត',
    'buttonEdit'			=> 'កែប្រែ',
    'buttonDelete'			=> 'លុប',
    'buttonSave'			=> 'រក្សាទុក',
    'buttonSearch'			=> 'ស្វែងរក',

    /* System Config */
    'html_title'			=>'',
    'project_name'			=>'ក្រសួងសេដ្ឋកិច្ច និងហិរញ្ញវត្ថុ',
    'sytem_copy_right'		=>'© Copyright 2017',
    'institude_name_kh'	    =>'ក្រសួងសេដ្ឋកិច្ច និងហិរញ្ញវត្ថុ',
    'institude_name_en'		=>'Ministry of Economy and Finance',
    'logout'				=>'ចេញពីប្រព័ន្ធ',
    'active'				=> 'ACTIVE',
    'closeStatus'			=> 'DISABLE',
    'detailInfo'            =>'ព័ត៌មានលំអិត',
    'all'                   =>'All',
    'description'			=> 'បរិយាយ',
    'position'              =>'តួនាទី /មុខតំណែង'

];
