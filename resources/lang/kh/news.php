<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'news_management'               =>'គ្រប់គ្រងព័ត៌មាន',
    'tagName'                       =>'Tag',
    'order'			                =>'លេខ​រៀង',
    'avatar'	                    =>'រូបថត',
    'create_by'                     =>'អ្នកបង្កើត',
];
