-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2016 at 12:42 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `public_procurment_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `central_ministry_agencies`
--

CREATE TABLE IF NOT EXISTS `central_ministry_agencies` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `central_ministry_agencies`
--

INSERT INTO `central_ministry_agencies` (`Id`, `Name`, `IsActive`, `Status`) VALUES
(24, 'អាជ្ញាធរសវនកម្ម​ជាតិ​', 1, 3),
(25, 'អាជ្ញាធរទន្លេសាប​​', 1, 3),
(26, 'ក្រុមប្រឹក្សាអភិវឌ្ឍន៍កម្ពុជា​', 1, 3),
(27, 'គណៈកម្មាធិការជាតិ​រៀបចំ​ការបោះឆ្នោត​', 1, 3),
(28, 'គណៈ​កម្មាធិការជាតិ​ទន្លេ​មេគង្គ​', 1, 3),
(29, 'អគ្គ​លេខាធិការដ្ឋានព្រឹទ្ធសភា​', 1, 3),
(30, 'អគ្គ​លេខាធិការដ្ឋានរដ្ឋសភា​', 1, 3),
(31, 'អគ្គលេខាធិការដ្ឋានក្រុមប្រឹក្សាធម្មនុញ្ញ', 1, 3),
(32, 'រដ្ឋលេខាធិការដ្ឋានអាកាសចរស៊ីវិល', 1, 3),
(33, 'អាជ្ញាធរជាតិ​ប្រឆាំងនឹងគ្រឿងញៀន​', 1, 3),
(34, 'អង្គភាពប្រឆាំងអំពើ​ពុករលួយ​', 1, 3),
(35, 'ឧត្តមក្រុមប្រឹក្សានៃ​អង្គចៅក្រម​', 1, 3),
(36, 'សាលាឧទ្ធរណ៍​', 1, 3),
(37, 'អាជ្ញាធរជាតិ​ប្រយុទ្ធនឹងជម្ងឺ​អេដស៍​', 1, 3),
(38, 'ទីស្ដីការគណៈរដ្ឋមន្រ្តី​', 1, 3),
(39, 'ក្រសួងការពារជាតិ​', 1, 3),
(40, 'ក្រសួងឧស្សាហកម្មនិងសិប្បកម្ម​', 1, 3),
(41, 'ក្រសួងរ៉ែ​និងថាមពល', 1, 3),
(42, 'ក្រសួងកិច្ចការនារី', 1, 3),
(43, 'ក្រសួងមុខងារសាធារណៈ​', 1, 3),
(44, 'ក្រសួងប្រៃសណីយ៍​និងទូរគមនាគមន៍​', 1, 3),
(45, 'ក្រសួងវប្បធម៌និងវិចិត្រ​សិល្បៈ​', 1, 3),
(46, 'ក្រសួងធម្មការនិងសាសនា​', 1, 3),
(47, 'ក្រសួងការបរទេសនិងសហប្រតិបត្តិការអន្តរជាតិ​', 1, 3),
(48, 'ក្រសួងទំនាក់ទំនងសភានិងអធិការកិច្ច​', 1, 3),
(49, 'ក្រសួងទេសចរណ៍​', 1, 3),
(50, 'ក្រសួងសុខាភិបាល​', 1, 3),
(51, 'ក្រសួងសាធារណៈការនិងដឹកជញ្ជូន​', 1, 3),
(52, 'ក្រសួងការងារនិងបណ្ដុះបណ្ដាលវិជ្ជាជីវៈ​', 1, 3),
(53, 'ក្រសួងផែនការ​', 1, 3),
(54, 'ក្រសួងកសិកម្មរុក្ខា​ប្រម៉ាញ់និងនេសាទ​', 1, 3),
(55, 'ក្រសួងធនធានទឹកនិងឧតុនិយម​', 1, 3),
(56, 'ក្រសួងព័ត៌មាន​', 1, 3),
(57, 'ក្រសួងមហាផ្ទៃ​(រដ្ឋបាល​ទូទៅ)', 1, 3),
(58, 'ក្រសួងមហាផ្ទៃ​(សន្តិសុខ)', 1, 3),
(59, 'ក្រសួង​សេដ្ឋកិច្ចនិងហិរញ្ញវត្ថុ​', 1, 3),
(60, 'ក្រសួងសង្គមកិច្ចអតីតយុទ្ធជននិងយុវនីតិ​សម្បទា​', 1, 3),
(61, 'ក្រសួងយុត្តិធម៌', 1, 3),
(62, 'ក្រសួងអប់រំ​យុវជននិងកីឡា​', 1, 3),
(63, 'ក្រសួងបរិស្ថាន​', 1, 3),
(64, 'ក្រសួងរៀបចំ​ដែនដី​នគរូបនីយកម្មនិងសំណង់​', 1, 3),
(65, 'តុលាការកំពូល​', 1, 3),
(66, 'ក្រសួងអភិវឌ្ឍន៍​ជនបទ​', 1, 3),
(67, 'ក្រសួងពាណិជ្ជកម្ម​', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `financial_entitites_to_agencies`
--

CREATE TABLE IF NOT EXISTS `financial_entitites_to_agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `central_ministry_agency_id` int(11) DEFAULT NULL,
  `local_financial_entities_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `financial_entitites_to_agencies`
--

INSERT INTO `financial_entitites_to_agencies` (`id`, `central_ministry_agency_id`, `local_financial_entities_id`) VALUES
(3, 1, 1),
(71, 52, 2),
(70, 52, 1),
(13, 51, 2),
(12, 51, 1),
(73, 53, 2),
(72, 53, 1),
(18, 54, 1),
(19, 54, 2),
(20, 55, 1),
(21, 55, 2),
(22, 56, 1),
(23, 56, 2),
(24, 57, 1),
(25, 57, 2),
(80, 58, 2),
(79, 58, 1),
(28, 59, 1),
(29, 59, 2),
(74, 61, 1),
(34, 62, 1),
(35, 62, 2),
(36, 63, 1),
(37, 63, 2),
(87, 64, 2),
(86, 64, 1),
(85, 65, 1),
(78, 66, 2),
(77, 67, 2),
(76, 67, 1);

-- --------------------------------------------------------

--
-- Table structure for table `local_financial_entities`
--

CREATE TABLE IF NOT EXISTS `local_financial_entities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `local_financial_entities`
--

INSERT INTO `local_financial_entities` (`id`, `name`, `order`, `active`) VALUES
(1, 'The central budget unit of the national procurement institution', 1, 1),
(2, 'Local financial entities of the national procurement institution', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `main_procurement`
--

CREATE TABLE IF NOT EXISTS `main_procurement` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ParentId` int(11) DEFAULT '0',
  `ProucrementCurrencyId` int(11) DEFAULT '0',
  `ProcurementTypeId` int(11) DEFAULT '0',
  `ProcurementMethodId` int(11) DEFAULT '0',
  `MEF_FundingSourceId` int(11) NOT NULL DEFAULT '0',
  `Status` int(11) DEFAULT NULL,
  `StatusId` int(11) DEFAULT '0',
  `MEFUserId` int(11) DEFAULT '0',
  `Name` varchar(255) DEFAULT NULL,
  `Price` bigint(20) DEFAULT NULL,
  `RequestDate` date DEFAULT NULL,
  `GivingDate` date DEFAULT NULL,
  `ContractDate` date DEFAULT NULL,
  `CreateDate` date DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `ClosedDate` date DEFAULT NULL,
  `ProjectDescription` text,
  `ProcurementYear` int(11) DEFAULT NULL,
  `isProcurement` tinyint(4) DEFAULT '0',
  `isCancel` tinyint(4) DEFAULT '0',
  `isUnderCopyRight` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `main_procurement`
--

INSERT INTO `main_procurement` (`Id`, `ParentId`, `ProucrementCurrencyId`, `ProcurementTypeId`, `ProcurementMethodId`, `MEF_FundingSourceId`, `Status`, `StatusId`, `MEFUserId`, `Name`, `Price`, `RequestDate`, `GivingDate`, `ContractDate`, `CreateDate`, `LastModified`, `ClosedDate`, `ProjectDescription`, `ProcurementYear`, `isProcurement`, `isCancel`, `isUnderCopyRight`) VALUES
(47, 0, 1, 3, 3, 1, 3, 32, 3, 'ជួសជុលថែទាំឧបករណ៍បច្ចេកទេសឧតុនិយមប្រចាំឆ្នាំ២០១៦', 100, '2016-07-01', '2016-07-10', '2016-08-10', '2016-08-03', '2016-08-03 13:46:38', NULL, 'ជួសជុលថែទាំឧបករណ៍បច្ចេកទេសឧតុនិយមប្រចាំឆ្នាំ២០១៦', 0, 0, 0, 0),
(46, 0, 1, 2, 3, 1, 3, 32, 3, 'ជួសជុលអគារការិយាល័យនៅអាកាសយានដ្ឋានបាត់ដំបង', 245, '2016-01-01', '2016-01-10', '2016-03-05', '2016-08-03', '2016-08-03 13:43:00', NULL, 'ជួសជុលអគារការិយាល័យនៅអាកាសយានដ្ឋានបាត់ដំបង', 0, 0, 0, 0),
(43, 0, 1, 1, 4, 1, 3, 50, 3, 'សម្ភារសំអាតសំបកកង់រថយន្ត សម្ភារបច្ចេកទេស និងសង្ហារឹម', 7570, '2016-02-05', '2016-02-20', '2016-06-26', '2016-08-03', '2016-08-03 09:36:15', NULL, 'សម្ភារសំអាតសំបកកង់រថយន្ត សម្ភារបច្ចេកទេស និងសង្ហារឹម', 0, 0, 0, 0),
(44, 0, 1, 2, 3, 1, 3, 43, 3, 'ជួសជុលពង្រីកសួនច្បារថ្មី និងជួសជុលចាក់បេតុងជុំវិញក្រសួង', 204, '2016-04-18', '2016-05-02', '2016-07-18', '2016-08-03', '2016-08-03 09:43:29', NULL, 'ជួសជុលពង្រីកសួនច្បារថ្មី និងជួសជុលចាក់បេតុងជុំវិញក្រសួង', 0, 0, 0, 0),
(45, 0, 1, 3, 3, 1, 3, 32, 3, 'ជួសជុលថែទាំបណ្តាញទឹកចូលអាកាសយានដ្ឋាន និងបំពាក់ធុងស្តុកទឹកចែកចាយតាមអគារនៅអាកាសយានដ្ឋានកោះកុង', 128, '2016-01-01', '2016-01-10', '2016-03-04', '2016-08-03', '2016-08-03 09:49:50', NULL, 'ជួសជុលថែទាំបណ្តាញទឹកចូលអាកាសយានដ្ឋាន និងបំពាក់ធុងស្តុកទឹកចែកចាយតាមអគារនៅអាកាសយានដ្ឋានកោះកុង', 0, 0, 0, 0),
(58, 0, 1, 1, 3, 1, 3, 25, 3, 'សម្ភារបច្ចេកទេសឆ្នាំ២០១៦', 200, '2016-08-01', '2016-08-10', '2016-10-19', '2016-08-03', '2016-08-03 14:56:43', NULL, '', 0, 0, 0, 0),
(59, 0, 1, 1, 3, 1, 3, 62, 3, 'ប្រេងឥន្ទនៈ និងប្រេងរំអិល', 1001, '2015-12-05', '2016-01-04', '2016-03-24', '2016-08-03', '2016-08-03 15:04:08', NULL, '', 0, 0, 0, 1),
(60, 0, 1, 1, 3, 1, 3, 62, 3, 'សម្ភារការិយាល័យ ការបោះពុម្ព និងសម្ភារបរិក្ខារបច្ចេកទេស', 2856, '2015-12-05', '2016-01-04', '2016-03-24', '2016-08-03', '2016-08-03 15:07:49', NULL, '', 0, 0, 0, 0),
(57, 0, 1, 1, 3, 1, 3, 25, 3, 'បោះពុម្ពទស្សនាវដ្តី ក្បាលលិខិត ស្រោមសំបុត្រ ប័ណ្ឌជូនពរ និងសៀវភៅ', 150, '2016-04-11', '2016-04-20', '2016-06-29', '2016-08-03', '2016-08-03 14:53:36', NULL, '', 0, 0, 0, 0),
(50, 0, 1, 1, 3, 1, 3, 44, 3, 'ឧបករណ៍ OTDR និងSplicing សម្រាប់ការងារតភ្ជាប់ខ្សែកាប្លិ៍អុបទិក', 480, '2016-01-15', '2016-01-29', '2016-04-06', '2016-08-03', '2016-08-03 13:59:38', NULL, '', 0, 0, 1, 0),
(51, 0, 1, 1, 3, 1, 3, 44, 3, 'រថយន្តសម្រាប់បម្រើឲ្យការងាររដ្ឋបាល', 460, '2016-03-04', '2016-03-27', '2016-06-07', '2016-08-03', '2016-08-03 14:03:26', NULL, '', 0, 0, 1, 0),
(52, 0, 1, 1, 4, 1, 3, 50, 3, 'ឱសថក្នុងស្រុក', 8000, '2016-04-05', '2016-04-20', '2016-08-22', '2016-08-03', '2016-08-03 14:11:28', NULL, '', 0, 0, 1, 0),
(53, 0, 1, 1, 4, 1, 3, 50, 3, 'ទិញឱសថបន្ថែម', 18500, '2016-08-01', '2016-09-10', '2016-12-03', '2016-08-03', '2016-08-03 14:19:03', NULL, '', 0, 1, 0, 0),
(54, 0, 1, 1, 3, 1, 3, 25, 3, 'សម្ភារការិយាល័យ និងអនាម័យ ឆ្នាំ២០១៦', 200, '2016-01-04', '2016-01-13', '2016-03-28', '2016-08-03', '2016-08-03 14:40:05', NULL, 'សម្ភារការិយាល័យ និងអនាម័យ ឆ្នាំ២០១៦', 0, 0, 0, 0),
(55, 0, 1, 1, 3, 1, 3, 25, 3, 'ទិញរថយន្តដឹកទំនិញធុនតូច(Pick Up)ចំនួន០៣គ្រឿង', 600, '2016-01-04', '2016-01-13', '2016-04-07', '2016-08-03', '2016-08-03 14:44:12', NULL, '', 0, 0, 1, 0),
(56, 0, 1, 1, 5, 1, 3, 25, 3, 'ឯកសណ្ឋាន និងសញ្ញស័ក្កមន្រ្តីរាជការ', 50, '2016-01-04', '2016-01-13', '2016-03-09', '2016-08-03', '2016-08-03 14:50:07', NULL, '', 0, 0, 0, 0),
(49, 0, 1, 1, 3, 1, 3, 44, 3, 'បោះពុម្ពកាលិបត្រ និងព្រឹត្តិបត្រក្នុងឆមាសទី១', 150, '2016-01-08', '2016-01-21', '2016-03-31', '2016-08-03', '2016-08-03 13:55:55', NULL, '', 0, 0, 0, 0),
(48, 0, 1, 1, 3, 1, 3, 32, 3, 'ប្រេងឥន្ទនៈឆមាសទី១', 425, '2015-12-15', '2015-12-25', '2016-03-01', '2016-08-03', '2016-08-03 13:50:01', NULL, '', 0, 0, 0, 0),
(42, 0, 1, 1, 3, 1, 3, 50, 3, 'ផ្គត់ផ្គង់ប្រេងឥន្ទនៈឆមាសទី១', 1080, '2016-01-10', '2016-01-25', '2016-04-13', '2016-08-03', '2016-08-03 09:27:53', NULL, 'ផ្គត់ផ្គង់ប្រេងឥន្ទនៈឆមាសទី១', 0, 0, 0, 0),
(61, 55, 1, 1, 3, 1, 3, 25, 3, 'ទិញរថយន្តដឹកទំនិញធុនតូច(Pick Up)ចំនួន០៣គ្រឿង', 680, '2016-01-04', '2016-01-13', '2016-04-07', '2016-08-03', '2016-08-03 15:18:52', NULL, 'ទិញរថយន្តដឹកទំនិញធុនតូច(Pick Up)ចំនួន០៣គ្រឿង', 0, 1, 0, 0),
(62, 51, 1, 1, 3, 1, 3, 44, 3, 'រថយន្តសម្រាប់បម្រើឲ្យការងាររដ្ឋបាល', 500, '2016-03-04', '2016-03-27', '2016-06-07', '2016-08-03', '2016-08-03 15:20:36', NULL, '', 0, 1, 0, 0),
(63, 52, 1, 1, 4, 1, 3, 50, 3, 'ឱសថក្នុងស្រុក', 8080, '2016-04-05', '2016-04-20', '2016-08-22', '2016-08-03', '2016-08-03 15:28:11', NULL, '', 0, 1, 0, 0),
(64, 50, 1, 1, 8, 1, 3, 44, 3, 'ឧបករណ៍ OTDR និងSplicing សម្រាប់ការងារតភ្ជាប់ខ្សែកាប្លិ៍អុបទិក', 480, '2016-01-15', '2016-01-29', '2016-04-06', '2016-08-04', '2016-08-04 14:32:23', NULL, '', 0, 1, 1, 0),
(65, 64, 1, 2, 8, 1, 3, 44, 3, 'ឧបករណ៍ OTDR និងSplicing សម្រាប់ការងារតភ្ជាប់ខ្សែកាប្លិ៍អុបទិក', 480, '2016-01-15', '2016-01-29', '2016-04-06', '2016-08-04', '2016-08-04 14:36:04', NULL, '', 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mef_funding_source`
--

CREATE TABLE IF NOT EXISTS `mef_funding_source` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `OrderNumber` int(11) DEFAULT '0',
  `IsActive` tinyint(11) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mef_funding_source`
--

INSERT INTO `mef_funding_source` (`Id`, `Name`, `OrderNumber`, `IsActive`) VALUES
(1, 'ថវិការដ្ឋ', 1, 1),
(2, 'ចំណាយថវិការដ្ឋរបស់ក្រសួងប្រែសណ័យ៏ និងទូរគមន៏ដែលអនុម័តក្នុងឆ្នាំ២០១៦', 2, 1),
(3, 'ភ្នាក់ងារកម្ពុជានាវារចរ កាំសាប', 3, 1),
(4, 'ថវិការដ្ឋឆ្នាំ២០១៦ (ជំពូក២១)', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mef_is_group_procurement`
--

CREATE TABLE IF NOT EXISTS `mef_is_group_procurement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `MainProcurementId` bigint(20) DEFAULT NULL,
  `IsProcurementGroupId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `mef_is_group_procurement`
--

INSERT INTO `mef_is_group_procurement` (`id`, `MainProcurementId`, `IsProcurementGroupId`) VALUES
(1, 42, 42),
(2, 43, 43),
(3, 44, 44),
(4, 45, 45),
(5, 46, 46),
(6, 47, 47),
(7, 48, 48),
(8, 49, 49),
(9, 50, 50),
(10, 51, 51),
(11, 52, 52),
(12, 53, 53),
(24, 64, 50),
(14, 54, 54),
(15, 55, 55),
(16, 56, 56),
(17, 57, 57),
(18, 58, 58),
(19, 59, 59),
(20, 60, 60),
(21, 61, 55),
(22, 62, 51),
(23, 63, 52),
(25, 65, 50);

-- --------------------------------------------------------

--
-- Table structure for table `mef_is_under_copy_right`
--

CREATE TABLE IF NOT EXISTS `mef_is_under_copy_right` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mef_is_under_copy_right`
--

INSERT INTO `mef_is_under_copy_right` (`id`, `Name`) VALUES
(1, 'លើសិទ្ធិ'),
(2, 'ក្រោមសិទ្ធិ');

-- --------------------------------------------------------

--
-- Table structure for table `mef_main_procurement_format_id`
--

CREATE TABLE IF NOT EXISTS `mef_main_procurement_format_id` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainProcurementId` int(11) DEFAULT NULL,
  `PPMIS_FORMAT_ID` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `mef_main_procurement_format_id`
--

INSERT INTO `mef_main_procurement_format_id` (`Id`, `MainProcurementId`, `PPMIS_FORMAT_ID`) VALUES
(42, 42, 'PPMIS00042'),
(43, 43, 'PPMIS00043'),
(44, 44, 'PPMIS00044'),
(45, 45, 'PPMIS00045'),
(46, 46, 'PPMIS00046'),
(47, 47, 'PPMIS00047'),
(48, 48, 'PPMIS00048'),
(49, 49, 'PPMIS00049'),
(50, 50, 'PPMIS00050'),
(51, 51, 'PPMIS00051'),
(52, 52, 'PPMIS00052'),
(53, 53, 'PPMIS00053'),
(54, 54, 'PPMIS00054'),
(55, 55, 'PPMIS00055'),
(56, 56, 'PPMIS00056'),
(57, 57, 'PPMIS00057'),
(58, 58, 'PPMIS00058'),
(59, 59, 'PPMIS00059'),
(60, 60, 'PPMIS00060'),
(61, 61, 'PPMIS00061'),
(62, 62, 'PPMIS00062'),
(63, 63, 'PPMIS00063'),
(64, 64, 'PPMIS00064'),
(65, 65, 'PPMIS00065');

-- --------------------------------------------------------

--
-- Table structure for table `mef_procurement_attachment`
--

CREATE TABLE IF NOT EXISTS `mef_procurement_attachment` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `MainProcurementId` int(11) NOT NULL,
  `SourceAttachment` varchar(200) NOT NULL,
  `EncryptAttachment` varchar(200) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `moef_autenticate_to_role`
--

CREATE TABLE IF NOT EXISTS `moef_autenticate_to_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `moef_role_id` bigint(20) NOT NULL,
  `meof_authenticate_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4102 ;

--
-- Dumping data for table `moef_autenticate_to_role`
--

INSERT INTO `moef_autenticate_to_role` (`id`, `moef_role_id`, `meof_authenticate_id`) VALUES
(4016, 33, 93),
(4101, 1, 79),
(4100, 1, 96),
(4099, 1, 71),
(4098, 1, 70),
(4097, 1, 69),
(4096, 1, 48),
(4095, 1, 115),
(4094, 1, 114),
(4093, 1, 113),
(4092, 1, 112),
(4014, 34, 93),
(4013, 34, 1),
(4091, 1, 111),
(4090, 1, 110),
(4089, 1, 81),
(4088, 1, 61),
(4087, 1, 95),
(4086, 1, 60),
(4085, 1, 58),
(4084, 1, 42),
(4083, 1, 68),
(4082, 1, 78),
(4081, 1, 97),
(4080, 1, 67),
(4079, 1, 65),
(4078, 1, 43),
(4077, 1, 57),
(4076, 1, 80),
(4075, 1, 98),
(4074, 1, 55),
(4073, 1, 54),
(4072, 1, 25),
(4071, 1, 82),
(4070, 1, 99),
(4069, 1, 64),
(4068, 1, 63),
(4067, 1, 62),
(4066, 1, 27),
(4065, 1, 83),
(4064, 1, 100),
(4063, 1, 53),
(4062, 1, 52),
(4061, 1, 51),
(4060, 1, 26),
(4059, 1, 7),
(4058, 1, 130),
(4057, 1, 129),
(4056, 1, 74),
(4055, 1, 84),
(4054, 1, 103),
(4053, 1, 73),
(4052, 1, 72),
(4051, 1, 5),
(4050, 1, 91),
(4049, 1, 92),
(4048, 1, 102),
(4047, 1, 87),
(4046, 1, 86),
(4045, 1, 4),
(4044, 1, 3),
(4043, 1, 122),
(4042, 1, 119),
(4041, 1, 120),
(4040, 1, 117),
(4039, 1, 126),
(4038, 1, 128),
(4037, 1, 125),
(4036, 1, 116),
(4035, 1, 121),
(4034, 1, 118),
(4033, 1, 104),
(4032, 1, 46),
(4031, 1, 124),
(4030, 1, 127),
(4029, 1, 123),
(4028, 1, 45),
(4027, 1, 44),
(4026, 1, 109),
(4025, 1, 77),
(4024, 1, 85),
(4023, 1, 101),
(4022, 1, 76),
(4021, 1, 75),
(4020, 1, 28),
(4019, 1, 17),
(4018, 1, 93),
(4017, 1, 1),
(4015, 33, 1);

-- --------------------------------------------------------

--
-- Table structure for table `moef_authenticate`
--

CREATE TABLE IF NOT EXISTS `moef_authenticate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `url` varchar(300) DEFAULT NULL,
  `description` text,
  `order` int(11) DEFAULT NULL,
  `icon` varchar(300) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=131 ;

--
-- Dumping data for table `moef_authenticate`
--

INSERT INTO `moef_authenticate` (`id`, `name`, `parent_id`, `url`, `description`, `order`, `icon`, `active`) VALUES
(1, 'Dashboard', 0, 'dashboard', 'Dashboard Module', 1, 'files/icons/1464757261_26234.png', 1),
(3, 'គ្រប់គ្រងអ្នកប្រើប្រព័ន្ធ', 0, 'user-authentication', 'User authentication', 3, 'files/icons/1470100175_79266.png', 1),
(4, ' អ្នកប្រើប្រព័ន្ធ', 3, 'user', 'User setup', 1, 'files/icons/1470100215_56006.png', 1),
(5, 'តួនាទីអ្នកប្រើ', 3, 'role', 'User role', 2, 'files/icons/1470100227_80198.png', 1),
(7, 'ការកំណត់ប្រព័ន្ធ', 0, 'system-setting', 'System setting', 5, 'files/icons/1470100181_93390.png', 1),
(45, 'តារាងផែនការដើមឆ្នាំ', 44, 'page-1', 'តារាងផែនការដើមឆ្នាំ', 1, 'files/icons/1470100334_74751.png', 1),
(17, 'គ្រប់គ្រងផែនការលទ្ធកម្ម', 0, 'procurment', '', 2, 'files/icons/1470100169_65042.png', 1),
(25, 'ក្រសួង​-ស្ថាប័ន​ថ្នាក់​កណ្ដាល​', 7, 'central-ministry', 'Central Ministry Setting up', 3, 'files/icons/1470100283_72055.png', 1),
(26, 'ប្រភេទលទ្ធកម្ម', 7, 'procurement-type', 'ប្រភេទលទ្ធកម្ម', 1, 'files/icons/1464191986_14204.png', 1),
(27, 'វិធីសាស្រ្តលទ្ធកម្ម', 7, 'procurement-method', 'វិធីសាស្រ្តលទ្ធកម្ម', 2, 'files/icons/1464191974_35014.png', 1),
(28, 'ផែនការលទ្ធកម្ម', 17, 'procurement-plan', '', 1, '', 1),
(44, 'របាយការណ៍', 17, 'procurement-reports', '', 2, 'files/icons/1470100312_81568.png', 1),
(46, 'ទំនិញ សំណង់ សេវាកម្ម', 124, 'report-start-year-goods-construct-service', 'ទំនិញ សំណង់ សេវាកម្ម', 4, '', 1),
(87, 'បង្កើត', 4, 'user/new', 'បង្កើត', 2, '', 0),
(43, 'គ្រឹះស្ថានសាធារណៈ', 7, 'public-institutions-enterprice', 'គ្រឹះស្ថានសាធារណៈ', 4, '', 1),
(42, 'រាជធានី​-ខេត្ត​', 7, 'province', 'រាជធានី​-ខេត្ត​', 5, '', 1),
(48, 'RESOURCE', 7, 'resource', '', 7, 'files/icons/1465365287_43501.png', 1),
(65, 'មើល', 43, 'public-institutions-enterprice/index', 'មើលគ្រឹះស្ថានសាធារណៈ', 1, '', 0),
(54, 'មើល', 25, 'central-ministry/index', 'មើលក្រសួង​ ស្ថាប័ន​ថ្នាក់​កណ្ដាល​', 1, '', 0),
(86, 'មើល', 4, 'user/index', '', 1, '', 0),
(78, 'រក្សាទុក', 43, 'public-institutions-enterprice/save', 'រក្សាទុកមើលគ្រឹះស្ថានសាធារណៈ', 4, '', 0),
(51, 'មើល', 26, 'procurement-type/index', 'មើលប្រភេទលទ្ធកម្ម', 1, '', 0),
(52, 'បង្កើត', 26, 'procurement-type/new', 'បង្កើតប្រភេទលទ្ធកម្ម', 2, '', 0),
(104, 'មើល', 46, 'report-start-year-goods-construct-service/index', 'មើល', 1, '', 0),
(53, 'លុប', 26, 'procurement-type/delete', 'លុបប្រភេទលទ្ធកម្ម', 3, '', 0),
(92, 'រក្សាទុក', 4, 'user/save', '', 4, '', 0),
(55, 'បង្កើត', 25, 'central-ministry/new', 'បង្កើតក្រសួង​ ស្ថាប័ន​ថ្នាក់​កណ្ដាល​', 2, '', 0),
(93, 'មើល', 1, 'dashboard/index', 'Dashboard', 1, '', 0),
(57, 'លុប', 25, 'central-ministry/delete', 'លុបក្រសួង​ ស្ថាប័ន​ថ្នាក់​កណ្ដាល​', 5, '', 0),
(91, 'លុប', 4, 'user/delete', '', 5, '', 0),
(58, 'មើល', 42, 'province/index', 'មើលរាជធានី​-ខេត្ត​', 1, '', 0),
(61, 'លុប', 42, 'province/delete', 'លុបរាជធានី​-ខេត្ត​', 4, '', 0),
(60, 'បង្កើត', 42, 'province/new', 'បង្កើតរាជធានី​-ខេត្ត​', 2, '', 0),
(81, 'រក្សាទុក', 42, 'province/save', '', 5, '', 0),
(62, 'មើល', 27, 'procurement-method/index', 'មើលវិធីសាស្រ្តលទ្ធកម្ម', 1, '', 0),
(63, 'បង្កើត', 27, 'procurement-method/new', 'បង្កើតវិធីសាស្រ្តលទ្ធកម្ម', 2, '', 0),
(64, 'លុប', 27, 'procurement-method/delete', 'លុបវិធីសាស្រ្តលទ្ធកម្ម', 3, '', 0),
(83, 'រក្សាទុក', 26, 'procurement-type/save', '', 4, '', 0),
(67, 'បង្កើត', 43, 'public-institutions-enterprice/new', 'បង្កើតគ្រឹះស្ថានសាធារណៈ', 2, '', 0),
(68, 'លុប', 43, 'public-institutions-enterprice/delete', 'លុបគ្រឹះស្ថានសាធារណៈ', 5, '', 0),
(82, 'រក្សាទុក', 27, 'procurement-method/save', '', 4, '', 0),
(69, 'មើល', 48, 'resource/index', 'មើលResource', 1, '', 0),
(70, 'បង្កើត', 48, 'resource/new', 'បង្កើតResource', 2, '', 0),
(71, 'លុប', 48, 'resource/delete', 'លុបResource', 3, '', 0),
(72, 'មើល', 5, 'role/index', 'មើលតូនាទីអ្នកប្រើ', 1, '', 0),
(73, 'បង្កើត', 5, 'role/new', 'បង្កើតតូនាទីអ្នកប្រើ', 2, '', 0),
(74, 'លុប', 5, 'role/delete', 'លុបតូនាទីអ្នកប្រើ', 5, '', 0),
(84, 'រក្សាទុក', 5, 'role/save', '', 4, '', 0),
(75, 'មើល', 28, 'procurement-plan/index', 'មើលផែនការលទ្ធកម្ម', 1, '', 0),
(76, 'បង្កើត', 28, 'procurement-plan/new', 'បង្កើតផែនការលទ្ធកម្ម', 2, '', 0),
(109, 'អានលំអិត', 28, 'procurement-plan/detail', 'មើលផែនការលទ្ធកម្មលំអិត', 6, '', 0),
(77, 'លុប', 28, 'procurement-plan/delete', 'លុបបង្កើតផែនការលទ្ធកម្ម', 5, '', 0),
(85, 'រក្សាទុក', 28, 'procurement-plan/save', '', 4, '', 0),
(79, 'រក្សាទុក', 48, 'resource/save', 'រក្សាទុកResource', 5, '', 0),
(80, 'រក្សាទុក', 25, 'central-ministry/save', 'រក្សាទុកក្រសួង​-ស្ថាប័ន​ថ្នាក់​កណ្ដាល​', 4, '', 0),
(95, 'កែប្រែ', 42, 'province/edit', '', 3, '', 0),
(96, 'កែប្រែ', 48, 'resource/edit', 'កែប្រែ', 4, '', 0),
(97, 'កែប្រែ', 43, 'public-institutions-enterprice/edit', 'កែប្រែ', 3, '', 0),
(98, 'កែប្រែ', 25, 'central-ministry/edit', 'កែប្រែ', 3, '', 0),
(99, 'កែប្រែ', 27, 'procurement-method/edit', 'កែប្រែ', 3, '', 0),
(100, 'កែប្រែ', 26, 'procurement-type/edit', 'កែប្រែ', 3, '', 0),
(101, 'កែប្រែ', 28, 'procurement-plan/edit', 'កែប្រែ', 3, '', 0),
(102, 'កែប្រែ', 4, 'user/edit', 'កែប្រែ', 3, '', 0),
(103, 'កែប្រែ', 5, 'role/edit', 'កែប្រែ', 3, '', 0),
(110, 'ប្រភពមូលនិធិ', 7, 'funding-source', 'ប្រភពមូលនិធិ', 6, '', 1),
(111, 'មើល', 110, 'funding-source/index', 'មើល', 1, '', 0),
(112, 'បង្កើត', 110, 'funding-source/new', 'បង្កើត', 2, '', 0),
(113, 'កែប្រែ', 110, 'funding-source/edit', 'កែប្រែ', 3, '', 0),
(114, 'លុប', 110, 'funding-source/delete', 'លុប', 4, '', 0),
(115, 'រក្សាទុក', 110, 'funding-source/save', 'រក្សាទុក', 5, '', 0),
(116, 'តារាងផែនការកែតម្រូវ', 44, 'page-1', '', 2, 'files/icons/1470100366_34737.png', 1),
(117, 'ទំនិញ សំណង់ សេវាកម្ម', 126, 'report-adviser-goods-construct-service', 'ទំនិញ សំណង់ សេវាកម្ម', 4, '', 1),
(118, 'សេវាទីប្រឹក្សា', 124, 'report-start-year-adviser', '', 4, '', 1),
(119, 'សេវាទីប្រឹក្សា', 126, 'report-edit-adviser', 'សេវាទីប្រឹក្សា', 4, '', 1),
(120, 'មើល', 117, 'report-adviser-goods-construct-service/index', 'មើល', 1, '', 0),
(121, 'មើល', 118, 'report-start-year-adviser/index', 'មើល', 4, '', 0),
(129, 'ផ្តូរពាក្យសំងាត់', 3, 'change-password', 'ផ្តូរពាក្យសំងាត់', 3, '', 1),
(130, 'ចេញពីប្រព័ន្ធ', 129, 'change-password/index', 'ចេញពីប្រព័ន្ធ', 1, '', 0),
(122, 'តេស', 119, 'report-edit-adviser/index', '', 1, '', 0),
(123, 'ស្ថាប័នអនុវត្តថវិកាកម្មវិធី', 45, 'report-start-year-financial-institution', 'ស្ថាប័នអនុវត្តថវិកាកម្មវិធី', 3, '', 1),
(124, 'ស្ថាប័នអនុវត្តលទ្ធកម្ម', 45, 'page-1', 'ស្ថាប័នអនុវត្តលទ្ទកម្ម', 3, 'files/icons/1470100407_36635.png', 1),
(125, 'ស្ថាប័នអនុវត្តថវិកាកម្មវិធី', 116, 'report-edit-financial-institution', 'ស្ថាប័នអនុវត្តថវិកាកម្មវិធី', 3, '', 1),
(126, 'ស្ថាប័នអនុវត្តលទ្ធកម្ម', 116, 'page-1', 'ស្ថាប័នអនុវត្តលទ្ធកម្ម', 3, 'files/icons/1470100436_25686.png', 1),
(127, 'មើល', 123, 'report-start-year-financial-institution/index', 'មើល', 4, '', 0),
(128, 'មើល', 125, 'report-edit-financial-institution/index', 'មើល', 4, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `moef_commune`
--

CREATE TABLE IF NOT EXISTS `moef_commune` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `moef_commune`
--

INSERT INTO `moef_commune` (`id`, `province_id`, `district_id`, `name`, `active`) VALUES
(1, 2, 11, 'ការដាប់', 1),
(2, 2, 9, 'តេស', 1),
(3, 1, 8, 'រការកេាង', 1),
(4, 2, 10, 'ខេខេ', 1),
(5, 2, 9, 'កាកា', 1),
(6, 2, 10, 'មាន់', 1),
(7, 11, 12, 'កេាះខ្សាច់ទន្លា', 1);

-- --------------------------------------------------------

--
-- Table structure for table `moef_department`
--

CREATE TABLE IF NOT EXISTS `moef_department` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `department` varchar(300) DEFAULT NULL,
  `abbr` varchar(300) DEFAULT NULL,
  `description` text,
  `order_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `moef_department`
--

INSERT INTO `moef_department` (`id`, `department`, `abbr`, `description`, `order_number`) VALUES
(1, 'Information Technology Department', 'ITD', 'Information Technology Department', NULL),
(2, 'Research & Development', 'R&D', NULL, NULL),
(5, 'General Department of Taxation', 'GDT', 'General Department of Taxation', NULL),
(6, 'Economic and Public Finance Policy Department', 'EPFPD', 'Economic and Public Finance Policy Department', NULL),
(7, 'Administration and Finance Department', 'AFD', 'Administration and Finance Department', NULL),
(8, 'Customs and Excise Department', 'CED', 'Customs and Excise Department', NULL),
(9, 'Investment and Cooperation Department', 'ICD', 'Investment and Cooperation Department', NULL),
(10, 'Non-tax Revenue Department', 'NTRD', 'Non-tax Revenue Department', NULL),
(11, 'Financial Industry Department', 'FID', 'Financial Industry Department', NULL),
(12, 'Financial Affairs Department', 'FAD', 'Financial Affairs Department', NULL),
(13, 'General Department of Public Procurement', 'GDPP', 'General Department of Public Procurement', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `moef_district`
--

CREATE TABLE IF NOT EXISTS `moef_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) DEFAULT NULL,
  `name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `moef_district`
--

INSERT INTO `moef_district` (`id`, `province_id`, `name`, `active`) VALUES
(8, 1, 'រកា', 1),
(9, 2, 'ធំកាក់', 1),
(10, 2, 'ធំកំ', 1),
(11, 2, 'ធំកំបេារ', 1),
(12, 11, 'ស្អាង', 1);

-- --------------------------------------------------------

--
-- Table structure for table `moef_meeting`
--

CREATE TABLE IF NOT EXISTS `moef_meeting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `list_atendee_id` text,
  `moef_user_id` int(11) DEFAULT NULL,
  `meeting_date` date DEFAULT NULL,
  `meeting_from_time` varchar(10) DEFAULT NULL,
  `meeting_to_time` varchar(10) DEFAULT NULL,
  `meeting_objective` text CHARACTER SET utf8,
  `meeting_attachment_file` text CHARACTER SET utf8,
  `meeting_attachment_file_original` varchar(300) DEFAULT NULL,
  `meeting_attachment_summery` text CHARACTER SET utf8,
  `meeting_location` text CHARACTER SET utf8,
  `meeting_active` tinyint(4) DEFAULT NULL,
  `other` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf32 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `moef_meeting`
--

INSERT INTO `moef_meeting` (`id`, `list_atendee_id`, `moef_user_id`, `meeting_date`, `meeting_from_time`, `meeting_to_time`, `meeting_objective`, `meeting_attachment_file`, `meeting_attachment_file_original`, `meeting_attachment_summery`, `meeting_location`, `meeting_active`, `other`) VALUES
(12, '7,2,4,8', 3, '2016-05-17', '12:00 AM', '12:00 AM', '', 'files/meeting-attachment/1463458813_37725.pptx', 'FMIS_TR_Development_Tools_Basics_V100.pptx', '', NULL, 1, ''),
(13, '', 3, '2016-05-17', '1:00 PM', '2:00 PM', '', '', '', '', NULL, 1, ''),
(14, '', 3, '2016-05-17', '2:00 PM', '1:00 PM', '', '', '', '', NULL, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `moef_position`
--

CREATE TABLE IF NOT EXISTS `moef_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(300) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `order_number` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `moef_position`
--

INSERT INTO `moef_position` (`id`, `position`, `description`, `order_number`) VALUES
(1, 'Project Manager', 'Project Managers', 0),
(2, 'Contract Officer', 'Contract Officers', 0),
(3, 'Project Coordinator', 'Project Coordinators', 0);

-- --------------------------------------------------------

--
-- Table structure for table `moef_province`
--

CREATE TABLE IF NOT EXISTS `moef_province` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `moef_province`
--

INSERT INTO `moef_province` (`Id`, `Name`, `IsActive`, `Status`) VALUES
(4, 'ខេត្តកំពង់ស្ពឺ', 1, 1),
(5, 'ខេត្តកំពង់ធំ​', 1, 1),
(6, 'រាជធានីភ្នំពេញ​', 1, 1),
(7, 'ខេត្តតាកែវ', 1, 1),
(8, 'ខេត្តព្រៃវែង', 1, 1),
(9, 'ខេត្តព្រះវិហារ', 1, 1),
(10, 'ខេត្តកណ្តាល', 1, 1),
(11, 'ខេត្តបាត់តំបង', 1, 1),
(12, 'ខេត្តកំពង់ចាម', 1, 1),
(13, 'ខេត្តសៀមរាប', 1, 1),
(14, 'ខេត្តកំពង់ឆ្នាំង', 1, 1),
(15, 'ខេត្តប៉ៃលិន', 1, 1),
(16, 'ខេត្តស្វាយរៀង​', 1, 1),
(17, 'ខេត្តក្រចេះ​', 1, 1),
(18, 'ខេត្ត​កំពត', 1, 1),
(19, 'ខេត្តមណ្ឌលគិរី​', 1, 1),
(20, 'ខេត្តព្រះសីហនុ​', 1, 1),
(21, 'ខេត្តបន្ទាយមានជ័យ​', 1, 1),
(22, 'ខេត្តកែប', 1, 1),
(23, 'ខេត្តរតនគិរី', 1, 1),
(24, 'ខេត្តពោធិសាត់​', 1, 1),
(25, 'ខេត្តស្ទឹងត្រែង​', 1, 1),
(26, 'ខេត្តកោះកុង​', 1, 1),
(27, 'ខេត្តឧត្តរ​មានជ័យ​', 1, 1),
(28, 'ខេត្ត​ត្បូង​ឃ្មុំ​', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `moef_role`
--

CREATE TABLE IF NOT EXISTS `moef_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(300) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `moef_role`
--

INSERT INTO `moef_role` (`id`, `role`, `parent_id`, `active`, `description`) VALUES
(1, 'System Administrator', 0, 1, ''),
(33, 'Moderater', 0, 1, ''),
(34, 'Accountant', 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `moef_staff`
--

CREATE TABLE IF NOT EXISTS `moef_staff` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(300) DEFAULT NULL,
  `last_name` varchar(300) DEFAULT NULL,
  `full_name` varchar(300) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `place_of_birth` varchar(300) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `phone_number` varchar(300) DEFAULT NULL,
  `avatar` varchar(300) DEFAULT NULL,
  `address1` text,
  `address2` text,
  `order_number` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `moef_staff`
--

INSERT INTO `moef_staff` (`id`, `first_name`, `last_name`, `full_name`, `gender`, `date_of_birth`, `place_of_birth`, `email`, `phone_number`, `avatar`, `address1`, `address2`, `order_number`, `position_id`, `active`) VALUES
(2, 'Rin', 'Narith', 'Rin Narith', 0, '1986-04-12', 'PP', 'narith.rin@borama.consulting', '015763683', '', 'PP', NULL, NULL, 2, 1),
(3, 'Heng', 'Vanda', 'Heng Vanda', 1, '2016-05-03', 'PP', 'hengvanda@gmail.com', '0125863365', 'files/staffs/1462845937_44980.jpg', 'PP', NULL, NULL, 2, 1),
(4, 'Sangvarith', 'Seang', 'Seang Sangvarith', 1, '2016-05-13', 'pp', 'sokunthea_chev@yahoo.com', '012336222', '', 'pp', NULL, NULL, 2, 1),
(5, 'Pheany', 'Pring', 'Aheany Pring', 0, '2016-05-13', 'pp', 'sreykongmeas20@gmail.com', '2002225522', '', 'pp', NULL, NULL, 2, 1),
(6, 'Kong', 'Marinmolive', 'Kong Marinmolive', 0, '2016-05-19', 'pp', 'sreykongmeas20@gmail.com', '0163636353', '', 'pp', NULL, NULL, 3, 1),
(7, 'Kim', 'Saray', 'Kim Saray', 0, '2016-05-07', 'pp', 'rin_lena96@yahoo.com', '0258336955', '', 'pp', NULL, NULL, 3, 1),
(8, 'Klaing', 'Ny', 'Klaing Ny', 0, '2016-05-13', 'pp', 'sarat.saing@borama.consulting', '025843655', '', 'pp', NULL, NULL, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `moef_user`
--

CREATE TABLE IF NOT EXISTS `moef_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(300) DEFAULT NULL,
  `last_name` varchar(300) DEFAULT NULL,
  `user_name` varchar(300) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `avatar` varchar(300) DEFAULT NULL,
  `phone_number` varchar(300) DEFAULT NULL,
  `address1` text,
  `address2` text,
  `password` varchar(300) DEFAULT NULL,
  `salt` varchar(300) DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `moef_role_id` int(11) DEFAULT NULL,
  `moef_department_id` int(11) DEFAULT '0',
  `order_number` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `moef_user`
--

INSERT INTO `moef_user` (`id`, `first_name`, `last_name`, `user_name`, `email`, `avatar`, `phone_number`, `address1`, `address2`, `password`, `salt`, `last_login_date`, `moef_role_id`, `moef_department_id`, `order_number`, `active`) VALUES
(3, 'រិន', 'ណារិទ្ធ', 'admin', 'rin_narith@yahoo.com', 'files/users/1462758425_34868.jpg', '015763638', 'Phnom Penh', NULL, '$2y$10$YrQ4DF/nzJ995p92hRS2lOgsj5LJnt3eUPPJWD6OC8MHZK.gFnZiK', '123', '2016-08-04 14:56:55', 1, 0, NULL, 1),
(14, 'Klaing', 'នី', 'ny', 'narith.rin@borama.consulting', '', '01253655', 'Phnom Penh', NULL, '$2y$10$BsYeE.SIX7624t.weITnr.jeLHVUeErv8OrfJWU.Pkn1ieV/3oWgO', '123', '2016-06-06 23:32:18', 33, 6, NULL, 1),
(15, 'Rin', 'Saray', 'saray', 'sarat.saing@borama.consulting', '', '012563366', 'Phnom Penh', NULL, '$2y$10$j1CUWLyrc5gTl7P3uifsMequESEmtpmB6e4HaYIyDESeq04GuJg4e', '123', '2016-05-28 15:22:54', 1, 0, NULL, 1),
(16, 'Rin', 'Saray', 'sokleng', 'sarat.saing@borama.consulting', '', '01253655', 'Phnom Penh', NULL, '$2y$10$4ZCMNCDQ9kf4CYKIP6ZLQOC8159Sx.uaVyZrOW.ZE9mfhpqemfSWq', '123', '2016-06-08 12:52:50', 33, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `moef_village`
--

CREATE TABLE IF NOT EXISTS `moef_village` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `moef_village`
--

INSERT INTO `moef_village` (`id`, `province_id`, `district_id`, `commune_id`, `name`, `active`) VALUES
(9, 11, 12, 7, 'កណ្តាលកោះកើត', 1),
(10, 11, 12, 7, 'កណ្តាលកេាះ', 1),
(8, 11, 12, 7, 'កណ្តាកោះលិច', 1),
(11, 11, 12, 7, 'ក្បាលកោះ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_advisory_services_international`
--

CREATE TABLE IF NOT EXISTS `procurement_advisory_services_international` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainProcurementId` int(11) DEFAULT '0',
  `ReferenceTotalDay_1` int(11) DEFAULT NULL,
  `ReferenceTotalDaySufix_1` varchar(11) DEFAULT NULL,
  `ReferenceFromDate_1` date DEFAULT NULL,
  `ReferenceToDate_1` date DEFAULT NULL,
  `ReferenceDescription_1` varchar(255) DEFAULT NULL,
  `ReferenceTotalDay_2` int(11) DEFAULT NULL,
  `ReferenceTotalDaySufix_2` varchar(11) DEFAULT NULL,
  `ReferenceFromDate_2` date DEFAULT NULL,
  `ReferenceToDate_2` date DEFAULT NULL,
  `ReferenceDescription_2` varchar(255) DEFAULT NULL,
  `ReferenceTotalDay_3` int(11) DEFAULT NULL,
  `ReferenceTotalDaySufix_3` varchar(11) DEFAULT NULL,
  `ReferenceFromDate_3` date DEFAULT NULL,
  `ReferenceToDate_3` date DEFAULT NULL,
  `ReferenceDescription_3` varchar(255) DEFAULT NULL,
  `ReferenceTotalDay_4` int(11) DEFAULT NULL,
  `ReferenceTotalDaySufix_4` varchar(11) DEFAULT NULL,
  `ReferenceFromDate_4` date DEFAULT NULL,
  `ReferenceToDate_4` date DEFAULT NULL,
  `ReferenceDescription_4` varchar(255) DEFAULT NULL,
  `RFPTotalDay_1` int(11) DEFAULT NULL,
  `RFPTotalDaySufix_1` varchar(11) DEFAULT NULL,
  `RFPToDate_1` date DEFAULT NULL,
  `RFPFromDate_1` date DEFAULT NULL,
  `RFPDescription_1` varchar(255) DEFAULT NULL,
  `RFPTotalDay_2` int(11) DEFAULT NULL,
  `RFPTotalDaySufix_2` varchar(11) DEFAULT NULL,
  `RFPToDate_2` date DEFAULT NULL,
  `RFPFromDate_2` date DEFAULT NULL,
  `RFPDescription_2` varchar(255) DEFAULT NULL,
  `RFPTotalDay_3` int(11) DEFAULT NULL,
  `RFPTotalDaySufix_3` varchar(11) DEFAULT NULL,
  `RFPToDate_3` date DEFAULT NULL,
  `RFPFromDate_3` date DEFAULT NULL,
  `RFPDescription_3` varchar(255) DEFAULT NULL,
  `RFPTotalDay_4` int(11) DEFAULT NULL,
  `RFPTotalDaySufix_4` varchar(11) DEFAULT NULL,
  `RFPToDate_4` date DEFAULT NULL,
  `RFPFromDate_4` date DEFAULT NULL,
  `RFPDescription_4` varchar(255) DEFAULT NULL,
  `TechnologicalTotalDay_1` int(11) DEFAULT NULL,
  `TechnologicalTotalDaySufix_1` varchar(11) DEFAULT NULL,
  `TechnologicalToDate_1` date DEFAULT NULL,
  `TechnologicalFromDate_1` date DEFAULT NULL,
  `TechnologicalDescription_1` varchar(255) DEFAULT NULL,
  `TechnologicalTotalDay_2` int(11) DEFAULT NULL,
  `TechnologicalTotalDaySufix_2` varchar(11) DEFAULT NULL,
  `TechnologicalToDate_2` date DEFAULT NULL,
  `TechnologicalFromDate_2` date DEFAULT NULL,
  `TechnologicalDescription_2` varchar(255) DEFAULT NULL,
  `TechnologicalTotalDay_3` int(11) DEFAULT NULL,
  `TechnologicalTotalDaySufix_3` varchar(11) DEFAULT NULL,
  `TechnologicalToDate_3` date DEFAULT NULL,
  `TechnologicalFromDate_3` date DEFAULT NULL,
  `TechnologicalDescription_3` varchar(255) DEFAULT NULL,
  `TechnologicalTotalDay_4` int(11) DEFAULT NULL,
  `TechnologicalTotalDaySufix_4` varchar(11) DEFAULT NULL,
  `TechnologicalToDate_4` date DEFAULT NULL,
  `TechnologicalFromDate_4` date DEFAULT NULL,
  `TechnologicalDescription_4` varchar(255) DEFAULT NULL,
  `FinanciallyTotalDay_1` int(11) DEFAULT NULL,
  `FinanciallyTotalDaySufix_1` varchar(11) DEFAULT NULL,
  `FinanciallyToDate_1` date DEFAULT NULL,
  `FinanciallyFromDate_1` date DEFAULT NULL,
  `FinanciallyDescription_1` varchar(255) DEFAULT NULL,
  `FinanciallyTotalDay_2` int(11) DEFAULT NULL,
  `FinanciallyTotalDaySufix_2` varchar(11) DEFAULT NULL,
  `FinanciallyToDate_2` date DEFAULT NULL,
  `FinanciallyFromDate_2` date DEFAULT NULL,
  `FinanciallyDescription_2` varchar(255) DEFAULT NULL,
  `FinanciallyTotalDay_3` int(11) DEFAULT NULL,
  `FinanciallyTotalDaySufix_3` varchar(11) DEFAULT NULL,
  `FinanciallyToDate_3` date DEFAULT NULL,
  `FinanciallyFromDate_3` date DEFAULT NULL,
  `FinanciallyDescription_3` varchar(255) DEFAULT NULL,
  `FinanciallyTotalDay_4` int(11) DEFAULT NULL,
  `FinanciallyTotalDaySufix_4` varchar(11) DEFAULT NULL,
  `FinanciallyToDate_4` date DEFAULT NULL,
  `FinanciallyFromDate_4` date DEFAULT NULL,
  `FinanciallyDescription_4` varchar(255) DEFAULT NULL,
  `FinanciallyTotalDay_5` int(11) DEFAULT NULL,
  `FinanciallyTotalDaySufix_5` varchar(11) DEFAULT NULL,
  `FinanciallyToDate_5` date DEFAULT NULL,
  `FinanciallyFromDate_5` date DEFAULT NULL,
  `FinanciallyDescription_5` varchar(255) DEFAULT NULL,
  `ContractTotalDay_1` int(11) DEFAULT NULL,
  `ContractTotalDaySufix_1` varchar(11) DEFAULT NULL,
  `ContractToDate_1` date DEFAULT NULL,
  `ContractFromDate_1` date DEFAULT NULL,
  `ContractDescription_1` varchar(255) DEFAULT NULL,
  `ContractTotalDay_2` int(11) DEFAULT NULL,
  `ContractTotalDaySufix_2` varchar(11) DEFAULT NULL,
  `ContractToDate_2` date DEFAULT NULL,
  `ContractFromDate_2` date DEFAULT NULL,
  `ContractDescription_2` varchar(255) DEFAULT NULL,
  `ContractTotalDay_3` int(11) DEFAULT NULL,
  `ContractTotalDaySufix_3` varchar(11) DEFAULT NULL,
  `ContractToDate_3` date DEFAULT NULL,
  `ContractFromDate_3` date DEFAULT NULL,
  `ContractDescription_3` varchar(255) DEFAULT NULL,
  `ContractTotalDay_4` int(11) DEFAULT NULL,
  `ContractTotalDaySufix_4` varchar(11) DEFAULT NULL,
  `ContractToDate_4` date DEFAULT NULL,
  `ContractFromDate_4` date DEFAULT NULL,
  `ContractDescription_4` varchar(255) DEFAULT NULL,
  `ContractTotalDay_5` int(11) DEFAULT NULL,
  `ContractTotalDaySufix_5` varchar(11) DEFAULT NULL,
  `ContractToDate_5` date DEFAULT NULL,
  `ContractFromDate_5` date DEFAULT NULL,
  `ContractDescription_5` varchar(255) DEFAULT NULL,
  `ContractTotalDay_6` int(11) DEFAULT NULL,
  `ContractTotalDaySufix_6` varchar(11) DEFAULT NULL,
  `ContractToDate_6` date DEFAULT NULL,
  `ContractFromDate_6` date DEFAULT NULL,
  `ContractDescription_6` varchar(255) DEFAULT NULL,
  `ContractTotalDay_7` int(11) DEFAULT NULL,
  `ContractTotalDaySufix_7` varchar(11) DEFAULT NULL,
  `ContractToDate_7` date DEFAULT NULL,
  `ContractFromDate_7` date DEFAULT NULL,
  `ContractDescription_7` varchar(255) DEFAULT NULL,
  `ContractTotalDay_8` int(11) DEFAULT NULL,
  `ContractTotalDaySufix_8` varchar(11) DEFAULT NULL,
  `ContractToDate_8` date DEFAULT NULL,
  `ContractFromDate_8` date DEFAULT NULL,
  `ContractDescription_8` varchar(255) DEFAULT NULL,
  `PerformanceTotalDay_1` int(11) DEFAULT NULL,
  `PerformanceToDate_1` date DEFAULT NULL,
  `PerformanceTotalDaySufix_1` varchar(10) DEFAULT NULL,
  `PerformanceFromDate_1` date DEFAULT NULL,
  `PerformanceDescription_1` varchar(255) DEFAULT NULL,
  `PerformanceTotalDay_2` int(11) DEFAULT NULL,
  `PerformanceTotalDaySufix_2` varchar(11) DEFAULT NULL,
  `PerformanceToDate_2` date DEFAULT NULL,
  `PerformanceFromDate_2` date DEFAULT NULL,
  `PerformanceDescription_2` varchar(255) DEFAULT NULL,
  `PerformanceTotalDay_3` int(11) DEFAULT NULL,
  `PerformanceTotalDaySufix_3` varchar(11) DEFAULT NULL,
  `PerformanceToDate_3` date DEFAULT NULL,
  `PerformanceFromDate_3` date DEFAULT NULL,
  `PerformanceDescription_3` varchar(255) DEFAULT NULL,
  `PerformanceTotalDay_4` int(11) DEFAULT NULL,
  `PerformanceTotalDaySufix_4` varchar(11) DEFAULT NULL,
  `PerformanceToDate_4` date DEFAULT NULL,
  `PerformanceFromDate_4` date DEFAULT NULL,
  `PerformanceDescription_4` varchar(255) DEFAULT NULL,
  `PerformanceTotalDay_5` int(11) DEFAULT NULL,
  `PerformanceTotalDaySufix_5` varchar(11) DEFAULT NULL,
  `PerformanceToDate_5` date DEFAULT NULL,
  `PerformanceFromDate_5` date DEFAULT NULL,
  `PerformanceDescription_5` varchar(255) DEFAULT NULL,
  `PerformanceTotalDay_6` int(11) DEFAULT NULL,
  `PerformanceTotalDaySufix_6` varchar(11) DEFAULT NULL,
  `PerformanceToDate_6` date DEFAULT NULL,
  `PerformanceFromDate_6` date DEFAULT NULL,
  `PerformanceDescription_6` varchar(255) DEFAULT NULL,
  `PerformanceTotalDay_7` int(11) DEFAULT NULL,
  `PerformanceTotalDaySufix_7` varchar(11) DEFAULT NULL,
  `PerformanceFromDate_7` date DEFAULT NULL,
  `PerformanceToDate_7` date DEFAULT NULL,
  `PerformanceDescription_7` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `procurement_advisory_services_international`
--

INSERT INTO `procurement_advisory_services_international` (`Id`, `MainProcurementId`, `ReferenceTotalDay_1`, `ReferenceTotalDaySufix_1`, `ReferenceFromDate_1`, `ReferenceToDate_1`, `ReferenceDescription_1`, `ReferenceTotalDay_2`, `ReferenceTotalDaySufix_2`, `ReferenceFromDate_2`, `ReferenceToDate_2`, `ReferenceDescription_2`, `ReferenceTotalDay_3`, `ReferenceTotalDaySufix_3`, `ReferenceFromDate_3`, `ReferenceToDate_3`, `ReferenceDescription_3`, `ReferenceTotalDay_4`, `ReferenceTotalDaySufix_4`, `ReferenceFromDate_4`, `ReferenceToDate_4`, `ReferenceDescription_4`, `RFPTotalDay_1`, `RFPTotalDaySufix_1`, `RFPToDate_1`, `RFPFromDate_1`, `RFPDescription_1`, `RFPTotalDay_2`, `RFPTotalDaySufix_2`, `RFPToDate_2`, `RFPFromDate_2`, `RFPDescription_2`, `RFPTotalDay_3`, `RFPTotalDaySufix_3`, `RFPToDate_3`, `RFPFromDate_3`, `RFPDescription_3`, `RFPTotalDay_4`, `RFPTotalDaySufix_4`, `RFPToDate_4`, `RFPFromDate_4`, `RFPDescription_4`, `TechnologicalTotalDay_1`, `TechnologicalTotalDaySufix_1`, `TechnologicalToDate_1`, `TechnologicalFromDate_1`, `TechnologicalDescription_1`, `TechnologicalTotalDay_2`, `TechnologicalTotalDaySufix_2`, `TechnologicalToDate_2`, `TechnologicalFromDate_2`, `TechnologicalDescription_2`, `TechnologicalTotalDay_3`, `TechnologicalTotalDaySufix_3`, `TechnologicalToDate_3`, `TechnologicalFromDate_3`, `TechnologicalDescription_3`, `TechnologicalTotalDay_4`, `TechnologicalTotalDaySufix_4`, `TechnologicalToDate_4`, `TechnologicalFromDate_4`, `TechnologicalDescription_4`, `FinanciallyTotalDay_1`, `FinanciallyTotalDaySufix_1`, `FinanciallyToDate_1`, `FinanciallyFromDate_1`, `FinanciallyDescription_1`, `FinanciallyTotalDay_2`, `FinanciallyTotalDaySufix_2`, `FinanciallyToDate_2`, `FinanciallyFromDate_2`, `FinanciallyDescription_2`, `FinanciallyTotalDay_3`, `FinanciallyTotalDaySufix_3`, `FinanciallyToDate_3`, `FinanciallyFromDate_3`, `FinanciallyDescription_3`, `FinanciallyTotalDay_4`, `FinanciallyTotalDaySufix_4`, `FinanciallyToDate_4`, `FinanciallyFromDate_4`, `FinanciallyDescription_4`, `FinanciallyTotalDay_5`, `FinanciallyTotalDaySufix_5`, `FinanciallyToDate_5`, `FinanciallyFromDate_5`, `FinanciallyDescription_5`, `ContractTotalDay_1`, `ContractTotalDaySufix_1`, `ContractToDate_1`, `ContractFromDate_1`, `ContractDescription_1`, `ContractTotalDay_2`, `ContractTotalDaySufix_2`, `ContractToDate_2`, `ContractFromDate_2`, `ContractDescription_2`, `ContractTotalDay_3`, `ContractTotalDaySufix_3`, `ContractToDate_3`, `ContractFromDate_3`, `ContractDescription_3`, `ContractTotalDay_4`, `ContractTotalDaySufix_4`, `ContractToDate_4`, `ContractFromDate_4`, `ContractDescription_4`, `ContractTotalDay_5`, `ContractTotalDaySufix_5`, `ContractToDate_5`, `ContractFromDate_5`, `ContractDescription_5`, `ContractTotalDay_6`, `ContractTotalDaySufix_6`, `ContractToDate_6`, `ContractFromDate_6`, `ContractDescription_6`, `ContractTotalDay_7`, `ContractTotalDaySufix_7`, `ContractToDate_7`, `ContractFromDate_7`, `ContractDescription_7`, `ContractTotalDay_8`, `ContractTotalDaySufix_8`, `ContractToDate_8`, `ContractFromDate_8`, `ContractDescription_8`, `PerformanceTotalDay_1`, `PerformanceToDate_1`, `PerformanceTotalDaySufix_1`, `PerformanceFromDate_1`, `PerformanceDescription_1`, `PerformanceTotalDay_2`, `PerformanceTotalDaySufix_2`, `PerformanceToDate_2`, `PerformanceFromDate_2`, `PerformanceDescription_2`, `PerformanceTotalDay_3`, `PerformanceTotalDaySufix_3`, `PerformanceToDate_3`, `PerformanceFromDate_3`, `PerformanceDescription_3`, `PerformanceTotalDay_4`, `PerformanceTotalDaySufix_4`, `PerformanceToDate_4`, `PerformanceFromDate_4`, `PerformanceDescription_4`, `PerformanceTotalDay_5`, `PerformanceTotalDaySufix_5`, `PerformanceToDate_5`, `PerformanceFromDate_5`, `PerformanceDescription_5`, `PerformanceTotalDay_6`, `PerformanceTotalDaySufix_6`, `PerformanceToDate_6`, `PerformanceFromDate_6`, `PerformanceDescription_6`, `PerformanceTotalDay_7`, `PerformanceTotalDaySufix_7`, `PerformanceFromDate_7`, `PerformanceToDate_7`, `PerformanceDescription_7`) VALUES
(6, 64, 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 2, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 4, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 2, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 0, '0000-00-00', '', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', ''),
(7, 65, 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 2, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 4, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 2, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 1, 'សប្តាហ៏', '0000-00-00', '0000-00-00', '', 0, '0000-00-00', '', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `procurement_advisory_services_local`
--

CREATE TABLE IF NOT EXISTS `procurement_advisory_services_local` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainProcurementId` int(11) DEFAULT '0',
  `BidTotalDay_1` int(11) DEFAULT NULL,
  `BidTotalDaySufix_1` varchar(20) DEFAULT NULL,
  `BidFromDate_1` date DEFAULT NULL,
  `BidToDate_1` date DEFAULT NULL,
  `BidDescription_1` varchar(255) DEFAULT NULL,
  `BidTotalDay_2` int(11) DEFAULT NULL,
  `BidTotalDaySufix_2` varchar(20) DEFAULT NULL,
  `BidFromDate_2` date DEFAULT NULL,
  `BidToDate_2` date DEFAULT NULL,
  `BidDescription_2` varchar(255) DEFAULT NULL,
  `BidTotalDay_3` int(11) DEFAULT NULL,
  `BidTotalDaySufix_3` varchar(20) DEFAULT NULL,
  `BidFromDate_3` date DEFAULT NULL,
  `BidToDate_3` date DEFAULT NULL,
  `BidDescription_3` varchar(255) DEFAULT NULL,
  `BidTotalDay_4` int(11) DEFAULT NULL,
  `BidTotalDaySufix_4` varchar(20) DEFAULT NULL,
  `BidFromDate_4` date DEFAULT NULL,
  `BidToDate_4` date DEFAULT NULL,
  `BidDescription_4` varchar(255) DEFAULT NULL,
  `AdvertiseTotalDay_1` int(11) DEFAULT NULL,
  `AdvertiseTotalDaySufix_1` varchar(20) DEFAULT NULL,
  `AdvertiseFromDate_1` date DEFAULT NULL,
  `AdvertiseToDate_1` date DEFAULT NULL,
  `AdvertiseDescription_1` varchar(255) DEFAULT NULL,
  `EvaluationTotalDay_1` int(11) DEFAULT NULL,
  `EvaluationFromDate_1` date DEFAULT NULL,
  `EvaluationToDate_1` date DEFAULT NULL,
  `EvaluationDescription_1` varchar(255) DEFAULT NULL,
  `EvaluationTotalDay_2` int(11) DEFAULT NULL,
  `EvaluationTotalDaySufix_2` varchar(20) CHARACTER SET utf32 DEFAULT NULL,
  `EvaluationFromDate_2` date DEFAULT NULL,
  `EvaluationToDate_2` date DEFAULT NULL,
  `EvaluationDescription_2` varchar(255) DEFAULT NULL,
  `EvaluationTotalDay_3` int(11) DEFAULT NULL,
  `EvaluationTotalDaySufix_3` varchar(20) DEFAULT NULL,
  `EvaluationFromDate_3` date DEFAULT NULL,
  `EvaluationToDate_3` date DEFAULT NULL,
  `EvaluationDescription_3` varchar(255) DEFAULT NULL,
  `EvaluationTotalDay_4` int(11) DEFAULT NULL,
  `EvaluationTotalDaySufix_4` varchar(20) DEFAULT NULL,
  `EvaluationFromDate_4` date DEFAULT NULL,
  `EvaluationToDate_4` date DEFAULT NULL,
  `EvaluationDescription_4` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_1` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_1` varchar(20) DEFAULT NULL,
  `ContractorFromDate_1` date DEFAULT NULL,
  `ContractorToDate_1` date DEFAULT NULL,
  `ContractorDescription_1` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_2` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_2` varchar(20) DEFAULT NULL,
  `ContractorFromDate_2` date DEFAULT NULL,
  `ContractorToDate_2` date DEFAULT NULL,
  `ContractorDescription_2` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_3` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_3` varchar(20) DEFAULT NULL,
  `ContractorFromDate_3` date DEFAULT NULL,
  `ContractorToDate_3` date DEFAULT NULL,
  `ContractorDescription_3` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_4` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_4` varchar(20) DEFAULT NULL,
  `ContractorFromDate_4` date DEFAULT NULL,
  `ContractorToDate_4` date DEFAULT NULL,
  `ContractorDescription_4` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_5` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_5` varchar(20) DEFAULT NULL,
  `ContractorFromDate_5` date DEFAULT NULL,
  `ContractorToDate_5` date DEFAULT NULL,
  `ContractorDescription_5` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_1` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_1` varchar(20) DEFAULT NULL,
  `EntrrustedFromDate_1` date DEFAULT NULL,
  `EntrrustedToDate_1` date DEFAULT NULL,
  `EntrrustedDescription_1` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_2` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_2` varchar(20) DEFAULT NULL,
  `EntrrustedFromDate_2` date DEFAULT NULL,
  `EntrrustedToDate_2` date DEFAULT NULL,
  `EntrrustedDescription_2` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_3` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_3` varchar(20) DEFAULT NULL,
  `EntrrustedFromDate_3` date DEFAULT NULL,
  `EntrrustedToDate_3` date DEFAULT NULL,
  `EntrrustedDescription_3` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_4` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_4` varchar(20) DEFAULT NULL,
  `EntrrustedFromDate_4` date DEFAULT NULL,
  `EntrrustedToDate_4` date DEFAULT NULL,
  `EntrrustedDescription_4` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_5` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_5` varchar(20) DEFAULT NULL,
  `EntrrustedFromDate_5` date DEFAULT NULL,
  `EntrrustedToDate_5` date DEFAULT NULL,
  `EntrrustedDescription_5` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_6` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_6` varchar(20) DEFAULT NULL,
  `EntrrustedFromDate_6` date DEFAULT NULL,
  `EntrrustedToDate_6` date DEFAULT NULL,
  `EntrrustedDescription_6` varchar(255) DEFAULT NULL,
  `PaymentTotalDay_1` int(11) DEFAULT NULL,
  `PaymentTotalDaySufix_1` varchar(20) DEFAULT NULL,
  `PaymentFromDate_1` date DEFAULT NULL,
  `PaymentToDate_1` date DEFAULT NULL,
  `PaymentDescription_1` varchar(255) DEFAULT NULL,
  `Payment_1` bigint(20) DEFAULT NULL,
  `Payment_2` bigint(20) DEFAULT NULL,
  `Payment_3` bigint(20) DEFAULT NULL,
  `Payment_4` bigint(20) DEFAULT NULL,
  `PaymentNextYear` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_currency`
--

CREATE TABLE IF NOT EXISTS `procurement_currency` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Symbole` varchar(255) DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `OrderNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `procurement_currency`
--

INSERT INTO `procurement_currency` (`Id`, `Name`, `Symbole`, `IsActive`, `OrderNumber`) VALUES
(1, 'លានរៀល', 'លានរៀល', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_entity_list`
--

CREATE TABLE IF NOT EXISTS `procurement_entity_list` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(500) NOT NULL,
  `IsActive` int(11) NOT NULL,
  `OrderNumber` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `procurement_entity_list`
--

INSERT INTO `procurement_entity_list` (`Id`, `Name`, `IsActive`, `OrderNumber`) VALUES
(1, 'រាជធានី​-ខេត្ត​', 1, 1),
(2, 'គ្រឹះស្ថាន​សាធារណៈ​-សហគ្រាស​សាធារណៈ', 1, 1),
(3, 'ក្រសួង​-ស្ថាប័ន​ថ្នាក់​កណ្ដាល​', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_goods_contruction_services`
--

CREATE TABLE IF NOT EXISTS `procurement_goods_contruction_services` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainProcurementId` int(11) DEFAULT '0',
  `PropertiesTotalDay_1` int(11) DEFAULT NULL,
  `PropertiesTotalDaySufix_1` varchar(255) DEFAULT NULL,
  `PropertiesFromDate_1` date DEFAULT NULL,
  `PropertiesToDate_1` date DEFAULT NULL,
  `PropertiesDescription_1` varchar(255) DEFAULT NULL,
  `PropertiesTotalDay_2` int(11) DEFAULT NULL,
  `PropertiesTotalDaySufix_2` varchar(255) DEFAULT NULL,
  `PropertiesFromDate_2` date DEFAULT NULL,
  `PropertiesToDate_2` date DEFAULT NULL,
  `PropertiesDescription_2` varchar(255) DEFAULT NULL,
  `PropertiesTotalDay_3` int(11) DEFAULT NULL,
  `PropertiesTotalDaySufix_3` varchar(255) DEFAULT NULL,
  `PropertiesFromDate_3` date DEFAULT NULL,
  `PropertiesToDate_3` date DEFAULT NULL,
  `PropertiesDescription_3` varchar(255) DEFAULT NULL,
  `PropertiesTotalDay_4` int(11) DEFAULT NULL,
  `PropertiesTotalDaySufix_4` varchar(255) DEFAULT NULL,
  `PropertiesFromDate_4` date DEFAULT NULL,
  `PropertiesToDate_4` date DEFAULT NULL,
  `PropertiesDescription_4` varchar(255) DEFAULT NULL,
  `BidTotalDay_1` int(11) DEFAULT NULL,
  `BidTotalDaySufix_1` varchar(255) DEFAULT NULL,
  `BidFromDate_1` date DEFAULT NULL,
  `BidToDate_1` date DEFAULT NULL,
  `BidDescription_1` varchar(255) DEFAULT NULL,
  `BidTotalDay_2` int(11) DEFAULT NULL,
  `BidTotalDaySufix_2` varchar(255) DEFAULT NULL,
  `BidFromDate_2` date DEFAULT NULL,
  `BidToDate_2` date DEFAULT NULL,
  `BidDescription_2` varchar(255) DEFAULT NULL,
  `BidTotalDay_3` int(11) DEFAULT NULL,
  `BidTotalDaySufix_3` varchar(255) DEFAULT NULL,
  `BidFromDate_3` date DEFAULT NULL,
  `BidToDate_3` date DEFAULT NULL,
  `BidDescription_3` varchar(255) DEFAULT NULL,
  `BidTotalDay_4` int(11) DEFAULT NULL,
  `BidTotalDaySufix_4` varchar(255) DEFAULT NULL,
  `BidFromDate_4` date DEFAULT NULL,
  `BidToDate_4` date DEFAULT NULL,
  `BidDescription_4` varchar(255) DEFAULT NULL,
  `AdvertiseTotalDay_1` int(11) DEFAULT NULL,
  `AdvertiseTotalDaySufix_1` varchar(255) DEFAULT NULL,
  `AdvertiseFromDate_1` date DEFAULT NULL,
  `AdvertiseToDate_1` date DEFAULT NULL,
  `AdvertiseDescription_1` varchar(255) DEFAULT NULL,
  `EvaluationTotalDay_1` int(11) DEFAULT NULL,
  `EvaluationTotalDaySufix_1` varchar(255) DEFAULT NULL,
  `EvaluationFromDate_1` date DEFAULT NULL,
  `EvaluationToDate_1` date DEFAULT NULL,
  `EvaluationDescription_1` varchar(255) DEFAULT NULL,
  `EvaluationTotalDay_2` int(11) DEFAULT NULL,
  `EvaluationFromDate_2` date DEFAULT NULL,
  `EvaluationToDate_2` date DEFAULT NULL,
  `EvaluationDescription_2` varchar(255) DEFAULT NULL,
  `EvaluationTotalDay_3` int(11) DEFAULT NULL,
  `EvaluationTotalDaySufix_3` varchar(255) DEFAULT NULL,
  `EvaluationTotalDaySufix_2` varchar(255) DEFAULT NULL,
  `EvaluationFromDate_3` date DEFAULT NULL,
  `EvaluationToDate_3` date DEFAULT NULL,
  `EvaluationDescription_3` varchar(255) DEFAULT NULL,
  `EvaluationTotalDay_4` int(11) DEFAULT NULL,
  `EvaluationTotalDaySufix_4` varchar(255) DEFAULT NULL,
  `EvaluationFromDate_4` date DEFAULT NULL,
  `EvaluationToDate_4` date DEFAULT NULL,
  `EvaluationDescription_4` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_1` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_1` varchar(255) DEFAULT NULL,
  `ContractorFromDate_1` date DEFAULT NULL,
  `ContractorToDate_1` date DEFAULT NULL,
  `ContractorDescription_1` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_2` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_2` varchar(255) DEFAULT NULL,
  `ContractorFromDate_2` date DEFAULT NULL,
  `ContractorToDate_2` date DEFAULT NULL,
  `ContractorDescription_2` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_3` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_3` varchar(255) DEFAULT NULL,
  `ContractorFromDate_3` date DEFAULT NULL,
  `ContractorToDate_3` date DEFAULT NULL,
  `ContractorDescription_3` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_4` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_4` varchar(255) DEFAULT NULL,
  `ContractorFromDate_4` date DEFAULT NULL,
  `ContractorToDate_4` date DEFAULT NULL,
  `ContractorDescription_4` varchar(255) DEFAULT NULL,
  `ContractorTotalDay_5` int(11) DEFAULT NULL,
  `ContractorTotalDaySufix_5` varchar(255) DEFAULT NULL,
  `ContractorFromDate_5` date DEFAULT NULL,
  `ContractorToDate_5` date DEFAULT NULL,
  `ContractorDescription_5` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_1` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_1` varchar(255) DEFAULT NULL,
  `EntrrustedFromDate_1` date DEFAULT NULL,
  `EntrrustedToDate_1` date DEFAULT NULL,
  `EntrrustedDescription_1` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_2` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_2` varchar(255) DEFAULT NULL,
  `EntrrustedFromDate_2` date DEFAULT NULL,
  `EntrrustedToDate_2` date DEFAULT NULL,
  `EntrrustedDescription_2` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_3` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_3` varchar(255) DEFAULT NULL,
  `EntrrustedFromDate_3` date DEFAULT NULL,
  `EntrrustedToDate_3` date DEFAULT NULL,
  `EntrrustedDescription_3` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_4` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_4` varchar(255) DEFAULT NULL,
  `EntrrustedFromDate_4` date DEFAULT NULL,
  `EntrrustedToDate_4` date DEFAULT NULL,
  `EntrrustedDescription_4` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_5` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_5` varchar(255) DEFAULT NULL,
  `EntrrustedFromDate_5` date DEFAULT NULL,
  `EntrrustedToDate_5` date DEFAULT NULL,
  `EntrrustedDescription_5` varchar(255) DEFAULT NULL,
  `EntrrustedTotalDay_6` int(11) DEFAULT NULL,
  `EntrrustedTotalDaySufix_6` varchar(255) DEFAULT NULL,
  `EntrrustedFromDate_6` date DEFAULT NULL,
  `EntrrustedToDate_6` date DEFAULT NULL,
  `EntrrustedDescription_6` varchar(255) DEFAULT NULL,
  `PaymentTotalDay_1` int(11) DEFAULT NULL,
  `PaymentTotalDaySufix_1` varchar(255) DEFAULT NULL,
  `PaymentFromDate_1` date DEFAULT NULL,
  `PaymentToDate_1` date DEFAULT NULL,
  `PaymentDescription_1` varchar(255) DEFAULT NULL,
  `Payment_1` bigint(20) DEFAULT NULL,
  `Payment_2` bigint(20) DEFAULT NULL,
  `Payment_3` bigint(20) DEFAULT NULL,
  `Payment_4` bigint(20) DEFAULT NULL,
  `PaymentNextYear` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `procurement_goods_contruction_services`
--

INSERT INTO `procurement_goods_contruction_services` (`Id`, `MainProcurementId`, `PropertiesTotalDay_1`, `PropertiesTotalDaySufix_1`, `PropertiesFromDate_1`, `PropertiesToDate_1`, `PropertiesDescription_1`, `PropertiesTotalDay_2`, `PropertiesTotalDaySufix_2`, `PropertiesFromDate_2`, `PropertiesToDate_2`, `PropertiesDescription_2`, `PropertiesTotalDay_3`, `PropertiesTotalDaySufix_3`, `PropertiesFromDate_3`, `PropertiesToDate_3`, `PropertiesDescription_3`, `PropertiesTotalDay_4`, `PropertiesTotalDaySufix_4`, `PropertiesFromDate_4`, `PropertiesToDate_4`, `PropertiesDescription_4`, `BidTotalDay_1`, `BidTotalDaySufix_1`, `BidFromDate_1`, `BidToDate_1`, `BidDescription_1`, `BidTotalDay_2`, `BidTotalDaySufix_2`, `BidFromDate_2`, `BidToDate_2`, `BidDescription_2`, `BidTotalDay_3`, `BidTotalDaySufix_3`, `BidFromDate_3`, `BidToDate_3`, `BidDescription_3`, `BidTotalDay_4`, `BidTotalDaySufix_4`, `BidFromDate_4`, `BidToDate_4`, `BidDescription_4`, `AdvertiseTotalDay_1`, `AdvertiseTotalDaySufix_1`, `AdvertiseFromDate_1`, `AdvertiseToDate_1`, `AdvertiseDescription_1`, `EvaluationTotalDay_1`, `EvaluationTotalDaySufix_1`, `EvaluationFromDate_1`, `EvaluationToDate_1`, `EvaluationDescription_1`, `EvaluationTotalDay_2`, `EvaluationFromDate_2`, `EvaluationToDate_2`, `EvaluationDescription_2`, `EvaluationTotalDay_3`, `EvaluationTotalDaySufix_3`, `EvaluationTotalDaySufix_2`, `EvaluationFromDate_3`, `EvaluationToDate_3`, `EvaluationDescription_3`, `EvaluationTotalDay_4`, `EvaluationTotalDaySufix_4`, `EvaluationFromDate_4`, `EvaluationToDate_4`, `EvaluationDescription_4`, `ContractorTotalDay_1`, `ContractorTotalDaySufix_1`, `ContractorFromDate_1`, `ContractorToDate_1`, `ContractorDescription_1`, `ContractorTotalDay_2`, `ContractorTotalDaySufix_2`, `ContractorFromDate_2`, `ContractorToDate_2`, `ContractorDescription_2`, `ContractorTotalDay_3`, `ContractorTotalDaySufix_3`, `ContractorFromDate_3`, `ContractorToDate_3`, `ContractorDescription_3`, `ContractorTotalDay_4`, `ContractorTotalDaySufix_4`, `ContractorFromDate_4`, `ContractorToDate_4`, `ContractorDescription_4`, `ContractorTotalDay_5`, `ContractorTotalDaySufix_5`, `ContractorFromDate_5`, `ContractorToDate_5`, `ContractorDescription_5`, `EntrrustedTotalDay_1`, `EntrrustedTotalDaySufix_1`, `EntrrustedFromDate_1`, `EntrrustedToDate_1`, `EntrrustedDescription_1`, `EntrrustedTotalDay_2`, `EntrrustedTotalDaySufix_2`, `EntrrustedFromDate_2`, `EntrrustedToDate_2`, `EntrrustedDescription_2`, `EntrrustedTotalDay_3`, `EntrrustedTotalDaySufix_3`, `EntrrustedFromDate_3`, `EntrrustedToDate_3`, `EntrrustedDescription_3`, `EntrrustedTotalDay_4`, `EntrrustedTotalDaySufix_4`, `EntrrustedFromDate_4`, `EntrrustedToDate_4`, `EntrrustedDescription_4`, `EntrrustedTotalDay_5`, `EntrrustedTotalDaySufix_5`, `EntrrustedFromDate_5`, `EntrrustedToDate_5`, `EntrrustedDescription_5`, `EntrrustedTotalDay_6`, `EntrrustedTotalDaySufix_6`, `EntrrustedFromDate_6`, `EntrrustedToDate_6`, `EntrrustedDescription_6`, `PaymentTotalDay_1`, `PaymentTotalDaySufix_1`, `PaymentFromDate_1`, `PaymentToDate_1`, `PaymentDescription_1`, `Payment_1`, `Payment_2`, `Payment_3`, `Payment_4`, `PaymentNextYear`) VALUES
(48, 63, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 11, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-04-20', '2016-04-22', '', 3, 'ថ្ងៃ', '2016-04-23', '2016-04-25', '', 2, 'ថ្ងៃ', '2016-04-26', '2016-04-27', '', 5, 'ថ្ងៃ', '2016-04-28', '2016-05-02', '', 60, 'ថ្ងៃ', '2016-05-03', '2016-07-01', '', 5, 'ថ្ងៃ', '2016-07-02', '2016-07-06', '', 10, '2016-07-07', '2016-07-16', '', 3, 'ថ្ងៃ', 'ថ្ងៃ', '2016-07-17', '2016-07-19', '', 6, 'ថ្ងៃ', '2016-07-20', '2016-07-25', '', 1, 'ថ្ងៃ', '2016-07-26', '2016-07-26', '', 10, 'ថ្ងៃ', '2016-07-27', '2016-08-05', '', 3, 'ថ្ងៃ', '2016-08-06', '2016-08-08', '', 9, 'ថ្ងៃ', '2016-08-09', '2016-08-17', '', 5, 'ថ្ងៃ', '2016-08-18', '2016-08-22', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ខែ', '0000-00-00', '0000-00-00', '', 0, 'ខែ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 0, 0, 8080, 0),
(47, 62, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-03-27', '2016-03-29', '', 2, 'ថ្ងៃ', '2016-03-30', '2016-03-31', '', 2, 'ថ្ងៃ', '2016-04-01', '2016-04-02', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-04-03', '2016-05-02', '', 4, 'ថ្ងៃ', '2016-05-03', '2016-05-06', '', 10, '2016-05-07', '2016-05-16', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-05-17', '2016-05-18', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-05-19', '2016-05-19', '', 10, 'ថ្ងៃ', '2016-05-20', '2016-05-29', '', 2, 'ថ្ងៃ', '2016-05-30', '2016-05-31', '', 4, 'ថ្ងៃ', '2016-06-01', '2016-06-04', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-06-17', '2016-06-17', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 0, 500, 0, 0),
(46, 61, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-14', '2016-01-16', '', 2, 'ថ្ងៃ', '2016-01-17', '2016-01-18', '', 2, 'ថ្ងៃ', '2016-01-19', '2016-01-20', '', 5, 'ថ្ងៃ', '2016-01-21', '2016-01-25', '', 30, 'ថ្ងៃ', '2016-01-26', '2016-02-24', '', 4, 'ថ្ងៃ', '2016-02-25', '2016-02-28', '', 10, '2016-02-29', '2016-03-09', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-03-10', '2016-03-11', '', 5, 'ថ្ងៃ', '2016-03-12', '2016-03-16', '', 1, 'ថ្ងៃ', '2016-03-17', '2016-03-17', '', 10, 'ថ្ងៃ', '2016-03-18', '2016-03-27', '', 2, 'ថ្ងៃ', '2016-03-28', '2016-03-29', '', 4, 'ថ្ងៃ', '2016-03-30', '2016-04-02', '', 5, 'ថ្ងៃ', '2016-04-03', '2016-04-07', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-04-08', '2016-04-08', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 0, 0, 680, 0),
(28, 43, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 11, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-02-20', '2016-02-22', '', 3, 'ថ្ងៃ', '2016-02-23', '2016-02-25', '', 2, 'ថ្ងៃ', '2016-02-26', '2016-02-27', '', 5, 'ថ្ងៃ', '2016-02-28', '2016-03-03', '', 60, 'ថ្ងៃ', '2016-03-04', '2016-05-02', '', 5, 'ថ្ងៃ', '2016-05-06', '2016-05-10', '', 10, '2016-05-11', '2016-05-20', '', 3, 'ថ្ងៃ', 'ថ្ងៃ', '2016-05-21', '2016-05-23', '', 6, 'ថ្ងៃ', '2016-05-24', '2016-05-29', '', 1, 'ថ្ងៃ', '2016-05-30', '2016-05-30', '', 10, 'ថ្ងៃ', '2016-05-31', '2016-06-09', '', 3, 'ថ្ងៃ', '2016-06-10', '2016-06-12', '', 9, 'ថ្ងៃ', '2016-06-13', '2016-06-21', '', 5, 'ថ្ងៃ', '2016-06-22', '2016-06-26', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ខែ', '0000-00-00', '0000-00-00', '', 0, 'ខែ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 0, 7570, 0, 0),
(29, 44, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-05-04', '2016-05-06', '', 2, 'ថ្ងៃ', '2016-05-09', '2016-05-10', '', 2, 'ថ្ងៃ', '2016-05-10', '2016-05-11', '', 5, 'ថ្ងៃ', '2016-05-12', '2016-05-16', '', 30, 'ថ្ងៃ', '2016-05-17', '2016-06-15', '', 4, 'ថ្ងៃ', '2016-06-16', '2016-06-19', '', 10, '2016-06-20', '2016-06-29', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-06-30', '2016-07-01', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-07-02', '2016-07-02', '', 10, 'ថ្ងៃ', '2016-07-03', '2016-07-12', '', 2, 'ថ្ងៃ', '2016-07-13', '2016-07-14', '', 4, 'ថ្ងៃ', '2016-07-15', '2016-07-18', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-08-05', '2016-08-05', '', 1, 'លើក', '2016-08-05', '2016-08-05', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 0, 204, 0, 0),
(30, 45, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-12', '2016-01-14', '', 2, 'ថ្ងៃ', '2016-01-15', '2016-01-16', '', 2, 'ថ្ងៃ', '2016-01-17', '2016-01-18', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-01-22', '2016-02-20', '', 4, 'ថ្ងៃ', '2016-02-21', '2016-02-24', '', 10, '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-02-25', '2016-02-26', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-02-27', '2016-02-27', '', 10, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', '2016-02-28', '2016-02-29', '', 4, 'ថ្ងៃ', '2016-03-01', '2016-03-04', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'លើក', '2016-03-07', '2016-03-07', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 128, 0, 0, 0, 0),
(31, 46, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-12', '2016-01-14', '', 2, 'ថ្ងៃ', '2016-01-15', '2016-01-16', '', 2, 'ថ្ងៃ', '2016-01-17', '2016-01-18', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-01-19', '2016-02-17', '', 4, 'ថ្ងៃ', '2016-02-18', '2016-02-21', '', 10, '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-02-22', '2016-02-23', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-02-24', '2016-02-24', '', 10, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', '2016-02-25', '2016-02-26', '', 4, 'ថ្ងៃ', '2016-02-27', '2016-03-01', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'លើក', '2016-04-03', '2016-04-03', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 245, 0, 0, 0, 0),
(32, 47, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-07-12', '2016-07-14', '', 2, 'ថ្ងៃ', '2016-07-15', '2016-07-16', '', 2, 'ថ្ងៃ', '2016-07-17', '2016-07-18', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-07-19', '2016-08-17', '', 4, 'ថ្ងៃ', '2016-08-18', '2016-08-21', '', 10, '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-08-22', '2016-08-23', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-08-24', '2016-08-24', '', 10, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', '2016-08-25', '2016-08-26', '', 4, 'ថ្ងៃ', '2016-08-27', '2016-08-30', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'លើក', '0000-00-00', '0000-00-00', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 0, 0, 100, 0),
(33, 48, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2015-12-27', '2015-12-29', '', 2, 'ថ្ងៃ', '2015-12-30', '2015-12-31', '', 2, 'ថ្ងៃ', '2016-01-01', '2016-01-02', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-01-03', '2016-02-01', '', 4, 'ថ្ងៃ', '2016-02-02', '2016-02-05', '', 10, '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-02-06', '2016-02-07', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-02-08', '2016-02-08', '', 10, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', '2016-02-09', '2016-02-10', '', 4, 'ថ្ងៃ', '2016-02-11', '2016-02-14', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'លើក', '2016-03-01', '2016-03-01', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 425, 0, 0, 0, 0),
(34, 49, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-22', '2016-01-24', '', 2, 'ថ្ងៃ', '2016-01-25', '2016-01-26', '', 2, 'ថ្ងៃ', '2016-01-27', '2016-01-28', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-01-29', '2016-02-27', '', 4, 'ថ្ងៃ', '2016-02-28', '2016-03-02', '', 10, '2016-03-03', '2016-03-12', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-03-13', '2016-03-14', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-03-15', '2016-03-15', '', 10, 'ថ្ងៃ', '2016-03-16', '2016-03-25', '', 2, 'ថ្ងៃ', '2016-03-26', '2016-03-27', '', 4, 'ថ្ងៃ', '2016-03-28', '2016-03-31', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-04-01', '2016-04-30', '', 1, 'លើក', '2016-05-01', '2016-05-01', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 45, '', '2016-05-01', '2016-06-14', '', 150, 0, 0, 0, 0),
(35, 50, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-29', '2016-01-31', '', 2, 'ថ្ងៃ', '2016-02-01', '2016-02-02', '', 2, 'ថ្ងៃ', '2016-02-03', '2016-02-04', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-02-05', '2016-03-05', '', 4, 'ថ្ងៃ', '2016-03-06', '2016-03-09', '', 10, '2016-03-10', '2016-03-19', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-03-20', '2016-03-21', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-03-22', '2016-03-22', '', 10, 'ថ្ងៃ', '2016-03-23', '2016-04-01', '', 2, 'ថ្ងៃ', '2016-04-02', '2016-04-03', '', 4, 'ថ្ងៃ', '2016-04-04', '2016-04-07', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-04-07', '2016-05-06', '', 1, 'ថ្ងៃ', '2016-04-29', '2016-04-29', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 480, 0, 0, 0),
(36, 51, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-03-27', '2016-03-29', '', 2, 'ថ្ងៃ', '2016-03-30', '2016-03-31', '', 2, 'ថ្ងៃ', '2016-04-01', '2016-04-02', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-04-03', '2016-05-02', '', 4, 'ថ្ងៃ', '2016-05-03', '2016-05-06', '', 10, '2016-05-07', '2016-05-16', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-05-17', '2016-05-18', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-05-19', '2016-05-19', '', 10, 'ថ្ងៃ', '2016-05-20', '2016-05-29', '', 2, 'ថ្ងៃ', '2016-05-30', '2016-05-31', '', 4, 'ថ្ងៃ', '2016-06-01', '2016-06-04', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-06-17', '2016-06-17', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 0, 460, 0, 0),
(37, 52, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 11, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-04-20', '2016-04-22', '', 3, 'ថ្ងៃ', '2016-04-23', '2016-04-25', '', 2, 'ថ្ងៃ', '2016-04-26', '2016-04-27', '', 5, 'ថ្ងៃ', '2016-04-28', '2016-05-02', '', 60, 'ថ្ងៃ', '2016-05-03', '2016-07-01', '', 5, 'ថ្ងៃ', '2016-07-02', '2016-07-06', '', 10, '2016-07-07', '2016-07-16', '', 3, 'ថ្ងៃ', 'ថ្ងៃ', '2016-07-17', '2016-07-19', '', 6, 'ថ្ងៃ', '2016-07-20', '2016-07-25', '', 1, 'ថ្ងៃ', '2016-07-26', '2016-07-26', '', 10, 'ថ្ងៃ', '2016-07-27', '2016-08-05', '', 3, 'ថ្ងៃ', '2016-08-06', '2016-08-08', '', 9, 'ថ្ងៃ', '2016-08-09', '2016-08-17', '', 5, 'ថ្ងៃ', '2016-08-18', '2016-08-22', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ខែ', '0000-00-00', '0000-00-00', '', 0, 'ខែ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 0, 0, 8000, 0),
(38, 53, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 11, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-08-01', '2016-08-03', '', 3, 'ថ្ងៃ', '2016-08-04', '2016-08-06', '', 2, 'ថ្ងៃ', '2016-08-07', '2016-08-08', '', 5, 'ថ្ងៃ', '2016-08-09', '2016-08-13', '', 60, 'ថ្ងៃ', '2016-08-14', '2016-10-12', '', 5, 'ថ្ងៃ', '2016-10-13', '2016-10-17', '', 10, '2016-10-18', '2016-10-27', '', 3, 'ថ្ងៃ', 'ថ្ងៃ', '2016-10-28', '2016-10-30', '', 6, 'ថ្ងៃ', '2016-10-31', '2016-11-05', '', 1, 'ថ្ងៃ', '2016-11-06', '2016-11-06', '', 10, 'ថ្ងៃ', '2016-11-07', '2016-11-16', '', 3, 'ថ្ងៃ', '2016-11-17', '2016-11-19', '', 9, 'ថ្ងៃ', '2016-11-20', '2016-11-28', '', 5, 'ថ្ងៃ', '2016-11-29', '2016-12-03', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ខែ', '0000-00-00', '0000-00-00', '', 0, 'ខែ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 0, 0, 18500, 0),
(39, 54, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-14', '2016-01-16', '', 2, 'ថ្ងៃ', '2016-01-17', '2016-01-18', '', 2, 'ថ្ងៃ', '2016-01-19', '2016-01-20', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-01-21', '2016-02-19', '', 4, 'ថ្ងៃ', '2016-02-20', '2016-02-23', '', 10, '2016-02-24', '2016-03-04', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-03-05', '2016-03-06', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-03-07', '2016-03-07', '', 10, 'ថ្ងៃ', '2016-03-08', '2016-03-17', '', 2, 'ថ្ងៃ', '2016-03-18', '2016-03-19', '', 4, 'ថ្ងៃ', '2016-03-20', '2016-03-23', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 200, 0, 0, 0, 0),
(40, 55, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-14', '2016-01-16', '', 2, 'ថ្ងៃ', '2016-01-17', '2016-01-18', '', 2, 'ថ្ងៃ', '2016-01-19', '2016-01-20', '', 5, 'ថ្ងៃ', '2016-01-21', '2016-01-25', '', 30, 'ថ្ងៃ', '2016-01-26', '2016-02-24', '', 4, 'ថ្ងៃ', '2016-02-25', '2016-02-28', '', 10, '2016-02-29', '2016-03-09', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-03-10', '2016-03-11', '', 5, 'ថ្ងៃ', '2016-03-12', '2016-03-16', '', 1, 'ថ្ងៃ', '2016-03-17', '2016-03-17', '', 10, 'ថ្ងៃ', '2016-03-18', '2016-03-27', '', 2, 'ថ្ងៃ', '2016-03-28', '2016-03-29', '', 4, 'ថ្ងៃ', '2016-03-30', '2016-04-02', '', 5, 'ថ្ងៃ', '2016-04-03', '2016-04-07', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 600, 0, 0, 0),
(41, 56, 3, 'ថ្ងៃ', '2016-01-14', '2016-01-16', '', 2, 'ថ្ងៃ', '2016-01-17', '2016-01-18', '', 2, 'ថ្ងៃ', '2016-01-19', '2016-01-20', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 15, 'ថ្ងៃ', '2016-01-21', '2016-02-04', '', 4, 'ថ្ងៃ', '2016-02-05', '2016-02-08', '', 10, '2016-02-09', '2016-02-18', '', 0, '', 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 2, 'ថ្ងៃ', '2016-02-19', '2016-02-20', '', 1, 'ថ្ងៃ', '2016-02-21', '2016-02-21', '', 10, 'ថ្ងៃ', '2016-02-22', '2016-03-02', '', 2, 'ថ្ងៃ', '2016-03-03', '2016-03-04', '', 1, 'ថ្ងៃ', '2016-03-05', '2016-03-05', '', 0, '', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-03-05', '2016-03-05', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ខែ', '0000-00-00', '0000-00-00', '', 45, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 50, 0, 0, 0, 0),
(42, 57, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-04-21', '2016-04-23', '', 2, 'ថ្ងៃ', '2016-04-24', '2016-04-25', '', 2, 'ថ្ងៃ', '2016-04-26', '2016-04-27', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-04-28', '2016-05-27', '', 4, 'ថ្ងៃ', '2016-05-28', '2016-05-31', '', 10, '2016-06-01', '2016-06-10', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-06-11', '2016-06-12', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-06-13', '2016-06-13', '', 10, 'ថ្ងៃ', '2016-06-14', '2016-06-23', '', 2, 'ថ្ងៃ', '2016-06-24', '2016-06-25', '', 4, 'ថ្ងៃ', '2016-06-26', '2016-06-29', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 0, 150, 0, 0),
(43, 58, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-08-11', '2016-08-13', '', 2, 'ថ្ងៃ', '2016-08-14', '2016-08-15', '', 2, 'ថ្ងៃ', '2016-08-16', '2016-08-17', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-08-18', '2016-09-16', '', 4, 'ថ្ងៃ', '2016-09-17', '2016-09-20', '', 10, '2016-09-21', '2016-09-30', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-10-01', '2016-10-02', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '2016-10-03', '2016-10-03', '', 10, 'ថ្ងៃ', '2016-10-04', '2016-10-13', '', 2, 'ថ្ងៃ', '2016-10-14', '2016-10-15', '', 4, 'ថ្ងៃ', '2016-10-16', '2016-10-19', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 0, 0, 200, 0),
(44, 59, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-04', '2016-01-06', '', 2, 'ថ្ងៃ', '2016-01-07', '2016-01-08', '', 2, 'ថ្ងៃ', '2016-01-09', '2016-01-10', '', 5, 'ថ្ងៃ', '2016-01-11', '2016-01-15', '', 30, 'ថ្ងៃ', '2016-01-16', '2016-02-14', '', 4, 'ថ្ងៃ', '2016-02-15', '2016-02-18', '', 10, '2016-02-19', '2016-02-28', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-02-29', '2016-03-01', '', 5, 'ថ្ងៃ', '2016-03-02', '2016-03-06', '', 1, 'ថ្ងៃ', '2016-03-07', '2016-03-07', '', 10, 'ថ្ងៃ', '2016-03-08', '2016-03-17', '', 2, 'ថ្ងៃ', '2016-03-18', '2016-03-19', '', 4, 'ថ្ងៃ', '2016-03-20', '2016-03-23', '', 5, 'ថ្ងៃ', '2016-03-24', '2016-03-28', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 30, 'ថ្ងៃ', '2016-03-24', '2016-04-22', '', 30, 'ថ្ងៃ', '2016-04-23', '2016-05-22', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 501, 501, 0, 0),
(45, 60, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-04', '2016-01-06', '', 2, 'ថ្ងៃ', '2016-01-07', '2016-01-08', '', 2, 'ថ្ងៃ', '2016-01-09', '2016-01-10', '', 5, 'ថ្ងៃ', '2016-01-11', '2016-01-15', '', 30, 'ថ្ងៃ', '2016-01-16', '2016-02-14', '', 4, 'ថ្ងៃ', '2016-02-15', '2016-02-18', '', 10, '2016-02-19', '2016-02-28', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-02-29', '2016-03-01', '', 5, 'ថ្ងៃ', '2016-03-02', '2016-03-06', '', 1, 'ថ្ងៃ', '2016-03-07', '2016-03-07', '', 10, 'ថ្ងៃ', '2016-03-08', '2016-03-17', '', 2, 'ថ្ងៃ', '2016-03-18', '2016-03-19', '', 4, 'ថ្ងៃ', '2016-03-20', '2016-03-23', '', 5, 'ថ្ងៃ', '2016-03-24', '2016-03-28', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 60, 'ថ្ងៃ', '2016-03-24', '2016-05-22', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 2856, 0, 0, 0),
(27, 42, 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 21, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 7, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 3, 'ថ្ងៃ', '2016-01-25', '2016-01-27', '', 2, 'ថ្ងៃ', '2016-01-28', '2016-01-29', '', 2, 'ថ្ងៃ', '2016-01-30', '2016-01-31', '', 5, 'ថ្ងៃ', '2016-02-01', '2016-02-05', '', 30, 'ថ្ងៃ', '2016-02-06', '2016-03-06', '', 4, 'ថ្ងៃ', '2016-03-07', '2016-03-10', '', 10, '2016-03-11', '2016-03-20', '', 2, 'ថ្ងៃ', 'ថ្ងៃ', '2016-03-21', '2016-03-22', '', 5, 'ថ្ងៃ', '2016-03-23', '2016-03-27', '', 1, 'ថ្ងៃ', '2016-03-28', '2016-03-28', '', 10, 'ថ្ងៃ', '2016-03-29', '2016-04-07', '', 2, 'ថ្ងៃ', '2016-04-08', '2016-04-09', '', 4, 'ថ្ងៃ', '2016-04-10', '2016-04-13', '', 5, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 1, 'ថ្ងៃ', '0000-00-00', '0000-00-00', '', 0, 'លើក', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, '', '0000-00-00', '0000-00-00', '', 0, 1080, 0, 0, 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `procurement_institution_view`
--
CREATE TABLE IF NOT EXISTS `procurement_institution_view` (
`Id` int(11)
,`Name` varchar(300)
,`IsActive` int(11)
,`Status` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `procurement_method`
--

CREATE TABLE IF NOT EXISTS `procurement_method` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `ABBR` varchar(50) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `OrderNumber` int(11) DEFAULT NULL,
  `Type` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `procurement_method`
--

INSERT INTO `procurement_method` (`Id`, `Name`, `IsActive`, `ABBR`, `parent_id`, `OrderNumber`, `Type`) VALUES
(1, 'ទំនិញ សំណង់ សេវាកម្ម', 1, 'ទំនិញ សំណង់ សេវាកម្ម', 0, 1, 1),
(2, 'សេវាទីប្រឹក្សា (ថវិកាដៃគូអភិវឌ្ឍន៏)', 1, 'សេវាទីប្រឹក្សា (ថវិកាដៃគូអភិវឌ្ឍន៏)', 0, 2, 2),
(3, 'ដេញថ្លៃប្រកួតប្រជែងក្នុងស្រុក', 1, 'ដបស', 1, 1, 1),
(4, 'ដេញថ្លៃប្រកួតប្រជែងជាលក្ខណះអន្តរជាតិ', 1, 'ដបអ', 1, 2, 1),
(5, 'ពីគ្រោះថ្លៃ', 1, 'ពថ', 1, 3, 1),
(6, 'ស្ទង់តម្លៃ', 1, 'សត', 1, 4, 1),
(7, 'ដោយឡែក', 1, 'លឡ', 1, 5, 1),
(8, 'ជាតិ', 1, 'ជាតិ', 2, 1, 2),
(9, 'អន្តរជាតិ', 1, 'អន្តរជាតិ', 2, 2, 2),
(10, 'សេវាទីប្រឹក្សា (ថវិការដ្ឋ)', 1, 'សេវាទីប្រឹក្សា (ថវិការដ្ឋ)', 0, 3, 3),
(11, 'ការជ្រើសរើសផ្អែកលើថ្លៃទាបបំផុត', 1, 'ជផថទ', 10, 1, 3),
(15, 'ការជ្រើសរើសផ្អែកលើថវិការកំណត់', 1, 'ជផថក', 10, 7, 3),
(12, 'ការជ្រើសរើសផ្អែកលើគុណភាព', 1, 'ជផគ', 10, 2, 3),
(13, 'ការជ្រើសរើសផ្អែកលើលក្ខណសម្បត្តិ', 1, 'ជផល', 10, 3, 3),
(14, 'ការជ្រើសរើសផ្អែកលើគុណភាព និងថ្លៃជាអន្តរេជាតិ', 1, 'ជផគតអ', 10, 4, 3),
(16, 'ការជ្រើសរើសផ្អែកលើការចរចាផ្ទាល់', 1, 'ជផច', 10, 6, 3),
(17, 'ការជ្រើសរើសផ្អែកលើគុណភាព និងថ្លៃក្នងស្រុក', 1, 'ជផគតស', 10, 5, 3),
(18, 'ដក', 1, 'ដក', 1, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_report_header_text`
--

CREATE TABLE IF NOT EXISTS `procurement_report_header_text` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(500) NOT NULL,
  `IsActive` int(11) NOT NULL,
  `OrderNumber` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `procurement_report_header_text`
--

INSERT INTO `procurement_report_header_text` (`Id`, `Name`, `IsActive`, `OrderNumber`) VALUES
(1, 'តារាងសរុបផែនការលទ្ធកម្ម របស់ក្រសួងសាធារណៈការ និងដឹកជញ្ជូន', 1, 1),
(2, 'តារាងសរុបផែនការលទ្ធកម្ម ទំនិញ របស់ក្រសួង-ស្ថាប័នថ្នាក់កណ្ដាល', 1, 2),
(3, 'តារាងសរុបផែនការលទ្ធកម្ម សំណង់ របស់ក្រសួង-ស្ថាប័នថ្នាក់កណ្ដាល', 1, 3),
(4, 'តារាងសរុបផែនការលទ្ធកម្ម សេវាកម្ម របស់ក្រសួង-ស្ថាប័នថ្នាក់កណ្ដាល', 1, 4),
(5, 'តារាងសរុបផែនការលទ្ធកម្ម ទំនិញ សំណង់ និងសេវា របស់ក្រសួងស្ថាប័ន-ថ្នាក់កណ្តាល', 1, 5),
(6, 'តារាងសរុបផែនការលទ្ធកម្ម ទំនិញ របស់រដ្ឋបាលរាធានី-ខេត្ត', 1, 6),
(7, 'តារាងសរុបផែនការលទ្ធកម្ម សំណង់ របស់រដ្ឋបាលរាធានី-ខេត្ត', 1, 7),
(8, 'តារាងសរុបផែនការលទ្ធកម្ម សេវាកម្ម របស់រដ្ឋបាលរាធានី-ខេត្ត', 1, 8),
(9, 'តារាងសរុបផែនការលទ្ធកម្ម ទំនិញ សំណង់ និងសេវាកម្ម របស់រដ្ឋបាលរាធានី-ខេត្ត', 1, 9),
(10, 'តារាងសរុបផែនការលទ្ធកម្ម ទំនិញ របស់សហគ្រាសសាធារណៈ-គ្រឹះស្ថានសាធារណៈ', 1, 10),
(11, 'តារាងសរុបផែនការលទ្ធកម្ម សំណង់ របស់សហគ្រាសសាធារណៈ-គ្រឹះស្ថានសាធារណៈ', 1, 11),
(12, 'តារាងសរុបផែនការលទ្ធកម្ម សេវាកម្ម របស់សហគ្រាសសាធារណៈ-គ្រឹះស្ថានសាធារណៈ', 1, 12),
(13, 'តារាងសរុបផែនការលទ្ធកម្ម ទំនិញ សំណង់ និងសេវាកម្ម របស់សហគ្រាសសាធារណៈ-គ្រឹះស្ថានសាធារណៈ', 1, 13),
(14, 'តារាងសរុបផែនការលទ្ធកម្ម ទំនិញ (ទូទាំងប្រទេស)\n', 1, 14),
(15, 'តារាងសរុបផែនការលទ្ធកម្ម សំណង់ (ទូទាំងប្រទេស)\n', 1, 15),
(16, 'តារាងសរុបផែនការលទ្ធកម្ម សេវាកម្ម (ទូទាំងប្រទេស)\n', 1, 16),
(17, 'តារាងសរុបផែនការលទ្ធកម្ម ទំនិញ សំណង់ និងសេវាកម្ម (ទូទាំងប្រទេស)\n', 1, 17),
(18, 'តារាងសរុបផែនការ លទ្ធកម្មកែតម្រូវ របស់...(ឈ្មោះស្ថាប័នអនុវត្តលទ្ធកម្ម)...', 1, 18),
(19, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មទំនិញ របស់ស្ថាប័នថ្នាក់កណ្តាល', 1, 19),
(20, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសំណង់ របស់ស្ថាប័នថ្នាក់កណ្តាល', 1, 20),
(21, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេវាកម្ម របស់ស្ថាប័នថ្នាក់កណ្តាល', 1, 21),
(22, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មទំនិញ សំណង់ សេវាកម្ម របស់ស្ថាប័នថ្នាក់កណ្តាល', 1, 22),
(23, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មទំនិញ របស់រដ្ឋបាលរាជធានី-ខេត្ត', 1, 23),
(24, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសំណង់ របស់រដ្ឋបាលរាជធានី-ខេត្ត', 1, 24),
(25, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេវាកម្ម របស់រដ្ឋបាលរាជធានី-ខេត្ត', 1, 25),
(26, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មទំនិញ សំណង់ សេវាកម្ម របស់រដ្ឋបាលរាជធានី-ខេត្ត', 1, 26),
(27, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មទំនិញ របស់សហគ្រាសសាធារណៈ-គ្រឹះស្ថានសាធារណៈ', 1, 27),
(28, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសំណង់ របស់សហគ្រាសសាធារណៈ-គ្រឹះស្ថានសាធារណៈ', 1, 28),
(29, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេវាកម្ម របស់សហគ្រាសសាធារណៈ-គ្រឹះស្ថានសាធារណៈ', 1, 29),
(30, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មទំនិញ សំណង់ សេវាកម្ម របស់សហគ្រាសសាធារណៈ-គ្រឹះស្ថានសាធារណៈ', 1, 30),
(31, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មទំនិញ (ទូទាំងប្រទេស)', 1, 31),
(32, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសំណង់ (ទូទាំងប្រទេស)', 1, 32),
(33, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេវាកម្ម (ទូទាំងប្រទេស)', 1, 33),
(34, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មទំនិញ សំណង់ សេវាកម្ម (ទូទាំងប្រទេស)', 1, 34),
(35, 'តារាងសរុប ផែនការលទ្ទកម្ម សេវាប្រឹក្សា របស់...(ឈ្មោះស្ថាប័នអនុវត្តលទ្ទកម្ម)...', 1, 35),
(36, 'តារាងសរុប ផែនការលទ្ធកម្ម សេរាទីប្រឹក្សា របស់...(ឈ្មោះស្ថាប័នដែលអនុវត្តថវិកាកម្មវិធី)...', 1, 36),
(37, 'តារាងសរុប ផែនការលទ្ធកម្ម សេរាទីប្រឹក្សា របស់...(ឈ្មោះអង្គភាពថវិកា) នៃ..(ឈ្មោះស្ថាប័នអនុរត្តលទ្ធកម្ម)...', 1, 37),
(38, 'តារាងសរុប ផែនការលទ្ធកម្មសេរាទីប្រឹក្សា របស់ស្ថាប័នថ្នាក់កណ្តាល', 1, 38),
(39, 'តារាងសរុប ផែនការលទ្ធកម្មសេរាទីប្រឹក្សា របស់រដ្ឋបាលរាជធានី-ខេត្ត', 1, 39),
(40, 'តារាងសរុប ផែនការលទ្ធកម្មសេរាទីប្រឹក្សា របស់គ្រឹះស្ថាន-សហគ្រាសសាធារណៈ', 1, 40),
(41, 'តារាងសរុប ផែនការលទ្ធកម្មសេរាទីប្រឹក្សា ទូទាំងប្រទេស', 1, 41),
(42, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេរាទីប្រឹក្សា របស់ .....(ឈ្មោះស្ថាប័នអនុវត្តលទ្ធកម្ម)...', 1, 42),
(43, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេរាទីប្រឹក្សា របស់ .....(ឈ្មោះអង្គភាពថវិកា)....នៃក្រសួង ....(ឈ្មោះស្ថាប័នអនុវត្តថវិកាកម្មវិធី)...', 1, 43),
(44, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេរាទីប្រឹក្សា របស់ .....(ឈ្មោះស្ថាប័នដែលអនុវត្តថវិកាកម្មវិធី)......', 1, 44),
(45, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេរាទីប្រឹក្សា របស់ស្ថាប័នថ្នាក់កណ្តាល', 1, 45),
(46, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេរាទីប្រឹក្សា របស់រដ្ឋបាលរាជធានី-ខេត្ត', 1, 46),
(47, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេរាទីប្រឹក្សា  របស់គ្រឹះស្ថាន-សហគ្រាសសាធារណៈ', 1, 47),
(48, 'តារាងសរុប ផែនការកែតម្រូវ លទ្ធកម្មសេរាទីប្រឹក្សា ទូទាំងប្រទេស', 1, 48),
(49, 'តារាងសរុប ផែនការលទ្ធកម្ម របស់...(ឈ្មោះអង្គភាពថវិកា)...នៃក្រសួង...(ឈ្មោះស្ថាប័នថវិកាកម្មវិធី)...', 1, 49),
(50, 'តារាងសរុប ផែនការលទ្ធកម្ម របស់....(ឈ្មោះស្ថាប័នថវិកាកម្មវិធី)...', 1, 50),
(51, 'តារាងសរុប ផែនការលទ្ធកម្ម ទំនិញ របស់...(ស្ថាប័នអនុវត្តថវិកាកម្មវិធី)...', 1, 51),
(52, 'តារាងសរុប ផែនការលទ្ធកម្ម សំណង់ របស់...(ស្ថាប័នអនុវត្តថវិកាកម្មវិធី)...', 1, 52),
(53, 'តារាងសរុប ផែនការលទ្ធកម្ម សេវាកម្ម របស់...(ស្ថាប័នអនុវត្តថវិកាកម្មវិធី)...', 1, 53),
(54, 'តារាងសរុបរួមផែនការលទ្ធកម្មទំនិញ សំណង់ និងសេវាកម្មរបស់....(ឈ្មោះស្ថាប័នថវិកាកម្មវិធី)...', 1, 54),
(55, 'តារាងសរុប ផែនការលទ្ធកម្មទំនិញ សំណង់ និងសេវាកម្ម របស់ស្ថាប័នអនុវត្តថវិកាកម្មវិធី', 1, 55),
(56, 'តារាងសរុប ផែនការលទ្ធកម្ម របស់....(ឈ្មោះស្ថាប័នអនុវត្តថវិកាកម្មវិធី)...', 1, 56),
(57, 'តារាងសរុប ផែនការលទ្ធកម្ម របស់...(ឈ្មោះអង្គភាពថវិកា)...នៃក្រសួង...(ឈ្មោះស្ថាប័នថវិកាកម្មវិធី)...', 1, 57),
(58, 'តារាងសរុប ផែនការលទ្ធកម្ម ទំនិញ របស់...(ស្ថាប័នអនុវត្តថវិកាកម្មវិធី)...', 1, 58),
(59, 'តារាងសរុប ផែនការលទ្ធកម្ម សំណង់ របស់...(ស្ថាប័នអនុវត្តថវិកាកម្មវិធី)...', 1, 59),
(60, 'តារាងសរុប ផែនការលទ្ធកម្ម សេវាកម្ម របស់...(ស្ថាប័នអនុវត្តថវិកាកម្មវិធី)...', 1, 60),
(61, 'តារាងសរុបរួមផែនការលទ្ធកម្មទំនិញ សំណង់ និងសេវាកម្មរបស់....(ឈ្មោះស្ថាប័នថវិកាកម្មវិធី)...', 1, 61),
(62, 'តារាងសរុប ផែនការលទ្ធកម្មទំនិញ សំណង់ និងសេវាកម្ម របស់ស្ថាប័នអនុវត្តថវិកាកម្មវិធី', 1, 62);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_type`
--

CREATE TABLE IF NOT EXISTS `procurement_type` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `ABBR` varchar(50) DEFAULT NULL,
  `OrderNumber` int(11) NOT NULL,
  `Type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `procurement_type`
--

INSERT INTO `procurement_type` (`Id`, `Name`, `IsActive`, `ABBR`, `OrderNumber`, `Type`) VALUES
(1, 'ទំនិញ', 1, 'ទំនិញ', 1, 1),
(2, 'សំណង់', 1, 'សំណង់', 2, 1),
(3, 'សេវាកម្ម', 1, 'សេវាកម្ម', 3, 1),
(4, 'សេវាទីប្រឹក្សា', 1, 'សេវាទីប្រឹក្សា', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `province_status`
--

CREATE TABLE IF NOT EXISTS `province_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `central_ministry_agency_id` int(11) DEFAULT NULL,
  `province_status` int(11) DEFAULT NULL,
  `sub_category_central_ministry_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `province_status`
--

INSERT INTO `province_status` (`id`, `central_ministry_agency_id`, `province_status`, `sub_category_central_ministry_id`) VALUES
(1, 1, 1, 0),
(2, 2, 0, 0),
(20, 25, 0, 0),
(22, 27, 0, 0),
(23, 28, 0, 0),
(24, 29, 0, 0),
(25, 30, 0, 0),
(26, 31, 0, 0),
(66, 32, 1, 2),
(28, 33, 0, 0),
(29, 34, 0, 0),
(30, 35, 0, 0),
(19, 24, 0, 0),
(21, 26, 0, 0),
(31, 36, 0, 0),
(32, 37, 0, 0),
(33, 38, 0, 0),
(64, 39, 0, 1),
(35, 40, 0, 0),
(36, 41, 0, 0),
(37, 42, 0, 0),
(38, 43, 0, 0),
(39, 44, 0, 0),
(40, 45, 0, 0),
(41, 46, 0, 0),
(42, 47, 0, 0),
(43, 48, 0, 0),
(44, 49, 0, 0),
(45, 50, 0, 0),
(46, 51, 1, 0),
(47, 52, 1, 0),
(48, 53, 1, 0),
(49, 54, 1, 0),
(50, 55, 1, 0),
(51, 56, 1, 0),
(52, 57, 1, 0),
(53, 58, 1, 0),
(54, 59, 1, 0),
(55, 60, 1, 0),
(56, 61, 1, 0),
(65, 62, 1, 1),
(58, 63, 1, 0),
(59, 64, 1, 0),
(60, 65, 1, 0),
(61, 66, 1, 0),
(62, 67, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `public_institutions_enterprice`
--

CREATE TABLE IF NOT EXISTS `public_institutions_enterprice` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `public_institutions_enterprice`
--

INSERT INTO `public_institutions_enterprice` (`Id`, `Name`, `IsActive`, `Status`) VALUES
(17, 'មជ្ឈមណ្ឌលពិសោធន៍​សុខាភិបាល​', 1, 2),
(12, 'ទូរគមនាគមន៍​កម្ពុជា​', 1, 2),
(13, 'វិទ្យាស្ថានស្រាវជ្រាវនិងអភិវឌ្ឍន៍​កសិកម្មកម្ពុជា​', 1, 2),
(14, 'ប្រៃសណីយ៍​កម្ពុជា​', 1, 2),
(15, 'កំពង់ផែស្វយ័តក្រុងភ្នំពេញ​', 1, 2),
(16, 'អគ្គិសនី​កម្ពុជា​', 1, 2),
(18, 'សាកលវិទ្យាល័យជាតិ​គ្រប់គ្រង​', 1, 2),
(19, 'វិទ្យាស្ថានស្រាវជ្រាវកៅស៊ូ​កម្ពុជា​', 1, 2),
(20, 'បេឡា​ជាតិ​របបសន្តិសុខសង្គមសម្រាប់​មន្រ្តី​រាជការស៊ីវិល​', 1, 2),
(21, 'វិទ្យាស្ថានជាតិ​សុខភាពសាធារណៈ​', 1, 2),
(22, 'សាកលវិទ្យាល័យភូមិន្ទនីតិសាស្រ្ត​និងវិទ្យាសាស្រ្តសេដ្ឋកិច្ច​', 1, 2),
(23, 'សាកលវិទ្យាល័យភូមិន្ទកសិកម្ម​', 1, 2),
(24, 'បេឡាជាតិ​អតីតយុទ្ធជន​', 1, 2),
(25, 'សាកលវិទ្យាល័យវិទ្យាសាស្រ្តសុខាភិបាល​', 1, 2),
(26, 'សាកលវិទ្យាល័យភូមិន្ទវិចិត្រសិល្បៈ​', 1, 2),
(27, 'មូលនិធិ​ជនពិការ​', 1, 2),
(28, 'រដ្ឋាករទឹកស្វយ័តក្រុងភ្នំពេញ​', 1, 2),
(29, 'កំពង់ផែស្វយ័តក្រុងព្រះសីហនុ​', 1, 2),
(30, 'សាលាជាតិ​កសិកម្មកំពង់ចាម​', 1, 2),
(31, 'បេឡាជាតិ​របបសន្តិសុខសង្គម​', 1, 2),
(32, 'រដ្ឋាករទឹកស្វយ័តសៀមរាប​', 1, 2),
(33, 'គ្រឹះស្ថានបោះពុម្ពនិងចែកផ្សាយ​', 1, 2),
(34, 'ភ្នាក់ងារកម្ពុជានាវាចរណ៍​', 1, 2),
(35, 'ក្រុមហ៊ុនហ្រ្គិនត្រេដ​', 1, 2),
(36, 'មន្ទីរពេទ្យកុមារជាតិ​', 1, 2),
(37, 'មន្ទីរពេទ្យព្រះកុសុមៈ​', 1, 2),
(38, 'សាលាជាតិ​កសិកម្មព្រែកលៀប​', 1, 2),
(39, 'វិទ្យាស្ថានជាតិ​ពហុ​បច្ចេកទេសកម្ពុជា​', 1, 2),
(40, 'មន្ទីរពេទ្យមិត្តភាពខ្មែរ-សូវៀត​', 1, 2),
(41, 'មន្ទីរពេទ្យកាល់ម៉ែត្រ​', 1, 2),
(42, 'មន្ទីរពេទ្យព្រះអង្គឌួង​', 1, 2),
(43, 'ធនាគារអភិវឌ្ឍន៍​ជនបទ​', 1, 2),
(44, 'បញ្ញត្តិករទូរគមនាគមន៍កម្ពុជា​', 1, 2),
(45, 'អាជ្ញាធរអប្សរា', 1, 2),
(46, 'អាជ្ញាធរអគ្គីសនី', 1, 2),
(47, 'មន្ទីរពិសោធន៏សំណង់', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category_central_ministry`
--

CREATE TABLE IF NOT EXISTS `sub_category_central_ministry` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `Active` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sub_category_central_ministry`
--

INSERT INTO `sub_category_central_ministry` (`Id`, `name`, `Active`) VALUES
(1, 'អង្គភាពថវិកាថ្នាក់កណ្ដាលនៃស្ថាប័នអនុវត្តលទ្ធកម្មថ្នាក់ជាតិ', 1),
(2, 'អង្គភាពថវិកាថ្នាក់មូលដ្ឋាននៃស្ថាប័នអនុវត្តលទ្ធកម្មថ្នាក់ជាតិ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `type_of_days`
--

CREATE TABLE IF NOT EXISTS `type_of_days` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `OrderNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `type_of_days`
--

INSERT INTO `type_of_days` (`Id`, `Name`, `IsActive`, `OrderNumber`) VALUES
(4, 'ថ្ងៃ', 1, 1),
(5, 'សប្តាហ៏', 1, 2),
(6, 'ខែ', 1, 4),
(7, 'លើក', 1, 3);

-- --------------------------------------------------------

--
-- Structure for view `procurement_institution_view`
--
DROP TABLE IF EXISTS `procurement_institution_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `procurement_institution_view` AS select `central_ministry_agencies`.`Id` AS `Id`,`central_ministry_agencies`.`Name` AS `Name`,`central_ministry_agencies`.`IsActive` AS `IsActive`,`central_ministry_agencies`.`Status` AS `Status` from `central_ministry_agencies` where (`central_ministry_agencies`.`IsActive` = 1) union select `moef_province`.`Id` AS `Id`,`moef_province`.`Name` AS `Name`,`moef_province`.`IsActive` AS `IsActive`,`moef_province`.`Status` AS `Status` from `moef_province` where (`moef_province`.`IsActive` = 1) union select `public_institutions_enterprice`.`Id` AS `Id`,`public_institutions_enterprice`.`Name` AS `Name`,`public_institutions_enterprice`.`IsActive` AS `IsActive`,`public_institutions_enterprice`.`Status` AS `Status` from `public_institutions_enterprice` where (`public_institutions_enterprice`.`IsActive` = 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
